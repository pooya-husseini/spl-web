(function () {
    'use strict';
    angular.module('lego.base', [
        'ngRoute',
        'ngSanitize',
        'ngAnimate',
        'ngResource',
        'ui.bootstrap',
        'ui.grid',
        'ui.grid.selection',
        'ui.grid.expandable',
        'ui.grid.pagination',
        'ui.grid.exporter',
        'ui.select',
        'ngTagsInput',
        'ui.grid.autoResize',
        'pascalprecht.translate',
        "toastr",
        "ngMessages",
        "ui.router",
        "ngMessages",
        "ui.mask",
        "mgcrea.ngStrap"
    ]);
})();
angular.module("lego.base").run(["$templateCache", function($templateCache) {$templateCache.put("/app/view/PrintDialog.html","<div class=\"modal-header\" id=\"print-dialog-header\"><h3 class=\"modal-title\">{{translate(\'app.print-dialog\')}}</h3></div><form name=\"form\" class=\"modal-body\"><div class=\"row text-center\"><div class=\"panel print-panel panel-default\" ng-click=\"Action.report(\'pdf\')\" do-action=\"P:Action.report(\'pdf\')\"><div class=\"print-panel-body\"><button class=\"btn\"><span>P</span></button></div><div class=\"panel-footer print-panel-footer\"><button class=\"btn btn-lg btn-media\"><img src=\"app/resources/images/mime/64x64/pdf.png\" class=\"btn-media-image\"></button></div></div><div class=\"panel print-panel panel-default\" ng-click=\"Action.report(\'docx\')\" do-action=\"W:Action.report(\'docx\')\"><div class=\"print-panel-body\"><button class=\"btn\"><span>W</span></button></div><div class=\"panel-footer print-panel-footer\"><button class=\"btn btn-lg btn-media\"><img src=\"app/resources/images/mime/64x64/docx.png\"></button></div></div><div class=\"panel print-panel panel-default\" ng-click=\"Action.report(\'html\')\" do-action=\"H:Action.report(\'html\')\"><div class=\"print-panel-body\"><button class=\"btn\"><span>H</span></button></div><div class=\"panel-footer print-panel-footer\"><button class=\"btn btn-lg btn-media\"><img src=\"app/resources/images/mime/64x64/html.png\"></button></div></div></div><div class=\"row text-center\"><div class=\"panel print-panel panel-default text-center\" ng-click=\"Action.report(\'xlsx\')\" do-action=\"X:Action.report(\'xlsx\')\"><div class=\"print-panel-body\"><button class=\"btn\"><span>X</span></button></div><div class=\"panel-footer print-panel-footer\"><button class=\"btn btn-lg btn-media\"><img src=\"app/resources/images/mime/64x64/xlsx.png\"></button></div></div><div class=\"panel panel-default print-panel\" ng-click=\"Action.report(\'csv\')\" do-action=\"C:Action.report(\'csv\')\"><div class=\"print-panel-body\"><button class=\"btn\"><span>C</span></button></div><div class=\"panel-footer print-panel-footer\"><button class=\"btn btn-lg btn-media\"><img src=\"app/resources/images/mime/64x64/csv.png\"></button></div></div></div></form><div class=\"modal-footer\"></div>");
$templateCache.put("/app/view/home.html","");
$templateCache.put("/app/modules/base/templates/MasterViewer.html","<div><div ng-class=\"{\'form\': orientation != \'inline\',\'form-horizontal\': orientation == \'horizontal\', \'form-inline form-inline-container\': orientation == \'inline\'}\"><div class=\"row\" ng-show=\"orientation == \'horizontal\'\"><span class=\"col-lg-6 col-md-6\" ng-repeat=\"item in dataList\"><label class=\"{{(\'col-sm-\' + labelSize + \' col-md-\' + labelSize)}} control-label\">{{item.label}}</label><div class=\"{{\'col-sm-\' + (12-labelSize) + \' col-md-\' + (12-labelSize)}}\"><input class=\"form-control\" value=\"{{item.content}}\" readonly=\"readonly\"></div></span></div><div class=\"row\" ng-show=\"orientation == \'vertical\'\"><span class=\"col-lg-6 col-md-6\" ng-repeat=\"item in dataList\"><label class=\"{{(\'col-sm-\' + labelSize + \' col-md-\' + labelSize)}} control-label\">{{item.label}}</label><div class=\"{{\'col-sm-\' + (12-labelSize) + \' col-md-\' + (12-labelSize)}}\"><input class=\"form-control\" value=\"{{item.content}}\" readonly=\"readonly\"></div></span></div></div><div ng-show=\"showPlaceHolder\" class=\"well\">{{palceholder}}</div></div>");
$templateCache.put("/app/modules/base/templates/amountCurrency.html","<div ng-show=\"orientation != \'inline\'\" ng-class=\"{\'form\': orientation != \'inline\',\'form-horizontal\': orientation == \'horizontal\', \'form-inline form-inline-container\': orientation == \'inline\'}\"><div class=\"row form-group\"><label for=\"{{id}}\" ng-class=\"{\'sr-only\': orientation == \'inline\'}\" class=\"control-label {{(orientation == \'horizontal\') ? (\'col-lg-\' + labelSize + \' col-md-\' + labelSize) : \'\'}}\"><span ng-if=\"label\">{{label}}</span><span ng-if=\"!label && orientation == \'vertical\'\">&nbsp;</span></label><div ng-show=\"orientation != \'inline\'\" class=\"{{(orientation == \'horizontal\') ? (\'col-lg-\' + (12 - labelSize) + \' col-md-\' + (12 - labelSize)) : \'\'}}\"><div class=\"input-group\" ng-class=\"{\'has-feedback\': feedback, \'has-success\': state == \'valid\', \'has-error\': state == \'invalid\', \'has-warning\': state == \'warning\', \'has-info\': state == \'notice\'}\"><input id=\"{{id}}\" type=\"text\" class=\"form-control\" ng-model=\"amount\" ng-style=\"{direction: \'ltr\'}\" ng-change=\"amountChanged()\" ng-disabled=\"disabled || currency === undefined\"> <span ng-if=\"orientation == \'inline\' && feedback\" class=\"glyphicon glyphicon-{{feedback}} form-control-feedback\"></span> <span class=\"input-group-addon small\"><span>{{currency.banknoteName}} </span><span>\\</span><select ng-model=\"currencyCode\"><option></option><option ng-repeat=\"item in currencyOptions\" value=\"{{item.code}}\" ng-selected=\"item.code == currencyCode\" ng-disabled=\"item.disabled !== \'Undefined\' && item.disabled\">{{item.code}}</option></select>&nbsp;&nbsp;&nbsp;</span></div><div style=\"width:auto ;background-color: #e4e4e4;border-radius: 3px\">{{toword}}</div></div></div></div>");
$templateCache.put("/app/modules/base/templates/body.html","<div class=\"body\" ng-class=\"{false:\'container\',true:\'container-stretched\'}[$ctrl.parent.panelStretched]\"><div class=\"page-content\" ng-click=\"$ctrl.parent.closeAllMenus()\"><div class=\"breadcrumb-container\"><div class=\"dropdown\"><button class=\"crud-operations\" id=\"dropdown1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">عملیات</button><ul class=\"dropdown-menu\" aria-labelledby=\"dropdown1\" has-permission=\"CREATE\"><li ng-repeat=\"action in $ctrl.getActions()\"><div ng-click=\"$ctrl.signalAction(action)\"><i class=\"glyphicon glyphicon-search\"></i> {{(\"Action.\"+action)| translate}}</div></li></ul></div></div><spl:body-content></spl:body-content><div class=\"bottom-spacing\"></div></div></div>");
$templateCache.put("/app/modules/base/templates/checkList-comp.html","<div class=\"panel panel-info\"><div class=\"panel-body\">{{$ctrl.header | translate}}</div><div class=\"panel-footer\"><div class=\"row\"><div class=\"col-lg-12 col-md-12\"><spl:exelector ng-model=\"$ctrl.model\" label=\"{{$ctrl.label | translate}}\" not-null=\"{{$ctrl.notNull}}\" label-size=\"{{$ctrl.labelSize}}\" name=\"$ctrl.name\" placeholder=\"{{$ctrl.placeholder}}\" fields-to-select=\"{{$ctrl.fieldsToSelect}}\" header-field=\"{{$ctrl.headerField}}\" detail-fields=\"{{$ctrl.detailFields}}\" disabled=\"$ctrl.disabled\" source-items=\"$ctrl.sourceItems\" bindable-field=\"{{$ctrl.bindableField}}\"></spl:exelector></div></div><div class=\"row\"><tags-input ng-model=\"$ctrl.ngModel\" placeholder=\"{{$ctrl.tagPlaceholder}}\" allow-duplicates=\"false\" add-from-autocomplete-only=\"true\"></tags-input></div></div></div>");
$templateCache.put("/app/modules/base/templates/checkList.html","<div class=\"panel panel-info\"><div class=\"panel-body\">{{header | translate}}</div><div class=\"panel-footer\"><div class=\"row\"><div class=\"col-lg-12 col-md-12\"><exelector ng-model=\"model\" label=\"{{label | translate}}\" not-null=\"{{notNull}}\" label-size=\"{{labelSize}}\" name=\"name\" placeholder=\"{{placeholder}}\" fields-to-select=\"{{fieldsToSelect}}\" header-field=\"{{headerField}}\" detail-fields=\"{{detailFields}}\" disabled=\"disabled\" source-items=\"sourceItems\" bindable-field=\"{{bindableField}}\"></exelector></div></div><div class=\"row\"><tags-input ng-model=\"ngModel\" placeholder=\"{{tagPlaceholder}}\" allow-duplicates=\"false\" add-from-autocomplete-only=\"true\"></tags-input></div></div></div>");
$templateCache.put("/app/modules/base/templates/checkbox.html","<div class=\"form-inline form-inline-container\"><div class=\"form-group\" ng-if=\"$ctrl.type==\'checkbox\'\"><label for=\"{{$ctrl.id}}\" class=\"control-label {{\'col-lg-\' + $ctrl.labelSize + \' col-md-\' + $ctrl.labelSize}}\"><span ng-if=\"$ctrl.label\">{{$ctrl.label}}</span> <span ng-if=\"!$ctrl.label\">&nbsp;</span> <span ng-show=\"$ctrl.notNull\" style=\"color: red\">*</span></label><div class=\"{{\'col-lg-\' + (12 - $ctrl.labelSize) + \' col-md-\' + (12 - $ctrl.labelSize)}}\"><input id=\"{{$ctrl.id}}\" ng-disabled=\"$ctrl.disabled\" class=\"form-control\" type=\"checkbox\" placeholder=\"{{$ctrl.placeholder}}\" ng-model=\"$ctrl.ngModel\" auto-focus=\"$ctrl.autofocus\"></div></div><div ng-if=\"$ctrl.type==\'toggle\'\" class=\"{{\'col-lg-\' + (12 - $ctrl.labelSize) + \' col-md-\' + (12 - $ctrl.labelSize)}}\"><label class=\"btn btn-primary btn-size-md\" ng-model=\"$ctrl.ngModel\" uib-btn-checkbox uncheckable>{{$ctrl.label | translate}}</label></div></div>");
$templateCache.put("/app/modules/base/templates/createpage.html","<div ng-if=\"$ctrl.parent.operation==\'CREATE\'\"><div class=\"panel panel-info\"><div class=\"panel-heading\"><h3 class=\"modal-title\">{{\'app.modal.create\'|translate}} {{$ctrl.titleKey|translate}}</h3></div></div><div class=\"panel-body\"><div class=\"row\"><div class=\"col-xs-9 col-xs-offset-1\"><form name=\"form\" class=\"modal-body\" do-action=\"ctrl+enter:Actions.insert()\" ng-transclude></form></div></div></div><div class=\"modal-footer\"><div class=\"row\"><div class=\"col-xs-12\"><button class=\"btn btn-primary btn-lg\" id=\"saveButton\" ng-click=\"$ctrl.insert(form)\">{{(\'app.button.create\'|translate)}}</button></div></div></div></div>");
$templateCache.put("/app/modules/base/templates/dateTimePicker.html","<div class=\"panel panel-default\"><div class=\"panel-heading\"><h4>{{$ctrl.header}}</h4><br></div><div class=\"panel-body\"><input type=\"text\" ng-model=\"sample\" ui-mask=\"9999/99/99\" ui-mask-placeholder ui-mask-placeholder-char=\"_\"> <span class=\"glyphicon glyphicon-remove\"></span><!--<span class=\"input-group-addon\"><i class=\"icon-reload icons\"></i></span>--></div></div>");
$templateCache.put("/app/modules/base/templates/datepicker.html","<div class=\"panel panel-default\"><div class=\"panel-heading\"><h4>{{$ctrl.header}}</h4></div><div class=\"panel-body\"><input type=\"text\" ng-model=\"sample\" ui-mask=\"9999/99/99\" ui-mask-placeholder ui-mask-placeholder-char=\"_\"> <span class=\"glyphicon glyphicon-remove\" style=\"display:block\"></span><!--<span class=\"input-group-addon\"><i class=\"icon-reload icons\"></i></span>--></div></div>");
$templateCache.put("/app/modules/base/templates/editpage.html","<div ng-if=\"$ctrl.parent.operation==\'EDIT\'\"><div class=\"panel panel-info\"><div class=\"panel-heading\"><h3 class=\"modal-title\">{{\'app.modal.edit\' | translate}} {{titleKey|translate}}</h3><button class=\"btn btn-danger close-button glyphicon glyphicon-remove\" ng-click=\"$ctrl.parent.Actions.cancel()\"></button></div></div><div class=\"panel-body\"><form name=\"form\" class=\"modal-body\" do-action=\"ctrl+enter:Action.insert()\" ng-transclude></form></div><div class=\"modal-footer\"><div class=\"row\"><div class=\"col-xs-12\"><button class=\"btn btn-primary btn-lg\" id=\"saveButton\" ng-click=\"$ctrl.insert(form)\">{{\'app.button.edit\' | translate}}</button></div></div></div></div>");
$templateCache.put("/app/modules/base/templates/exelector-directive.html","<div class=\"form form-horizontal form-input\"><div class=\"form-group\" ng-class=\"{\'has-feedback\': feedback, \'has-success\': state == \'valid\', \'has-error\': state == \'invalid\', \'has-warning\': state == \'warning\', \'has-info\': state == \'notice\'}\"><label id=\"label\" for=\"{{name}}\" class=\"control-label {{(\'col-xs-\' + labelSize + \' col-md-\' + labelSize)}}\"><span ng-if=\"label\">{{label|translate}}</span><span ng-show=\"notNull\" style=\"color: red\">*</span></label><div class=\"{{(\'col-lg-\' + (12 - labelSize) + \' col-md-\' + (12 - labelSize))}}\"><ui-select ng-model=\"item.selected\" theme=\"select2\" ng-disabled=\"disabled\" class=\"exelector-content\"><ui-select-match placeholder=\"{{placeholder}}\"><span ng-repeat=\"selectedField in selectedFields\">{{$select.selected[selectedField]+\' \'}}</span></ui-select-match><ui-select-choices repeat=\"item in data | propsFilter: { {{propsFilter}} }\"><div ng-if=\"headerField!=undefined\" ng-bind-html=\"item[headerField] | highlight: $select.search\"></div><small><span ng-repeat=\"field in fieldNames\" ng-bind-html=\"\' \'+item[field] | highlight: $select.search\"></span></small></ui-select-choices></ui-select></div></div></div>");
$templateCache.put("/app/modules/base/templates/exelector.html","<div class=\"form form-horizontal form-input\"><div class=\"form-group\"><label id=\"$ctrl.label\" for=\"{{$ctrl.name}}\" class=\"control-label {{(\'col-xs-\' + $ctrl.labelSize + \' col-md-\' + $ctrl.labelSize)}}\"><span ng-if=\"$ctrl.label\">{{$ctrl.label|translate}}</span><span ng-show=\"$ctrl.notNull\" style=\"color: red\">*</span></label><div class=\"{{(\'col-lg-\' + (12 - $ctrl.labelSize) + \' col-md-\' + (12 - $ctrl.labelSize))}}\"><ui-select ng-model=\"$ctrl.item.selected\" theme=\"select2\" ng-disabled=\"$ctrl.disabled\" class=\"exelector-content\"><ui-select-match placeholder=\"{{$ctrl.placeholder}}\"><span ng-repeat=\"selectedField in $ctrl.selectedFields\">{{$select.selected[selectedField]+\' \'}}</span></ui-select-match><ui-select-choices repeat=\"item in $ctrl.data | propsFilter: { {{$ctrl.propsFilter}} }\"><div ng-if=\"$ctrl.headerField!=undefined\" ng-bind-html=\"item[$ctrl.headerField] | highlight: $select.search\"></div><small><span ng-repeat=\"field in $ctrl.fieldNames\" ng-bind-html=\"\' \'+item[field] | highlight: $select.search\"></span></small></ui-select-choices></ui-select></div></div></div>");
$templateCache.put("/app/modules/base/templates/footer.html","<div class=\"footer-navbar\" ng-transclude></div>");
$templateCache.put("/app/modules/base/templates/inputText.html","<div class=\"form form-horizontal form-input input-sm\"><div class=\"form-group\"><label for=\"{{$ctrl.name}}\" class=\"control-label {{\'col-lg-\' + $ctrl.labelSize + \' col-md-\' + $ctrl.labelSize}}\"><span ng-if=\"$ctrl.label\">{{$ctrl.label|translate}}</span><span ng-show=\"$ctrl.notNull\" style=\"color: red\">*</span></label><div class=\"{{\'col-lg-\' + (12 - $ctrl.labelSize) + \' col-md-\' + (12 - $ctrl.labelSize)}}\"><input id=\"{{$ctrl.name}}\" ng-disabled=\"$ctrl.disabled\" type=\"{{$ctrl.type}}\" placeholder=\"{{$ctrl.placeholder}}\" ng-model=\"$ctrl.ngModel\" popover-trigger=\"mouseenter\" name=\"{{$ctrl.name}}\" typeahead=\"item for item in $ctrl.getItems($viewValue)\" class=\"spl-input-text\" uib-popover=\"{{$ctrl.tooltip}}\"></div></div></div>");
$templateCache.put("/app/modules/base/templates/lookup.html","<div ng-class=\"{\'default\': \'panel-default\', \'danger\': \'panel-danger\', \'info\': \'panel-info\', \'warning\': \'panel-warning\'}[type]\"><div><div ng-class=\"{\'form\': orientation != \'inline\',\'form-horizontal\': orientation == \'horizontal\', \'form-inline form-inline-container\': orientation == \'inline\'}\"><div class=\"form-group\" ng-class=\"{\'has-feedback\': feedback, \'has-success\': state == \'valid\', \'has-error\': state == \'invalid\', \'has-warning\': state == \'warning\', \'has-info\': state == \'notice\'}\"><div class=\"panel panel-default lookupPanelBorder lookupRow\" ng-class=\"{\'default\': \'panel-default\', \'danger\': \'panel-danger\', \'info\': \'panel-info\', \'warning\': \'panel-warning\'}[type]\"><div><h4 class=\"panel-title\">{{title}}<div class=\"row\" style=\"padding-top: 10px\"><span class=\"col-lg-6 col-md-6\"><div ng-show=\"orientation != \'inline\'\" class=\"{{(orientation == \'horizontal\') ? (\'col-lg-12 col-md-12\') : \'\'}}\"><div class=\"row form-group\"><label for=\"searchbox\" ng-class=\"{\'sr-only\': orientation == \'inline\'}\" class=\"control-label {{(orientation == \'horizontal\') ? (\'col-sm-\' + labelSize + \' col-md-\' + labelSize) : \'\'}}\"><span ng-if=\"label\">{{label}}</span></label><div class=\"input-group\"><input id=\"searchbox\" ng-blur=\"trigger()\" class=\"form-control\" type=\"text\" ng-model=\"model\"><span ng-if=\"orientation == \'inline\' && feedback\" class=\"glyphicon glyphicon-{{feedback}} form-control-feedback\"></span> <span class=\"input-group-addon\"><i style=\"cursor: pointer\" ng-click=\"trigger()\" class=\"glyphicon glyphicon-search\"></i> <i ng-show=\"searchIcon\" style=\"cursor: pointer\" ng-click=\"findItem()\" class=\"glyphicon glyphicon-filter\"></i> <i ng-show=\"addIcon\" style=\"cursor: pointer\" ng-click=\"addItem()\" class=\"glyphicon glyphicon-plus\"></i></span></div></div><span ng-if=\"feedback\" class=\"glyphicon glyphicon-{{feedback}} form-control-feedback\"></span></div><div class=\"form-group\" ng-show=\"orientation == inline\"><label for=\"searchbox\" ng-class=\"{\'sr-only\': orientation == \'inline\'}\" class=\"control-label {{(orientation == \'inline\') ? (\'col-sm-\' + labelSize) : \'\'}}\"><span ng-if=\"label\">{{label}}</span><span ng-if=\"!label && orientation == \'\'\">&nbsp;</span></label><div class=\"input-group\"><input id=\"searchbox\" class=\"col-md-2 col-sm-2 form-control\" type=\"text\" placeholder=\"{{placeholder}}\" ng-model=\"model\"><span ng-if=\"orientation == \'inline\' && feedback\" class=\"glyphicon glyphicon-{{feedback}} form-control-feedback\"></span> <span class=\"input-group-addon\"><span ng-click=\"trigger()\" class=\"glyphicon glyphicon-search\"></span></span></div></div></span><span class=\"col-lg-6 col-md-6\"></span></div></h4></div><div ng-show=\"showBody\" class=\"row panel-body lookupBodyRow\" style=\"background-color: #e4e4e4\"><div class=\"row\" ng-show=\"orientation == \'horizontal\' && showDetail==true\"><span class=\"col-lg-6 col-md-6\" ng-repeat=\"item in dataList\"><label class=\"{{(\'col-sm-\' + labelSize + \' col-md-\' + labelSize)}} control-label\">{{item.label}}</label><div class=\"{{\'col-sm-\' + (12-labelSize) + \' col-md-\' + (12-labelSize)}}\"><input class=\"form-control\" value=\"{{item.content}}\" readonly=\"readonly\"></div></span></div><div class=\"row\" ng-show=\"orientation == \'vertical\' && showDetail==true\" style=\"background-color: #e4e4e4\"><span class=\"col-lg-6 col-md-6\" ng-repeat=\"item in dataList\"><label class=\"{{(\'col-sm-\' + labelSize + \' col-md-\' + labelSize) }} control-label\">{{item.label}}</label><div class=\"{{\'col-sm-\' + (12-labelSize) + \' col-md-\' + (12-labelSize)}}\"><input class=\"form-control\" value=\"{{item.content}}\" readonly=\"readonly\"></div></span></div></div></div></div></div></div></div>");
$templateCache.put("/app/modules/base/templates/mainpage.html","<div ng-if=\"$ctrl.parent.operation!=\'CREATE\'\"><div class=\"panel panel-info\"><div class=\"panel-heading\">{{\'app.panel.title.search\'|translate}}</div></div><div class=\"panel-body\"><div class=\"row\"><div class=\"col-xs-12\"><form name=\"form\" class=\"search-form\" ng-transclude></form></div></div><div class=\"row\"><div class=\"col-xs-2 col-xs-offset-10\"><button ng-click=\"$ctrl.parent.Actions.reset()\" class=\"btn btn-primary btn-search\">{{\'app.button.reset\'|translate}}</button> <button ng-click=\"$ctrl.find(form)\" class=\"btn btn-success btn-search\">{{\'app.button.search\' |translate}}</button></div></div></div><div class=\"row operation-buttons\"><div class=\"col-xs-3\"><button class=\"btn btn-primary\" ng-click=\"$ctrl.parent.Actions.update()\" ng-disabled=\"$ctrl.parent.MainGrid.isSelected()==false\">{{\'app.button.edit\'|translate}}</button> <button class=\"btn btn-info\" ng-click=\"$ctrl.parent.Actions.show()\" ng-disabled=\"$ctrl.parent.MainGrid.isSelected()==false\">{{\'app.button.show\'|translate}}</button> <button class=\"btn btn-danger\" ng-click=\"$ctrl.parent.Actions.delete()\" ng-disabled=\"$ctrl.parent.MainGrid.isSelected()==false\">{{\'app.button.delete\'|translate}}</button></div><div class=\"col-xs-2 col-xs-offset-7\"><span>{{\"app.panel.title.search.result\" | translate}} <span class=\"badge\">{{$ctrl.parent.MainGrid.content.length}}</span></span></div></div><div class=\"row\"><div class=\"col-xs-12\"><div ui-grid=\"$ctrl.parent.MainGrid\" class=\"main-grid\" ui-grid-selection dir=\"rtl\" ui-grid-pagination ui-grid-auto-resize></div></div></div><div class=\"row text-center\"><div class=\"col-xs-12\"><uib-pagination items-per-page=\"1\" total-items=\"$ctrl.parent.MainGrid.gridApi.pagination.getTotalPages()\" ng-model=\"$ctrl.parent.MainGrid.currentPage\" max-size=\"100\" class=\"pagination-sm\" boundary-links=\"true\" rotate=\"false\" num-pages=\"$ctrl.parent.MainGrid.numPages\" ng-change=\"$ctrl.parent.MainGrid.gridApi.pagination.seek($ctrl.parent.MainGrid.currentPage)\" previous-text=\"{{\'app.previous\'|translate}}\" next-text=\"{{\'app.next\'|translate}}\" first-text=\"{{\'app.first\'|translate}}\" last-text=\"{{\'app.last\'|translate}}\"></uib-pagination></div></div></div>");
$templateCache.put("/app/modules/base/templates/monthPicker.html","<div class=\"panel panel-default\"><div class=\"panel-heading\"><h4>{{$ctrl.header}}</h4><br></div><div class=\"panel-body\"><input type=\"text\" ng-model=\"sample\" ui-mask=\"\" ui-mask-placeholder ui-mask-placeholder-char=\"_\"> <span class=\"glyphicon glyphicon-remove\"></span><!--<span class=\"input-group-addon\"><i class=\"icon-reload icons\"></i></span>--></div></div>");
$templateCache.put("/app/modules/base/templates/panel.html","<div class=\"panel\" ng-disable=\"$ctrl.disabled\" ng-class=\"{\'default\': \'panel-default\', \'danger\': \'panel-danger\', \'info\': \'panel-info\', \'warning\': \'panel-warning\'}[$ctrl.type]\"><div class=\"panel-heading\"><h4 class=\"panel-title\" ng-click=\"isOpen = !isOpen\" style=\"cursor: pointer\"><i ng-if=\"$ctrl.collapsible\" class=\"glyphicon {{isOpen ? \'glyphicon-chevron-right\' : \'glyphicon-chevron-down\'}}\"></i> {{$ctrl.title}}</h4></div><div ng-show=\"!$ctrl.collapsible\" class=\"panel-body\" ng-transclude></div><div ng-show=\"$ctrl.collapsible\"><div class=\"panel-body animate-show\" ng-transclude ng-show=\"isOpen\"></div></div></div>");
$templateCache.put("/app/modules/base/templates/panelbar.html","<div class=\"menu-container\"><div ng-class=\"{false:\'application-sidebar\',true:\'application-sidebar-stretched\'}[$ctrl.parent.panelStretched]\"><div class=\"menu-item\" ng-click=\"$ctrl.Actions.selectHome()\"><div class=\"glyphicon glyphicon-home menu-item-icon\"></div><p class=\"menu-item-title\">صفحه اصلی</p></div><div ng-init=\"menu.expanded=false\" ng-repeat=\"menu in $ctrl.menus track by menu.title\"><div class=\"menu-item\" ng-click=\"$ctrl.Actions.expandMenu(menu)\" ng-class=\"{\'menu-expanded\': menu.expanded}\"><div class=\"glyphicon glyphicon-th-large menu-item-icon\"></div><p class=\"menu-item-title\">{{(\'menu.\' + menu.prefix +\".header\") | translate}}</p></div><div ng-show=\"menu.expanded==true && $ctrl.parent.panelStretched==false\"><div ng-repeat=\"detail in menu.details\" class=\"sub-menu-item\" ng-class=\"{\'sub-item-selected\': (menu.prefix+\'.\'+detail.labelKey)==$ctrl.SelectedPage}\" ng-click=\"$ctrl.Actions.select(menu,detail)\" angular-ripple><div class=\"glyphicon glyphicon-triangle-left sub-menu-item-icon\"></div><p class=\"sub-menu-item-title\">{{(\'menu.\'+menu.prefix+\'.\'+detail.labelKey) | translate}}</p></div></div><div ng-if=\"menu.entered==true && $ctrl.parent.panelStretched==true\" class=\"menu-sub-panel-stretched\"><div class=\"menu-header\">{{(\'menu.\' + menu.prefix+\".header\") | translate}}</div><div ng-repeat=\"detail in menu.details\" class=\"sub-menu-item\" ng-class=\"{\'sub-item-selected\': (menu.prefix+\'.\'+detail.labelKey)==$ctrl.SelectedPage}\" ng-click=\"$ctrl.Actions.select(detail)\"><div class=\"glyphicon glyphicon-triangle-left sub-menu-item-icon\"></div><p class=\"sub-menu-item-title\">{{(\'menu.\'+menu.prefix+\'.\'+detail.labelKey) | translate}}</p></div></div></div></div></div>");
$templateCache.put("/app/modules/base/templates/popup.html","<div ng-transclude></div>");
$templateCache.put("/app/modules/base/templates/radio.html","<div class=\"form-inline form-inline-container form-group\"><div class=\"radio {{\'col-sm-\' + (12 - $ctrl.labelSize) + \' col-sm-offset-\' + $ctrl.labelSize + \' col-md-\' + (12 - $ctrl.labelSize) + \' col-md-offset-\' + $ctrl.labelSize}}\"><label><input ng-disabled=\"$ctrl.disabled\" id=\"{{$ctrl.id}}\" name=\"bu-radio-{{$ctrl.parent.group}}\" type=\"radio\" ng-model=\"$ctrl.ngModel\"> {{$ctrl.label}}</label></div></div><div class=\"form-inline form-inline-container\"><div class=\"form-group\" ng-if=\"$ctrl.type==\'checkbox\'\"><label for=\"{{$ctrl.id}}\" class=\"control-label {{\'col-lg-\' + $ctrl.labelSize + \' col-md-\' + $ctrl.labelSize}}\"><span ng-if=\"$ctrl.label\">{{$ctrl.label}}</span> <span ng-if=\"!$ctrl.label\">&nbsp;</span> <span ng-show=\"$ctrl.notNull\" style=\"color: red\">*</span></label><div class=\"{{\'col-lg-\' + (12 - $ctrl.labelSize) + \' col-md-\' + (12 - $ctrl.labelSize)}}\"><input id=\"{{$ctrl.id}}\" ng-disabled=\"$ctrl.disabled\" class=\"form-control\" type=\"checkbox\" placeholder=\"{{$ctrl.placeholder}}\" ng-model=\"$ctrl.ngModel\" auto-focus=\"$ctrl.autofocus\"></div></div><div ng-if=\"$ctrl.type==\'toggle\'\" class=\"{{\'col-lg-\' + (12 - $ctrl.labelSize) + \' col-md-\' + (12 - $ctrl.labelSize)}}\"><label class=\"btn btn-primary btn-size-md\" ng-model=\"$ctrl.ngModel\" uib-btn-checkbox uncheckable>{{$ctrl.label | translate}}</label></div></div>");
$templateCache.put("/app/modules/base/templates/searchpage.html","<div ng-if=\"$ctrl.parent.operation==\'SEARCH\'\"><div class=\"panel panel-info\"><div class=\"panel-heading\">{{\'app.modal.search\' | translate}} {{titleKey | translate}}</div></div><div class=\"panel-body\"><div class=\"row\"><div class=\"col-xs-12\"><div class=\"search-modal-body\" do-action=\"ctrl+enter:Actions.find()\"><form name=\"Form\" ng-enter=\"$ctrl.parent.Actions.find()\" class=\"search-form\" ng-transclude></form></div></div></div><div class=\"row\"><div class=\"col-xs-1 col-xs-offset-11\"><button ng-click=\"$ctrl.parent.Actions.find()\" class=\"btn btn-success btn-search\">{{\'app.button.search\' | translate}}</button></div></div><div class=\"row\"><div class=\"col-xs-12\"><div class=\"modal-grid\" ui-grid=\"$ctrl.parent.MainGrid\" ui-grid-selection ui-grid-pagination ui-grid-auto-resize dir=\"{{$ctrl.parent.localeDirection}}\"></div></div></div></div><div class=\"modal-footer\"><div class=\"row\" style=\"text-align: center\"><div class=\"col-xs-12\"><button ng-disabled=\"!$ctrl.parent.MainGrid.isSelected\" class=\"btn btn-info btn-lg\" ng-click=\"$ctrl.parent.Actions.add()\">{{\'app.button.select\' | translate}}</button> <button class=\"btn btn-primary btn-lg\" ng-click=\"$ctrl.parent.Actions.reset()\">{{\'app.button.reset\' | translate}}</button></div></div></div></div>");
$templateCache.put("/app/modules/base/templates/select.html","<div ng-class=\"{\'form\': orientation != \'inline\',\'form-horizontal\': orientation == \'horizontal\', \'form-inline form-inline-container\': orientation == \'inline\'}\"><div class=\"form-group\" ng-class=\"{\'has-feedback\': feedback, \'has-success\': state == \'valid\', \'has-error\': state == \'invalid\', \'has-warning\': state == \'warning\', \'has-info\': state == \'notice\'}\"><label for=\"{{id}}\" ng-class=\"{\'sr-only\': orientation == \'inline\'}\" class=\"control-label {{(orientation == \'horizontal\') ? (\'col-sm-\' + labelSize + \' col-md-\' + labelSize) : \'\'}}\"><span ng-if=\"label\">{{label}}</span><span ng-if=\"!label && orientation == \'vertical\'\">&nbsp;</span></label><div class=\"{{(orientation == \'horizontal\') ? (\'col-sm-\' + (12 - labelSize) + \' col-md-\' + (12 - labelSize)) : \'\'}}\"><!--ng-options=\"item.value as item.caption for item in items\"--><select name=\"y\" ng-disabled=\"disabled\" id=\"{{id}}\" class=\"form-control\" ng-model=\"model\"><option value=\"\"></option><option ng-repeat=\"item in items\" value=\"{{item.value}}\" ng-selected=\"item.value == model\">{{item.caption}}</option></select><span ng-if=\"feedback\" class=\"glyphicon glyphicon-{{feedback}} form-control-feedback\"></span></div></div></div>");
$templateCache.put("/app/modules/base/templates/showpage.html","<div ng-if=\"$ctrl.parent.operation==\'SHOW\'\"><div class=\"panel panel-info\"><div class=\"panel-heading\"><h3 class=\"modal-title\">{{\'app.modal.showDetails\' | translate}} {{$ctrl.parent.titleKey| translate}}</h3><button class=\"btn btn-danger close-button glyphicon glyphicon-remove\" ng-click=\"$ctrl.cancel()\"></button></div></div><div class=\"panel-body\"><form name=\"ShowUser\" class=\"modal-body\" ng-transclude></form></div></div>");
$templateCache.put("/app/modules/base/templates/simple-search-result.html","<div class=\"row operation-buttons\"><div class=\"col-xs-3\"><button class=\"btn btn-primary\" ng-click=\"$ctrl.parent.Actions.edit()\" ng-disabled=\"$ctrl.parent.MainGrid.isSelected==false\">{{\'app.button.edit\'|translate}}</button> <button class=\"btn btn-info\" ng-click=\"$ctrl.parent.Actions.show()\" ng-disabled=\"$ctrl.parent.MainGrid.isSelected==false\">{{\'app.button.show\'|translate}}</button> <button class=\"btn btn-danger\" ng-click=\"$ctrl.parent.Actions.remove()\" ng-disabled=\"$ctrl.parent.MainGrid.isSelected==false\">{{\'app.button.delete\'|translate}}</button></div><div class=\"col-xs-2 col-xs-offset-7\"><span>{{\"app.panel.title.search.result\" | translate}} <span class=\"badge\">{{$ctrl.parent.MainGrid.content.length}}</span></span></div></div><div class=\"row\"><div class=\"col-xs-12\"><div ui-grid=\"$ctrl.parent.MainGrid\" class=\"main-grid\" ui-grid-selection dir=\"rtl\" ui-grid-pagination ui-grid-auto-resize></div></div></div><div class=\"row text-center\"><div class=\"col-xs-12\"><uib-pagination items-per-page=\"1\" total-items=\"$ctrl.parent.MainGrid.gridApi.pagination.getTotalPages()\" ng-model=\"$ctrl.parent.MainGrid.currentPage\" max-size=\"100\" class=\"pagination-sm\" boundary-links=\"true\" rotate=\"false\" num-pages=\"$ctrl.parent.MainGrid.numPages\" ng-change=\"$ctrl.parent.MainGrid.gridApi.pagination.seek($ctrl.parent.MainGrid.currentPage)\" previous-text=\"{{\'app.previous\'|translate}}\" next-text=\"{{\'app.next\'|translate}}\" first-text=\"{{\'app.first\'|translate}}\" last-text=\"{{\'app.last\'|translate}}\"></uib-pagination></div></div>");
$templateCache.put("/app/modules/base/templates/testable-body.html","<div class=\"body\" ng-class=\"{false:\'container\',true:\'container-stretched\'}[$ctrl.parent.panelStretched]\"><div class=\"page-content\" ng-click=\"$ctrl.parent.panel.closeAllMenus()\"><div class=\"main-content\" ng-transclude></div><div class=\"bottom-spacing\"></div></div></div>");
$templateCache.put("/app/modules/base/templates/textarea.html","<div class=\"form-group form-inline-container form-input input-sm\"><label for=\"{{$ctrl.id}}\" class=\"control-label {{\'col-lg-\' + $ctrl.labelSize + \' col-md-\' + $ctrl.labelSize}}\"><span ng-if=\"$ctrl.label\">{{$ctrl.label}}</span> <span ng-if=\"!$ctrl.label\">&nbsp;</span> <span ng-show=\"$ctrl.notNull\" style=\"color: red\">*</span></label><div class=\"{{\'col-lg-\' + (12 - $ctrl.labelSize) + \' col-md-\' + (12 - $ctrl.labelSize)}}\"><textarea class=\"form-control\" ng-if=\"$ctrl.tailable\" show-tail ng-disabled=\"$ctrl.disabled\" rows=\"4\" id=\"{{$ctrl.id}}\" placeholder=\"{{$ctrl.placeholder}}\" ng-model=\"$ctrl.ngModel\">\n            </textarea></div></div>");
$templateCache.put("/app/modules/base/templates/timepicker.html","<div class=\"panel panel-default\"><div class=\"panel-heading\"><h4>{{$ctrl.header}}</h4><br></div><div class=\"panel-body\"><input type=\"text\" ng-model=\"sample\" ui-mask=\"99:99:99\" ui-mask-placeholder ui-mask-placeholder-char=\"_\"> <span class=\"glyphicon glyphicon-remove\"></span><!--<span class=\"input-group-addon\"><i class=\"icon-reload icons\"></i></span>--></div></div>");
$templateCache.put("/app/modules/base/templates/topnavbar.html","<div class=\"top-navbar\"><div class=\"col-lg-2\"><div ng-class=\"{false:\'hamburger-panel\',true:\'hamburger-panel-stretched\'}[$ctrl.parent.panelStretched]\"><i class=\"glyphicon glyphicon-menu-hamburger\" ng-click=\"$ctrl.parent.Actions.togglePanelStretch();\"></i></div></div><div class=\"col-xs-2 col-xs-offset-7 dropdown\"><div class=\"user-icon-container\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\"><span class=\"glyphicon glyphicon-user user-icon\"></span> <span class=\"caret\"></span> <span id=\"user-dropdownmenu\">{{user.fullName}}</span></div><ul class=\"dropdown-menu user-dropdown-menu\" aria-labelledby=\"user-dropdownmenu\"><li><div><i class=\"glyphicon glyphicon-cog\"></i>تنظیمات من</div></li><li><div><i class=\"glyphicon glyphicon-envelope\"></i>پیغام های من</div></li><li><div><a href=\"/logout\"><i class=\"glyphicon glyphicon-log-out\"></i>تغییر کاربر</a></div></li></ul></div></div>");
$templateCache.put("/app/modules/base/templates/uiDate.html","<div ng-show=\"orientation != \'inline\'\" ng-class=\"{\'form\': orientation != \'inline\',\'form-horizontal\': orientation == \'horizontal\', \'form-inline form-inline-container\': orientation == \'inline\'}\"><div class=\"row form-group\"><label for=\"{{id}}\" ng-class=\"{\'sr-only\': orientation == \'inline\'}\" class=\"control-label {{(orientation == \'horizontal\') ? (\'col-lg-\' + labelSize + \' col-md-\' + labelSize) : \'\'}}\"><span ng-if=\"label\">{{label}}</span><span ng-if=\"!label && orientation == \'vertical\'\">&nbsp;</span></label><div ng-show=\"orientation != \'inline\'\" class=\"{{(orientation == \'horizontal\') ? (\'col-lg-\' + (12 - labelSize) + \' col-md-\' + (12 - labelSize)) : \'\'}}\"><div class=\"input-group\" ng-class=\"{\'has-feedback\': feedback, \'has-success\': state == \'valid\', \'has-error\': state == \'invalid\', \'has-warning\': state == \'warning\', \'has-info\': state == \'notice\'}\" ng-show=\"editable != \'false\' \"><input id=\"searchbox\" placeholder=\"{{placeholder}}\" type=\"text\" class=\"form-control\" datepicker-popup-persian=\"{{format}}\" ng-model=\"model\" is-open=\"persianIsOpen\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\" ng-required=\"true\" close-text=\"closeText\"><span ng-if=\"orientation == \'inline\' && feedback\" class=\"glyphicon glyphicon-{{feedback}} form-control-feedback\"></span> <span class=\"input-group-addon\"><span ng-click=\"openPersian($event)\" class=\"glyphicon glyphicon-calendar\"></span></span></div><div class=\"input-group\" ng-class=\"{\'has-feedback\': feedback, \'has-success\': state == \'valid\', \'has-error\': state == \'invalid\', \'has-warning\': state == \'warning\', \'has-info\': state == \'notice\'}\" ng-show=\"editable === \'false\'\"><input readonly=\"true\" placeholder=\"{{placeholder}}\" id=\"searchbox\" type=\"text\" class=\"form-control\" datepicker-popup-persian=\"{{format}}\" ng-model=\"model\" is-open=\"persianIsOpen\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\" ng-required=\"true\" close-text=\"closeText\"><span ng-if=\"orientation == \'inline\' && feedback\" class=\"glyphicon glyphicon-{{feedback}} form-control-feedback\"></span> <span class=\"input-group-addon\"><span class=\"glyphicon glyphicon-calendar\"></span></span></div></div></div></div>");
$templateCache.put("/app/modules/base/templates/validationMessages.html","<div class=\"error-messages\" ng-if=\"$ctrl.interacted($ctrl.form[$ctrl.name])\" ng-messages=\"$ctrl.form[$ctrl.name].$error\"><div ng-message=\"required\">{{\'messages.required\'|translate}}</div><div ng-message=\"minlength\">{{$ctrl.format($ctrl.translate(\'messages.minlength\'),[$ctrl.form[$ctrl.name].data])}}</div><div ng-message=\"maxlength\">{{$ctrl.format($ctrl.translate(\'messages.maxlength\'),[$ctrl.form[$ctrl.name].data])}}</div><div ng-message=\"equals\">{{$ctrl.format($ctrl.translate(\'messages.equals\'),[$ctrl.form[$ctrl.name].data])}}</div><div ng-message=\"sameAs\">{{$ctrl.format($ctrl.translate(\'messages.sameAs\'),[$ctrl.form[$ctrl.name].data])}}</div><div ng-message=\"number\">{{\'messages.number\'|translate}}</div><div ng-message=\"email\">{{\'messages.email\'|translate}}</div><div ng-message=\"english\">{{\'messages.english\'|translate}}</div><div ng-message=\"persian\">{{\'messages.persian\'|translate}}</div><div ng-message=\"pattern\">{{\'messages.pattern\'|translate}}</div><div ng-message=\"greaterThan\">{{$ctrl.format($ctrl.translate(\'messages.greaterThan\'),[$ctrl.form[$ctrl.name].data])}}</div><div ng-message=\"lessThan\">{{$ctrl.format($ctrl.translate(\'messages.lessThan\'),[$ctrl.form[$ctrl.name].data])}}</div><div ng-message=\"strong-password\">{{\'messages.strong-password\'|translate}}</div><div ng-transclude></div></div>");
$templateCache.put("/app/modules/base/templates/waiting.html","<div class=\"waiting\" data-loading><div class=\"please-wait\">لطفا صبر کنید....</div><ul class=\"nav navbar-nav sidebar-nav\"><li><div class=\"loading-spiner-holder\"><div id=\"loader\"><div class=\"bull\"></div><div class=\"bull\"></div><div class=\"bull\"></div><div class=\"bull\"></div><div class=\"bull\"></div></div></div></li></ul></div>");
$templateCache.put("/app/modules/base/templates/yearPicker.html","<div class=\"panel panel-default\"><div class=\"panel-heading\"><h4>{{$ctrl.header}}</h4><br></div><div class=\"panel-body\"><input type=\"text\" ng-model=\"sample\" ui-mask=\"9999\" ui-mask-placeholder ui-mask-placeholder-char=\"_\"> <span class=\"glyphicon glyphicon-remove\"></span><!--<span class=\"input-group-addon\"><i class=\"icon-reload icons\"></i></span>--></div></div>");
$templateCache.put("/app/modules/routing/classic/templates/body-content.html","<div class=\"main-content\" ng-view></div>");
$templateCache.put("/app/modules/routing/ui-route/templates/body-content.html","<div class=\"main-content\" ui-view></div>");}]);
(function (angular) {

    'use strict';
    angular.module('lego.ui.route', ['ui.router', 'ngSanitize', 'ngResource', 'lego.base']);

})(window.angular);
(function (module) {
    module.component("splPanelBar", splPanelBar());

    function splPanelBar() {
        return {
            templateUrl: "/app/modules/base/templates/panelbar.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    Controller.$inject = ["Operations", "panel", "$state"];
    
    function Controller(operations, PanelService) {
        var self = this;

        this.$onInit = function () {
            PanelService.loadMenu().then(function (menus) {
                self.menus = menus;
            });
            self.Actions = operations.createPanelUiRouteActions(self);
        }
    }
})(angular.module("lego.ui.route"));
(function (module) {
    module.component("splBodyContent", component());
    function component() {
        return {
            templateUrl: "/app/modules/routing/ui-route/templates/body-content.html",
            transclude: false
        }
    }
})(angular.module("lego.ui.route"));
(function () {
    'use strict';
    angular.module('lego.routing.classic', ['ngRoute', 'ngSanitize', 'ngResource', 'lego.base']);
})();

(function (module) {
    module.component("splPanelBar", splPanelBar());

    function splPanelBar() {
        return {
            templateUrl: "/app/modules/base/templates/panelbar.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    Controller.$inject = ["Operations", "panel"];

    function Controller(operations, PanelService) {
        var self = this;

        this.$onInit = function () {
            PanelService.loadMenu().then(function (menus) {
                self.menus = menus;
                self.Actions = operations.createPanelNgRouteActions(self);
            });

        }
    }
})(angular.module("lego.routing.classic"));
(function (module) {
    module.component("splBodyContent", component());
    function component() {
        return {
            templateUrl: "/app/modules/routing/classic/templates/body-content.html",
            transclude: false
        }
    }
})(angular.module("lego.routing.classic"));
(function (angular) {
    'use strict';
    angular.module('lego.test', ['application']);
})(window.angular);
/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Jul 17, 2016 2:17:47 PM
 * @version 2.5-SNAPSHOT
 */

(function (module) {
    module.factory('RestServiceMock', Service);
    function Model() {
        this.items = {};
        this.id = 0;
    }

    var model = new Model();

    Service.$inject = ['$q', '$timeout', 'Utils'];

    function Service($q, $timeout, Utils) {

        return {
            createInstance: function () {
                return {
                    delete: function (item) {
                        var defer = $q.defer();
                        if (model.items[item.id]) {
                            var item = model.items[item.id];
                            delete model.items[item.id];
                            defer.resolve(item);
                            $timeout(function () {
                                defer.resolve(item);
                            }, 100);
                        }
                        return {$promise: defer.promise};
                    },
                    create: function (obj) {
                        var item = Utils.cloneObject(obj);
                        var defer = $q.defer();
                        $timeout(function () {
                            var id = model.id++;
                            model.items[id] = item;
                            item.id = id;
                            defer.resolve(item);
                        }, 100);
                        return {$promise: defer.promise};
                    },
                    search: function (item) {


                        var defer = $q.defer();
                        $timeout(function () {
                            if (item == undefined) {
                                defer.resolve(model.items);
                            } else {
                                defer.resolve(model.items[item]);
                            }
                        }, 100);

                        return {$promise: defer.promise};
                    },
                    update: function (obj) {
                        var item = Utils.cloneObject(obj);

                        model.items[0] = item;
                        var deffer = $q.defer();
                        $timeout(function () {
                            model.items[0] = item;
                            deffer.resolve(model.items[0])
                        }, 100);
                        return {$promise: deffer.promise};
                    },

                }
            },
            createObject: function () {
                model = new Model();
            }
        }
    }
})(angular.module('lego.test'));

(function (module) {
    var $urlRouterProviderRef;
    var $stateProviderRef;

    module.config(Configure);
    module.run(Runnable);

    Configure.$inject = ["$locationProvider", "$urlRouterProvider", "$stateProvider"];
    Runnable.$inject = ['$q', '$state', '$http', 'panel'];

    function Configure($locationProvider, $urlRouterProvider, $stateProvider) {
        $urlRouterProviderRef = $urlRouterProvider;
        $locationProvider.html5Mode(false);
        $stateProviderRef = $stateProvider;

        // $stateProvider.state('com', {
        //     template: "<ui-view/>",
        //     url: "/home"
        // });

        // $urlRouterProvider.otherwise("/");

    }

    function Runnable($q, $state, $http, panel) {
        $http.get("app/conf/routes.json").success(function (data) {
            var home = {};
            _.forEach(data, function (value) {

                var keys = Object.keys(value);

                var state = {};

                _.forEach(keys, function (key) {
                    state[key] = value[key];
                });

                // for (var i = 0; i < keys.length; i++) {
                //     var key = keys[i];
                //
                // }
                if (state.home) {
                    home = state;
                }
                // if (!state.parent) {
                //     state.parent = '';
                // }

                $stateProviderRef.state(value.name, state);
            });
            $state.go(home.name);
        });

        // panel.loadMenu().then(function (menus) {
        //     _.forEach(menus, function (menu) {
        //
        //         _.forEach(menu.details, function (detail) {
        //             var name = detail.domain + "." + detail.title;
        //             var url = name.replace(/\./g, "/");
        //             var state = {
        //                 url: url,
        //                 templateUrl: "app/views/" + url + ".html"
        //                 // parent: 'root'
        //             };
        //             $stateProviderRef.state(name, state);
        //         });
        //     });
        // });

        // $state.go("com");
    }
})(angular.module("lego.ui.route"));
(function (module) {
    module.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $routeProvider.when('/', {
            redirectTo: '/home'
        }).when('/:page*', {
            templateUrl: function (parameters) {
                var page = parameters.page;
                if (!/\.html$/.test(page)) {
                    page = page + ".html";
                }
                return "/app/view/" + page;
            }
        });
    }]);

})(angular.module('lego.routing.classic'));
(function (module) {
    module.directive('strongPasswordValidator', Validation);
    Validation.$inject = ["Utils"];

    const pattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.strongPasswordValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'strong-password',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));
(function (module) {
    module.directive('sameAsValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {

                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = attrs.sameAsValidator;
                var initState = new Utils.testAndSet();

                ngModel.$validators.sameAsValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'sameAs',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return data === value;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));
(function (module) {
    module.directive('requiredValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.requiredValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'required',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return value && value.length > 0;
                    }
                };
            }
        }
    }
})(angular.module("lego.base"));
(function (module) {
    module.directive('persianValidator', Validation);

    Validation.$inject = ["Utils"];

    const pattern = /^([آ-ی\s]|[۰۱۲۳۴۵۶۷۸۹])+$/;

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.persianValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'persian',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));

(function (module) {
    module.directive('patternValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var pattern = attrs.patternValidator;
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.patternValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'pattern',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || new RegExp("^" + pattern + "$").test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));
(function (module) {
    module.directive('numberValidator', Validation);

    Validation.$inject = ["Utils"];
    const pattern = /^[+\-]?(\d|[۰۱۲۳۴۵۶۷۸۹])+(\.(\d|[۰۱۲۳۴۵۶۷۸۹])+)?$/;


    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {

                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();
                
                ngModel.$validators.numberValidator = function (value) {
                    // var value = ngModel.$modelValue;
                    Utils.validityCheck({
                        form: form,
                        validationName: 'number',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));
/**
 * Created by mokaram on 06/21/2016.
 */
(function (module) {
    module.directive('minLengthValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = parseInt(attrs.minLengthValidator);
                var initState = new Utils.testAndSet();

                ngModel.$validators.minLengthValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'minlength',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || value.length >= data;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));
/**
 * Created by mokaram on 06/21/2016.
 */
(function (module) {
    module.directive('maxLengthValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = parseInt(attrs.maxLengthValidator);
                var initState = new Utils.testAndSet();

                ngModel.$validators.maxLengthValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'maxlength',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || value.length <= data;
                    }
                };
            }
        }
    }
})(angular.module("lego.base"));
(function (module) {
    module.directive('lessThanValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = parseFloat(attrs.lessThanValidator);
                var initState = new Utils.testAndSet();

                ngModel.$validators.lessThanValidator = function (value) {
                    value = parseFloat(value);

                    Utils.validityCheck({
                        form: form,
                        validationName: 'lessThan',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || value < data;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));
(function (module) {
    module.directive('greaterThanValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = parseFloat(attrs.greaterThanValidator);
                var initState = new Utils.testAndSet();

                ngModel.$validators.greaterThanValidator = function (value) {

                    value = parseFloat(value);

                    Utils.validityCheck({
                        form: form,
                        validationName: 'greaterThan',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker,
                        data: data
                    });


                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || value > data;
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));
(function (module) {
    module.directive('equalsValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {

                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = attrs.equalsValidator;
                var initState = new Utils.testAndSet();

                ngModel.$validators.equalsValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'equals',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker,
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return data == value;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));
(function (module) {
    module.directive('englishValidator', Validation);

    const pattern = /^[\s\w\d\?><;,\{}\[\]\-_\+=!@#\$%^&\(\)\*\|']*$/;

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.englishValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'english',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));
(function (module) {


    module.directive('emailValidator', Validation);

    Validation.$inject = ["Utils"];
    const email = /[a-z]+([\._][a-z0-9]+)*([+\-][^@]+)?@(\d+\.\d+\.\d+\.\d+|([a-z0-9]+(-[a-z0-9]+)?\.)*[a-z]{2,})/i;


    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.emailValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'email',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || email.test(value);
                    }
                };
            }
        }
    }
})(angular.module("lego.base"));
(function (module) {
    module.directive('customValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^ngController', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[2];
                var ngModel = ctrls[0];
                var ngController = ctrls[1];
                var data = attrs.customValidator;
                var initState = new Utils.testAndSet();

                ngModel.$validators.customValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'custom',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return ngController[data]();
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));
(function (module) {
    module.config(['legoProvider', function (legoProvider) {
            var trs = {

                'prefix': 'messages',
                "required": "مقدار این فیلد اجباری است",
                "minlength": "طول عبارت باید بیشتر از {0} باشد",
                "maxlength": "طول عبارت باید کمتر از {0} باشد",
                "email": "مقدار وارد شده آدرس ایمیل نمی باشد",
                "pattern": "مقدار وارد شده از الگوی مورد نظر تبعیت نمی کند",
                "greaterThan": "مقدار وارد شده باید بیشتر از {0} باشد",
                "lessThan": "مقدار وارد شده باید کمتر از {0} باشد",
                "english": "مقدار وارد شده باید کاراکترهای انگلیسی باشد",
                "persian": "مقدار وارد شده باید کاراکترهای فارسی باشد",
                "equals": "مقدار وارد شده باید برابر با {0} باشد",
                "sameAs": "مقدار وارد شده باید برابر با {0} باشد",
                "number": "مقدار وارد شده باید عدد باشد",
                "custom": "مقدار وارد شده نادرست است",
                "strong-password": "رمز وارد شده از لحاظ امنیتی ضعیف می باشد"
            };
            legoProvider.loadTranslations(trs, "fa_IR");
        }]
    );
})(angular.module("lego.base"));
(function (module) {
    module.config(['legoProvider', function (legoProvider) {
            var trs = {

                'prefix': 'app',
                "title": "پروژه ی من",
                "success": "عملیات با موفقیت انجام شد",
                "failed": "عملیات با خطا مواجه گردید",
                "danger": "اخطار",
                "warning": "هشدار",
                "not-found": "موردی یافت نشد",
                "upload.success": "ارسال فایل با موفقیت انجام گردید",
                "upload.failed": "ارسال فایل با خطا مواجه گردید",
                "question-title": "سوال",
                "question-body": "آیا مطمین هستید؟",
                "button.create": "ایجاد",
                "button.save": "ثبت",
                "button.close": "بستن",
                "button.edit": "ویرایش",
                "button.search": "جستجو",
                "button.delete": "حذف",
                "button.cancel": "انصراف",
                "button.show": "نمایش جزییات",
                "button.select": "انتخاب",
                "button.reset": "پاک کن",
                "button.report": "گزارش",
                "button.add": "افزودن",
                "modal.edit": "ویرایش",
                "modal.create": "ایجاد",
                "modal.showDetails": "نمایش جزییات",
                "modal.search": "جستجوی",
                "changePassword": "تغییر رمز عبور",
                "code": "کد",
                "description": "توضیحات",
                "required": "اجباری",
                "yes": "بلی",
                "no": "خیر",
                "validation.error.number": "مقدار وارد شده باید حتما یک عدد باشد",
                "validation.error.equals": "مقادیر مساوی یکدیگر نیستند",
                "validation.error.required": "باید مقدار این فیلد را وارد کنید",
                "validation.error.maxLength": "تعداد حروف وارد شده از محدوده ی مجاز بیشتر است",
                "validation.error.minLength": "تعداد حروف وارد شده از محدوده ی مجاز کمتر است",
                "validation.error.pattern": "مقدار وارد شده از الگوی مورد نظر تبعیت نمی کند",
                "validation.error.greaterThan": "مقدار وارد شده از حد مجاز کمتر است",
                "validation.error.lessThan": "مقدار وارد شده از حد مجاز بیشتر است",
                "connection.dropped": "ارتباط با سرور قطع گردید",
                "connection.failed": "ارتباط با سرور امکان پذیر نمی باشد",
                "logout": "تغییر کاربر",
                "select.master-viewer": "بر روی دکمه ی انتخاب کلیک نمایید",
                "panel.title.search": "موارد قابل جستجو",
                "panel.title.search.result": "نتیجه جستجو",
                "oldPassword": "رمز عبور قدیمی",
                "newPassword": "رمز عبور جدید",
                "previous": "قبلی",
                "next": "بعدی",
                "first": "اول",
                "last": "آخر",
                "mainPage": "صفحه اصلی",
                "print-dialog": "چاپ گزارش",
                "index": "اندیس",
                "download": "دانلود",
                "upload": "آپلود",
                "operations": "عملیات"
            };
        legoProvider.loadTranslations(trs, "fa_IR");
        }]
    );
})(angular.module("lego.base"));
(function (module) {
    module.config(['legoProvider', function (legoProvider) {
        var trs = {
            'prefix': 'app',
            "title": "My project",
            "success": "Operation Succeed!",
            "failed": "Operation failed!",
            "danger": "Danger",
            "warning": "Warning",
            "not-found": "No item found!",
            "upload.success": "Uploading file succeed!",
            "upload.failed": "Uploading file failed!",
            "question-title": "Question",
            "question-body": "Are you sure?",
            "button.create": "Create",
            "button.save": "Save",
            "button.close": "Close",
            "button.edit": "Edit",
            "button.search": "Search",
            "button.delete": "Delete",
            "button.cancel": "Cancel",
            "button.show": "Show details",
            "button.select": "Select",
            "button.reset": "Reset",
            "button.report": "Report",
            "button.add": "Add",
            "modal.edit": "Edit",
            "modal.create": "Create",
            "changePassword": "Change Password",
            "modal.showDetails": "Show details",
            "modal.search": "Search",
            "code": "Code",
            "description": "Description",
            "required": "Required",
            "yes": "Yes",
            "no": "No",
            "validation.error.number": "The entered value is not number!",
            "validation.error.equals": "The items are not equal!",
            "validation.error.required": "This item is required!",
            "validation.error.maxLength": "The item length is above than limit!",
            "validation.error.minLength": "The item length is below than limit!",
            "validation.error.pattern": "The item does not match with the pattern",
            "validation.error.greaterThan": "The item is greater than the limit",
            "validation.error.lessThan": "The item is smaller than the limit",
            "connection.dropped": "Connection dropped!",
            "connection.failed": "Connection failed!",
            "logout": "Logout",
            "select.master-viewer": "Click on the select button!",
            "panel.title.search": "Searchable items",
            "panel.title.search.result": "Search result",
            "oldPassword": "Old password",
            "newPassword": "New password",
            "previous": "Previous",
            "next": "Next",
            "first": "First",
            "last": "Last",
            "mainPage": "Main Page",
            "print-dialog": "Printing dialog",
            "index": "index",
            "download": "Download",
            "upload": "Upload",
            "operations": "Operations"
        };
        legoProvider.loadTranslations(trs, "en_US");
    }
    ]);
})(angular.module("lego.base"));
(function (module) {
    module.config(['legoProvider', function (legoProvider) {
            var trs = {
                'prefix': 'Action',
                "CREATE": "صفحه ایجاد",
                "SEARCH": "صفحه جستجو"
            };
            legoProvider.loadTranslations(trs, "fa_IR");
        }]
    );
})(angular.module("lego.base"));
(function (module) {
    module.config(['legoProvider', function (legoProvider) {
        var trs = {
            'prefix': 'Action',
            "CREATE": "Create Page",
            "SEARCH": "Search Page"
        };
        legoProvider.loadTranslations(trs, "en_US");
    }
    ]);
})(angular.module("lego.base"));
(function (module) {
    module.service("panel", ["$http", "$q", PanelService]);


    function PanelService($http, $q) {

        return {
            loadMenu: function () {

                var defer = $q.defer();
                $http.get("app/conf/menu.json").then(function (success) {

                    // these two operations is just for removing empty objects from menu

                    var menus = _.filter(success.data.menus, function (item) {
                        return Object.keys(item).length > 0;
                    });
                    _.forEach(menus, function (menu) {
                        menu.details = _.filter(menu.details, function (item) {
                            return Object.keys(item).length > 0;
                        });
                    });
                    defer.resolve(menus);
                });
                return defer.promise;
            }
        }
    }
})(angular.module("lego.base"));
(function (module) {
    module.service("print", ["$uibModal", print]);

    function print(e) {
        return {
            print: function (url, model) {
                return e.open({
                    templateUrl: "/app/view/PrintDialog.html",
                    size: "sm",
                    controller: "PrintController",
                    windowClass: "modal-print-content",
                    resolve: {
                        url: function () {
                            return url;
                        },
                        model: function () {
                            return model;
                        }
                    }
                }).result
            }
        }
    }
})(angular.module("lego.base"));
(function (module) {
    module.provider('lego', ['$translateProvider', lego]);

    function lego($translateProvider) {
        return {
            loadTranslations: function (translations, locale) {
                var convertedTranslation = {};
                _.forOwn(translations, function (value, key) {
                    if (key !== "prefix") {
                        var translationKey = translations.prefix + "." + key;
                        if (translations.prefix == "" || translations.prefix.trim() == "") {
                            translationKey = key;
                        }
                        convertedTranslation[translationKey] = value;
                    }
                });
                $translateProvider.translations(locale, convertedTranslation);
            },
            $get: function () {

            }
        }
    }
})(angular.module('lego.base'));
(function (module) {
    module.filter('propsFilter', Filter);

    function Filter() {
        return function (items, props) {
            var out = [];

            if (angular.isArray(items)) {
                items.forEach(function (item) {
                    var itemMatches = false;

                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }
                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        }
    }


})(angular.module("lego.base"));
/**
 * Created by keshvari on 8/19/14.
 */
(function (module) {

    module.filter('persianNumber', persianNumber);

    function persianNumber() {
        return function (item) {
            if (angular.isUndefined(item)) {
                return '';
            }
            var id = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            return item.replace(/[0-9]/g, function (w) {
                return id[+w]
            });
        };
    }
})(angular.module('lego.base'));
(function (module) {
    module.filter('persianDate', persianDate);

    persianDate.$inject = ['Utils'];

    function persianDate(utils) {
        return function (item) {
            if (angular.isUndefined(item)) {
                return '';
            }
            var gYear = parseInt(item.split('-')[0]);
            var gmonth = parseInt(item.split('-')[1]);
            var gDay = parseInt(item.split('-')[2]);
            var jalali = JalaliDate.gregorianToJalali(gYear, gmonth, gDay);
            return utils.normalizeGDate(jalali, "/")
        };
    }


})(angular.module('lego.base'));
/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Dec 1, 2015 5:37:15 PM
 * @version 1.0.0
 */


(function (module) {

    module.filter('timedPersianDate', ['persianDateFilter', timedPersianDate]);

    function timedPersianDate(persianDateFilter) {
        return function (date) {
            if (date != undefined && date != '') {
                return persianDateFilter(date) + " " + date.substring(10);
            } else {
                return "";
            }
        };
    }
})(angular.module("lego.base"));
(function (module) {
    module.filter('boolean', booleanFilter);

    function booleanFilter() {
        return function (code) {
            if (code == true) {
                return "بلی";
            } else {
                return "خیر";
            }
        };
    }
})(angular.module("lego.base"));
(function (module) {

    var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    var ARGUMENT_NAMES = /([^\s,]+)/g;

    function getParamNames(func) {
        var fnStr = func.toString().replace(STRIP_COMMENTS, '');
        var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
        if (result === null)
            result = [];
        return result;
    }

    module.factory('Utils', Utils);

    Utils.$inject = ['$q', 'i18nService', '$translate', '$filter'];

    function Utils($q, i18nService, $translate, $filter) {

        function translate(key) {
            return $filter("translate")(key);
        }

        function translateAsync(key) {
            return $translate(key);
        }

        return {

            cloneObject: function (obj) {
                return JSON.parse(JSON.stringify(obj));
            },
            normalizeGDate: function (date, delimiter) {
                delimiter = delimiter || '/';
                return date[0] + delimiter + ('0' + date[1]).slice(-2) + delimiter + ('0' + date[2]).slice(-2);
            },

            translate: function (item) {
                return translate(item)
            },

            initGrid: function (ctrl, gridConf, autoArrange) {
                var grid = {
                    multiSelect: false,

                    enableHighlighting: true,
                    enableColumnReordering: true,
                    enableColumnResize: true,
                    enableRowSelection: true,
                    enableRowHeaderSelection: false,
                    enablePaginationControls: false,
                    paginationCurrentPage: 1,
                    minRowsToShow: 10,
                    paginationPageSize: 10,
                    totalItems: 100,
                    content: [],
                    data: '$ctrl.parent.MainGrid.content',
                    onRegisterApi: function (gridApi) {
                        grid.gridApi = gridApi;
                    }
                };

                grid.isSelected = function () {
                    return grid.gridApi != undefined &&
                        grid.gridApi.selection.getSelectedRows() != undefined &&
                        grid.gridApi.selection.getSelectedRows().length > 0;
                };

                grid.selectedRowId = function () {
                    if (grid.isSelected()) {
                        return grid.gridApi.selection.getSelectedRows()[0].id;
                    }
                };

                _.assign(grid, gridConf);

                if (autoArrange) {
                    arrangeColumnsFunc(grid, 100);
                }
                i18nService.setCurrentLang('fa');

                return grid;
            },
            arrangeGridColumns: function (grid) {
                arrangeColumnsFunc(grid, 100);
            },
            extractSelectedItem: function (grid) {
                if (grid == undefined || grid.data == undefined) {
                    return {};
                }

                var selected = copyObject(grid.selectedItems);

                if (selected.length > 1) {
                    return selected
                } else if (selected.length === 1) {
                    return selected[0];
                } else {
                    return {};
                }
            },

            removeGridItem: function (model, item) {
                return _.remove(model, item);

            },
            addToModel: function (model, data) {
                return _.concat(model, data);
            },

            populateGrid: function (grid, data) {
                if (data instanceof Array) {
                    grid.data = data;
                } else {
                    grid.data = [data];
                }
            },
            convertEnum: function (e, className) {
                return _.map(e, function (item, index) {
                    return {
                        code: item.value,
                        description: translate(className + "." + item.value + ".label"),
                        index: index
                    }
                });

            },
            loadAllPromises: function (promises) {
                var defer = $q.defer();
                $q.all(promises).then(function () {
                    defer.resolve();
                }, function () {
                    defer.reject();
                });
                return defer.promise;
            },

            intersect: function (item1, item2) {
                return _.intersection(item1, item2);
            },
            isFunction: function (item) {

                return typeof item === "function";
            },
            validityCheck: function (conf) {
                var elementNgModel = conf.form[conf.ngModel.$name];

                if (conf.form.saved || conf.validityChecker()) {
                    elementNgModel.$setValidity(conf.validationName, true);
                    conf.element.removeClass('has-error');
                } else {
                    elementNgModel.$setValidity(conf.validationName, false);
                    if (!conf.initState) {
                        conf.element.addClass('has-error');
                    }
                    if (conf.data) {
                        elementNgModel.data = conf.data;
                    }
                }
            },
            testAndSet: TestAndSet
        };
    }

    /**
     * @return {Function}
     */
    function TestAndSet() {
        var that = this;
        return function () {
            if (that.result == undefined) {
                that.result = false;
                return true;
            }
            return that.result;
        };
    }

    function arrangeColumnsFunc(grid, headsLength) {
        if (headsLength == undefined) {
            headsLength = 88;
        }

        var total = headsLength;
        var gfilter = _.filter(grid.columnDefs, function (item) {
            return item.visible != false;
        });

        if (headsLength == 100) {
            var count = gfilter.length;
        } else {
            count = gfilter.length - 1; // to remove the operation column
        }
        var a = _.range(count);

        var slice = headsLength / count;
        var acc = 0;
        var array = _.map(a, function () {
            var returnVal = slice;
            if (total < slice) {
                returnVal = total
            }
            total -= slice;
            acc += returnVal;
            return returnVal;
        });
        array.push(100 - acc);

        _.each(gfilter, function (item, index) {
            item.width = array[index] + "%";
        });
    }

    function copyObject(obj) {
        return JSON.parse(JSON.stringify(obj));
    }
})(angular.module('lego.base'));
(function (module) {

    module.factory('User', User);

    User.$inject = ['$http', '$q'];

    function User($http, $q) {

        return {
            getCurrentUser: getCurrentUser
        };

        function getCurrentUser() {
            var defer = $q.defer();

            $http.get("/login/user").success(function (success) {
                var user = {
                    username: success.username,
                    displayName: success.firstName,
                    fullName: success.firstName + "  " + success.lastName,
                    avatar: "/app/resources/images/admin.png",
                    status: 'busy',
                    lastLoginDate: success.lastLoginDate,
                    role: success.role
                };
                defer.resolve(user);
            });

            return defer.promise;
        }


    }
})(angular.module('lego.base'));
(function (module) {

    module.factory('Storage', Storage);

    function Storage() {

        return {

            writeOnStorage: function (key, value) {
                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem(key, value);
                    return true;
                } else {
                    return false;
                }
            },
            readStorageValue: function (key) {
                if (typeof(Storage) !== "undefined") {
                    return localStorage.getItem(key);
                } else {
                    return undefined;
                }
            }
        };
    }
})(angular.module('lego.base'));
/**
 * Created by pooya on 20/12/14.
 */

(function (module) {
    module.factory('ProjectCache', projectCache);

    projectCache.$inject = ['$cacheFactory'];

    function projectCache($cacheFactory) {
        return $cacheFactory('projectCache');
    }
})(angular.module("lego.base"));

(function (module) {

    module.factory('Operations', operations);

    function closeAllMenus(ctrl) {
        _.forEach(ctrl.menus, function (value) {
            value.expanded = false;
            value.entered = false;
        });
    }

    operations.$inject = ['Utils', 'NotificationUtils', '$timeout', '$location', '$resource', 'ProjectCache', '$uibModal', 'FormActions', '$state', '$q'];

    function operations(utils, NotificationUtils, $timeout, $location, $resource, projectCache, $uibModal, FormActions, $state, $q) {

        return {
            /**
             *
             *
             * @param templateUrl
             * @param controller
             * @param extraOps
             * @returns {{select: ops.select, edit: ops.edit, create: ops.create, show: ops.show}}
             */
            makeBasicCrudService: function (templateUrl, controller, extraOps) {

                function makeModal(crudType, data) {
                    return $uibModal.open({
                        templateUrl: templateUrl,
                        size: 'lg',
                        animation: true,
                        controller: controller,
                        controllerAs: '$ctrl',
                        resolve: {
                            data: function () {
                                return data;
                            }
                        }
                    }).result;
                }

                var ops = {
                    select: function () {
                        return makeModal("SEARCH", undefined);
                    },
                    edit: function (model) {
                        return makeModal("EDIT", model);
                    },
                    create: function (model) {
                        return makeModal("CREATE", model);
                    },

                    show: function (model) {
                        return makeModal("SHOW", model);
                    }
                };

                _.assign(ops, extraOps);

                return ops;
            },

            /**
             *  makes the basic rest operations
             * @param {string} url the url to define operations on it
             * @param {object} extraOperations the extra operations that should added to the result
             * @param {object} cacheableConfig configuration for the transferring data. that contains cacheKeyFunc and cacheKeyValue.
             *  two callback functions to produce the key and value of the cache
             * @returns {$resource} the $resource with the given parameters
             */
            makeBasicRestClient: function (url, extraOperations, cacheableConfig) {
                var readOps = {
                    findById: {method: 'GET', params: {id: '@id'}},
                    searchColumns: {method: 'GET', isArray: true, params: {url: 'searchColumn'}},
                    get: {method: 'GET', isArray: true, params: {url: 'all'}},
                    search: {method: 'POST', isArray: true, params: {url: 'search'}},
                };
                var cacheable = Object.keys(cacheableConfig).length == 2;

                var fullCrudOps = {
                    create: {
                        method: 'POST', params: {url: 'create'},
                        transformRequest: function (data, h) {
                            if (cacheable) {
                                addItemToCache(data);
                            }
                            return JSON.stringify(data);
                        }
                    },
                    update: {
                        method: 'PUT', params: {id: '@id'},
                        transformResponse: function (data, h) {
                            var obj = JSON.parse(data);
                            if (cacheable) {
                                addItemToCache(obj);
                            }
                            return obj;
                        }
                    },
                    delete: {
                        method: 'DELETE', params: {id: '@id'},
                        transformRequest: function (data, h) {
                            return JSON.stringify(data);
                        }
                    }
                };

                var ops = {};

                _.assign(ops, readOps);
                _.assign(ops, fullCrudOps);
                _.assign(ops, extraOperations);

                var resource = $resource(url, {}, ops);

                function addItemToCache(data) {
                    var key = cacheableConfig.cacheKeyFunc(data);
                    // var key = data.code + '.' + data.mainType;
                    var value = cacheableConfig.cacheValueFunc(data);
                    // var value = data.description;
                    projectCache.put(key, value);
                }

                function removeItemFromCache(data) {
                    var key = cacheableConfig.cacheKeyFunc(data);
                    // var key = data.code + '.' + data.mainType;
                    projectCache.remove(key);
                }

                if (cacheable) {
                    resource.get().$promise.then(function (success) {
                        for (var index in success) {
                            if (success.hasOwnProperty(index)) {
                                addItemToCache(success[index]);
                            }
                        }
                    });
                }

                return resource;
            },

            initialize: function (ctrl, preModel) {

                FormActions.setActions(["SEARCH", "CREATE"]);

                ctrl.operation = "SEARCH";

                FormActions.subscribe(function (action) {
                    ctrl.operation = action;
                });


                ctrl.Model = {};
                ctrl.Search = {};
                ctrl.selected = {};

                if (preModel != undefined) {
                    ctrl.Model = utils.cloneObject(preModel);
                }

                window.setTimeout(function () {
                    $(window).resize();
                    $(window).resize();
                }, 1000);

                if (utils.isFunction(ctrl.$onInit)) {
                    ctrl.$onInit();
                }

            },

            createPanelUiRouteActions: function (ctrl) {
                return {
                    selectHome: function () {
                        $state.go("home")
                    },

                    closeAllMenus: function () {
                        closeAllMenus(ctrl);
                    },

                    leaveMenu: function (menu) {
                        menu.entered = false;
                    },

                    expandMenu: function (menu) {
                        if (menu.entered == false && menu.expanded == false) {
                            closeAllMenus(ctrl);
                            menu.entered = true;
                            menu.expanded = true;
                        } else {
                            closeAllMenus(ctrl);
                        }
                    },

                    select: function (menu, detail) {
                        $state.go(detail.state);

                        ctrl.SelectedPage = menu.prefix + "." + detail.labelKey;
                        ctrl.selectedIndex = -1;
                        ctrl.hasCreate = detail.hasCreate;

                        ctrl.closeAllMenus();
                    }
                }

            },
            createPanelNgRouteActions: function (ctrl) {
                return {
                    selectHome: function () {
                        $location.url("/#")
                    },

                    closeAllMenus: function () {
                        closeAllMenus(ctrl);
                    },

                    leaveMenu: function (menu) {
                        menu.entered = false;
                    },

                    expandMenu: function (menu) {
                        if (menu.entered == false && menu.expanded == false) {
                            closeAllMenus(ctrl);
                            menu.entered = true;
                            menu.expanded = true;
                        } else {
                            closeAllMenus(ctrl);
                        }
                    },

                    select: function (menu, menuItem) {
                        $timeout(function () {
                            $location.url(menu.prefix.replace(/\./g, "/") + "/" + menuItem.labelKey);

                            ctrl.SelectedPage = menu.prefix + "." + menuItem.labelKey;
                            ctrl.selectedIndex = -1;
                            ctrl.hasCreate = menuItem.hasCreate;

                            ctrl.closeAllMenus();
                        })
                    }
                }

            },
            /**
             *
             * @param config
             * @param extraActions
             * @returns {{find: actions.find, insert: actions.insert, add: actions.add, cancel: actions.cancel}}
             */
            createBasicModalActions: function (config, extraActions) {
                function getModel() {
                    return config.ctrl["Model"];
                }

                function getGrid() {
                    return config.ctrl["MainGrid"];
                }


                var actions = {
                    find: function () {
                        config.service.search(getModel()).then(
                            function (data) {
                                getGrid().content = data
                            }
                        );
                    },
                    insert: function () {
                        config.$modalInstance.close(getModel());
                    },
                    add: function () {
                        var selected = getGrid().gridApi.selection.getSelectedRows()[0];
                        config.$modalInstance.close(selected);
                    },
                    cancel: function () {
                        config.$modalInstance.dismiss();
                    }
                };

                _.assign(actions, extraActions);

                return actions;
            },
            makeBasicServiceOperations: function (config, extraOperations) {
                var operations = {
                    update: function (item) {
                        var defer = $q.defer();
                        config.restService.update(item).$promise.then(
                            function (success) {
                                NotificationUtils.notifySuccess();
                                defer.resolve(success);
                            },
                            function (failure) {
                                defer.reject(failure);
                            }
                        );

                        return defer.promise;
                    },
                    create: function (item) {
                        var defer = $q.defer();
                        config.restService.create(item).$promise.then(
                            function (success) {
                                NotificationUtils.notifySuccess();
                                defer.resolve(success);
                            },
                            function (failure) {
                                defer.reject(failure);
                            }
                        );

                        return defer.promise;
                    },

                    search: function (item) {
                        var defer = $q.defer();
                        config.restService.search(item).$promise.then(
                            function (data) {
                                if (data.length <= 0) {
                                    NotificationUtils.notify(utils.translate('app.not-found'), "warning");
                                }
                                defer.resolve(data);
                            },
                            function (failure) {
                                defer.reject(failure);
                            }
                        );

                        return defer.promise;
                    },
                    delete: function (item) {
                        var defer = $q.defer();
                        NotificationUtils.question().then(function () {
                            config.restService.delete({id: item.id}).$promise.then(
                                function (success) {
                                    NotificationUtils.notifySuccess();
                                    defer.resolve(success);
                                },
                                function (failure) {
                                    defer.reject(failure);
                                }
                            )
                        });

                        return defer.promise;
                    },
                    changePassword: function (item) {
                        var defer = $q.defer();

                        config.crudService.changePassword(item).then(
                            function (result) {
                                config.restService.changePassword(result).$promise.then(
                                    function (success) {
                                        NotificationUtils.notifySuccess();
                                        defer.resolve(success);
                                    },
                                    function (failure) {
                                        defer.reject(failure);
                                    }
                                );
                            }
                        );
                        return defer.promise;
                    }
                };
                _.assign(operations, extraOperations);
                return operations;
            },
            makeBasicActions: function (config, extraActions) {
                // crudService, restService, grid, model, printUrl
                function getSelectedItem() {
                    return getGrid().gridApi.selection.getSelectedRows()[0];
                }

                function getModel() {
                    return config.ctrl['Search'];
                }

                function getGrid() {
                    return config.ctrl['MainGrid'];
                }

                function getOperation() {
                    return config.ctrl["operation"];
                }

                function setOperation(op) {
                    FormActions.signalAction(op);
                }

                var actions = {
                    show: function () {
                        setOperation("SHOW");
                        var selectedItem = getSelectedItem();
                        config.crudService.show(selectedItem);
                    },

                    update: function (args) {
                        var selectedItem = getSelectedItem();
                        setOperation("EDIT");
                        config.crudService.edit(selectedItem).then(
                            function (result) {
                                config.service.update(result);
                            });
                    },
                    create: function () {
                        config.service.create(config.ctrl["Model"]).then(
                            function (success) {
                                $timeout(function () {
                                    config.ctrl['Model'] = {};
                                });
                            }
                        );
                    },

                    reset: function () {
                        $timeout(function () {
                            config.ctrl['Search'] = {};
                        });
                    },

                    showPrint: function () {
                        getPrintService().print(config.printUrl, getModel());
                    },

                    search: function () {

                        config.service.search(getModel()).then(
                            function (data) {

                                getGrid().content = data;
                            });
                    },
                    delete: function () {
                        var selectedItem = getSelectedItem();
                        config.service.delete(selectedItem).then(
                            function (success) {
                                utils.removeGridItem(getGrid().content, selectedItem);
                            }
                        );
                    },
                    changePassword: function () {
                        var selectedItem = getSelectedItem();
                        config.crudService.changePassword(selectedItem).then(
                            function (result) {
                                config.service.changePassword(result);
                            }
                        );
                    }
                };
                _.assign(actions, extraActions);
                return actions;
            }
        }
    }

})(angular.module('lego.base'));
(function (module) {

    module.factory('NotificationUtils', NotificationUtils);

    NotificationUtils.$inject = ['$q', '$filter', 'toastr'];

    function NotificationUtils($q, $filter, toastr) {

        function translate(key) {
            return $filter("translate")(key);
        }

        function notifyFunc(msg, type) {


            switch (type) {
                case "warning":
                    //alertify.warning(msg);
                    toastr.warning(msg);
                    break;
                case "normal":
                    toastr.info(msg);
                    break;
                case "danger":
                case "error":

                    toastr.error(msg, {
                        "timeOut": "-1",
                        "extendedTimeOut": "-1"
                    });
                    break;
                case "success":
                    toastr.success(msg);
                    break;
            }

        }

        return {
            notifySuccess: function () {
                return notifyFunc(translate('app.success'), "success");
            },
            notify: function (msg, type) {
                return notifyFunc(msg, type);
            },

            fixedNotify: function (msg, type) {

                switch (type) {
                    case "warning":
                        toastr.warning(msg);
                        break;
                    case "normal":
                        toastr.info(msg);
                        break;
                    case "danger":
                    case "error":
                        toastr.error(msg, {
                            "timeOut": "-1",
                            "extendedTimeOut": "-1"
                        });
                        break;
                    case "success":
                        toastr.success(msg);
                        break;
                }
            },

            question: function (msg) {
                msg = msg || translate('app.question-body');
                var defer = $q.defer();
                alertify
                    .okBtn(translate('app.yes'))
                    .cancelBtn(translate('app.no'))
                    .confirm(msg, function () {
                        defer.resolve();
                    }, function () {
                        defer.reject();
                    });


                return defer.promise;
            }
        };
    }

})(angular.module('lego.base'));
/**
 * Created by pooya on 20/12/14.
 */

(function (module) {
    module.factory('FormActions', FormActions);

    function FormActions() {

        var self = this;
        self.subs = [];
        return {
            subscribe: function (cb) {
                self.subs.push(cb);
            },
            setActions: function (actions) {
                self.actions = actions;
            },
            getActions: function () {
                return self.actions;
            },
            signalAction: function (action) {
                _.forEach(self.subs, function (s) {
                    s(action);
                });
            }
        }
    }
})(angular.module("lego.base"));
(function (module) {

    module.factory('HttpInterceptor', httpInterceptor);

    httpInterceptor.$inject = ['$q', 'Utils'];

    function httpInterceptor($q, utils) {
        return {
            'request': function (config) {
                return config;
            },

            'responseError': function (response) {
                // do something on error
                if (response.status === null || response.status === 500) {
                    var exceptionType = undefined;
                    var exceptionTypeHeader = response.headers("exceptionType");
                    if (exceptionTypeHeader != null) {
                        exceptionType = exceptionTypeHeader;
                    } else {
                        exceptionType = response.data.exception;
                    }
                    //var exceptionType = response.headers("exception_type");
                    var error = {
                        method: response.config.method,
                        url: response.config.url,
                        message: response.data,
                        status: response.status
                    };
                    if (exceptionType == undefined) {
                        utils.notify(utils.translate('app.failed'), "danger");
                    } else {
                        var translation = utils.translate('exception.translation.' + exceptionType);
                        if (translation == undefined) {
                            utils.notify(utils.translate('app.failed'), "danger");
                        } else {
                            utils.notify(translation, "danger");
                        }
                    }
                }
                return $q.reject(response);
            }
        };
    }
})(angular.module('lego.base'));
(function (module) {

    var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    var ARGUMENT_NAMES = /([^\s,]+)/g;

    function getParamNames(func) {
        var fnStr = func.toString().replace(STRIP_COMMENTS, '');
        var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
        if (result === null)
            result = [];
        return result;
    }

    module.factory('FileUtils', FileUtils);

    FileUtils.$inject = ['$http'];

    function FileUtils($http) {
        return {
            saveData: (function () {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style.display = "none";
                return function (data, fileName, mimeType) {
                    var blob = new Blob([data], {type: mimeType}),
                        url = window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = fileName;
                    a.click();
                    window.URL.revokeObjectURL(url);
                };
            })(),

            upload: function (item, url) {
                var children = item.getElementsByTagName("input");

                var fileInput = _.find(children, function (item) {
                    return item.className.indexOf('ng-hide') < 0;
                });
                var formData = new FormData();
                formData.append("file", fileInput.files[0]);

                return $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formData,
                    transformRequest: angular.identity
                });
            },
            getMimeType: function (extension) {
                switch (extension) {
                    case "docx":
                        return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    case "xlsx":
                        return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    case "pdf":
                        return "application/pdf";
                    case "html":
                        return "text/html";
                    case "xml":
                        return "application/html";
                    case "csv":
                        return "text/plain";
                }
            }
        };
    }

})(angular.module('lego.base'));
/**
 * @author Mohammad Milad Naseri (m.m.naseri@gmail.com)
 * @date 14/8/14 AD
 */
(function (module) {
    'use strict';
    module.provider('uiValidation', ['$injector', function ($injector) {
        var validators = {};
        this.register = function (name, validator) {
            if (angular.isObject(name) && angular.isUndefined(validator)) {
                angular.forEach(name, function (validator, name) {
                    this.register(name, validator);
                }, this);
                return this;
            }
            if (!angular.isString(name)) {
                throw new Error("Validator name must be a string");
            }
            if (angular.isFunction(validator)) {
                if (!angular.isArray(validator.$inject)) {
                    validator.$inject = $injector.annotate(validator);
                }
            } else if (angular.isArray(validator) && validator.length > 0 && angular.isFunction(validator[validator.length - 1])) {
                var annotation = [];
                for (var i = 0; i < validator.length - 1; i++) {
                    annotation.push(validator[i]);
                }
                validator = validator[validator.length - 1];
                validator.$inject = annotation;
            } else {
                throw new Error("Invalid validator object");
            }
            validators[name] = validator;
            return this;
        };
        this.$get = function () {
            //we make a copy to disallow tampering with the validators in run phase
            return angular.extend({}, validators);
        };
        //registering default validators
        //noinspection JSUnusedGlobalSymbols
        this.register({
            //checks that the given element has a value
            required: function (value) {
                return value !== "" && value !== undefined && value !== null && value !== false;
            },
            //checks for the given values to be in order. Value must be between first and second.
            between: function (value, first, second) {
                if (!isNaN(parseFloat(value)) && !isNaN(parseFloat(first)) && !isNaN(parseFloat(second))) {
                    value = parseFloat(value);
                    first = parseFloat(first);
                    second = parseFloat(second);
                }
                return value >= first && value <= second;
            },
            //checks to see value is less than target
            lessThan: function (value, target) {
                if (!isNaN(parseFloat(value)) && !isNaN(parseFloat(target))) {
                    value = parseFloat(value);
                    target = parseFloat(target);
                }
                return value < target;
            },
            //checks to see if value is greater than target
            greaterThan: function (value, target) {
                if (!isNaN(parseFloat(value)) && !isNaN(parseFloat(target))) {
                    value = parseFloat(value);
                    target = parseFloat(target);
                }
                return value > target;
            },
            //performs an identity equality
            equals: function (value, target) {
                if (value == undefined || value == "") {
                    return true;
                }
                return value == target;
            },
            //performs a strict equality
            sameAs: function (value, target) {
                if (value == undefined || value == "") {
                    return true;
                }
                return value === target;
            },
            //matches the given value against a pattern
            pattern: function (value, pattern) {
                if (value == undefined || value == "") {
                    return true;
                }
                return new RegExp("^" + pattern + "$").test(value);
            },
            //matches the given value against a numeric pattern
            number: function (value) {
                if (value == undefined || value == "") {
                    return true;
                } else {
                    return /^[+\-]?(\d|[۰۱۲۳۴۵۶۷۸۹])+(\.(\d|[۰۱۲۳۴۵۶۷۸۹])+)?$/.test(value);
                }
            },
            //checks to see if the given value is an email address
            email: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return /[a-z]+([\._][a-z0-9]+)*([+\-][^@]+)?@(\d+\.\d+\.\d+\.\d+|([a-z0-9]+(-[a-z0-9]+)?\.)*[a-z]{2,})/i.test(value);
            },
            //checks that the value has a length more than the specified value
            minLength: function (value, length) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && value.length >= parseInt(length);
            },
            //checks that the value has a length less than the specified value
            maxLength: function (value, length) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && value.length <= parseInt(length);
            },
            //checks that the given password is strong enough
            strongPassword: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && value.length > 5 && /\d/.test(value) && /[a-z]/.test(value) && /[A-Z]/.test(value) && /[`~!@#$%^&*\(\)\-+=_\[\]\{\}|\\\/?,\.<>]/.test(value);
            },
            persian: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && /^[آ-ی\s]+$/.test(value);
            },
            english: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && /[\s\w\d]+/.test(value);
            },
            //runs a custom validator function on the input data
            custom: function (value, fn) {
                if (!angular.isFunction(fn)) {
                    throw new Error("Given input is not a valid function: " + fn);
                }
                var args = [value];
                for (var i = 2; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }
                return fn.apply(null, args);
            }
        });
    }]);
    module.directive('uiValidation', ['uiValidation', '$parse', '$injector', '$q', 'Utils', function (uiValidation, $parse, $injector, $q, utils) {
        return {
            restrict: "A",
            link: function ($scope, $element, $attrs) {
                if (!$attrs.name) {
                    throw new Error("Element must have a name for validation to work properly");
                }
                var form = $element[0];
                while (form && form.nodeName.toLowerCase() != 'form') {
                    form = form.parentNode;
                }
                if (!form) {
                    throw new Error("Validation cannot be performed on an element that is outside a form");
                }
                if (!form.hasAttribute('name')) {
                    throw new Error("Form name must be specified for validation to work");
                }
                var messages = {};
                if (angular.isDefined($attrs['validationError'])) {
                    messages = $parse($attrs['validationError'])($scope);
                }
                if (!angular.isObject(messages)) {
                    throw new Error("Validation error messages declared through `validationErrors` must be a valid JSON object");
                }
                var $target = ($element[0].nodeName.toLowerCase() == 'input' || $element[0].nodeName.toLowerCase() == 'select' ? $element : $element.find('input').add($element.find('select')));
                $target.on('focus', function () {
                    $scope.$apply(function () {
                        $element.addClass('ui-active');
                    });
                });
                $target.on('blur', function () {
                    $scope.$apply(function () {
                        $element.removeClass('ui-active');
                        $element.addClass('ui-seen');
                        triggerValidation(null, null);
                    });
                });
                $element.addClass('ui-pristine');
                if (!angular.isString($attrs.modelAttribute)) {
                    $attrs.modelAttribute = angular.isDefined($attrs['model']) ? 'model' : (angular.isDefined($attrs['ngModel']) ? 'ngModel' : 'model');
                }
                //find the form hosting the element
                var formName = angular.element(form).attr('name');
                var elementName = $attrs.name;
                if (!angular.isObject($scope[formName])) {
                    $scope[formName] = {};
                }
                if (!angular.isObject($scope[formName].$error)) {
                    $scope[formName].$error = {};
                }
                if (angular.isUndefined($scope[formName].$valid)) {
                    $scope[formName].$valid = true;
                }
                $scope[formName][elementName] = {
                    $valid: true,
                    $error: {}
                };
                var expression = $parse($attrs.uiValidation);
                //set up the context
                var context = Object.create($scope);
                angular.forEach(uiValidation, function (validator, validation) {
                    var message = messages[validation] || 'app.validation.error.' + validation;

                    message = utils.translate(message);

                    // $scope.$emit('bu.ui.text', {
                    //     text: function () {
                    //         return message;
                    //     },
                    //     set: function (newMessage) {
                    //         message = newMessage;
                    //     }
                    // });
                    messages[validation] = message;
                    context[validation] = function () {
                        //because validation is triggered on being linked, this will be applied even when
                        //the directive has just been initialized
                        $element.addClass('validation-' + validation);
                        var args = [$parse($attrs[$attrs.modelAttribute])($scope)];
                        for (var i = 0; i < arguments.length; i++) {
                            args.push(arguments[i]);
                        }
                        var $inject = [];
                        angular.forEach(validator.$inject, function (dependency) {
                            $inject.push(dependency);
                        });
                        $inject.splice(0, args.length);
                        args.splice(0, 0, null);
                        var func = validator.bind.apply(validator, args);
                        func.$inject = $inject;
                        var validated = $q.defer();
                        var result = $injector.invoke(func);
                        if (angular.isObject(result) && angular.isFunction(result.then)) {
                            //while validation is in progress, we will assume that the field is invalid
                            result.then(validated.resolve, validated.reject);
                        } else {
                            validated.resolve(result);
                        }
                        validated.promise.then(function (result) {
                            if (!result) {
                                //we first register the errors with the element
                                $scope[formName][elementName].$error[validation] = true;
                                $scope[formName][elementName].$valid = false;
                                //then lets invalidate the form
                                $scope[formName].$valid = false;
                                if (angular.isArray($scope[formName].$error[validation])) {
                                    var found = false;
                                    angular.forEach($scope[formName].$error[validation], function (value) {
                                        found = !found && value == elementName;
                                    });
                                    if (!found) {
                                        $scope[formName].$error[validation].push(elementName);
                                    }
                                } else {
                                    $scope[formName].$error[validation] = [elementName];
                                }
                            }
                        });
                    };
                });

                function triggerValidation(newValue, oldValue) {
                    if (newValue != oldValue) {
                        $element.removeClass('ui-pristine');
                        $element.addClass('ui-dirty');
                    }
                    //we assume the form to be valid (in regards to the current element)
                    $scope[formName][elementName].$valid = true;
                    var count = 0;
                    angular.forEach($scope[formName].$error, function (value, name) {
                        var errors = [];
                        angular.forEach($scope[formName].$error[name], function (value) {
                            if (value != elementName) {
                                errors.push(value);
                            }
                        });
                        $scope[formName].$error[name] = errors;
                        if (errors.length == 0) {
                            delete $scope[formName].$error[name];
                        } else {
                            count++;
                        }
                    });
                    $scope[formName].$valid = count == 0;
                    angular.forEach($scope[formName][elementName].$error, function (value, name) {
                        delete $scope[formName][elementName].$error[name];
                    });
                    //now let's call the expression to see if the validation is broken
                    expression(context);
                }

                //add necessary watches
                $scope.$watch($attrs[$attrs.modelAttribute], triggerValidation);
                //if a `dependsOn` clause is specified, we will trigger the validation when the
                //`dependsOn` models change, as well
                if (angular.isString($attrs.dependsOn)) {
                    $scope.$watch($attrs.dependsOn, triggerValidation, true);
                }
                $scope.$watch(function () {
                    return $scope[formName][elementName].$error;
                }, function () {
                    $($target).popover('destroy');
                    var errorMessages = displayErrors();
                    if (errorMessages.length > 0 && $element.hasClass('ui-active')) {
                        $($target).popover('show');
                    }
                }, true);
                $scope.$watch(function () {
                    return $element.hasClass('ui-seen');
                }, displayErrors);
                $scope.$watch(function () {
                    return $element.hasClass('ui-dirty');
                }, displayErrors);
                function displayErrors() {
                    $($target).popover('destroy');
                    $element.removeClass('has-error');
                    var errorMessages = [];
                    angular.forEach($scope[formName][elementName].$error, function (invalid, error) {
                        errorMessages.push(messages[error]);
                    });
                    if (errorMessages.length > 0 && ($element.hasClass('ui-dirty') || $element.hasClass('ui-seen'))) {
                        $element.addClass('has-error');

                        $($target).popover({
                            content: '<ul><li>' + errorMessages.join("</li><li>") + '</li></ul>',
                            html: true,
                            placement: 'top',
                            trigger: 'focus',
                            template: '<div class="popover popover-error" style="z-index:10000;" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
                        });
                    }
                    return errorMessages;
                }
            }
        };
    }]);

})(angular.module('lego.base'));

/**
 * Created by keshvari on 7/27/14.
 */
(function (module) {

    module.directive("uiInputChecklist", ["$q", "$http", function ($q, $http) {
        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkList.html',
            scope: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                labelSize: "@",
                ngModel: '=?',
                descriptor: '&?',
                disabled: "@",
                visible:"@"
            },
            defaults: {
                label: " ",
                placeholder: "",
                validation: "",
                state: "normal",
                feedback: ""
            },
            link: function(scope){
                scope.visible=angular.isDefined(scope.visible)?scope.visible:true;
                scope.disabled=angular.isDefined(scope.disabled)?scope.disabled:false;
            }
        }
    }]);
})(angular.module('lego.base'));
/**
 * Created by pooya on 6/19/16.
 */


(function (module) {

    module.directive("splTranslationPrefix", [function () {
        return {
            restrict: "A",
            transclude: false,
            scope: false,
            link: function (scope, element, attributes) {
                scope.translationPrefix = attributes.splTranslationPrefix;
            },
            controller: Controller
        }
    }]);

    Controller.$inject = ["$scope"];

    function Controller($scope) {
        var self = this;

        $scope.$watch('translationPrefix', function (newVal, oldVal) {

            self.translationPrefix = newVal;
            
        })
    }
})(angular.module('lego.base'));


(function(module) {
    module.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    });
})(angular.module("lego.base"));
/**
 * Created by keshvari on 9/8/14.
 */
(function (module) {
    function emptyDataList(scope) {
        if (scope.showDetail == true) {
            /*removing last items*/
            while (scope.dataList.length > 0) {
                scope.dataList.pop();
            }
        }
    }

    function exitErrorMode(element) {
        element.removeClass('has-error');
        angular.element('.popover').remove();
    }

    function enterErrorMode(result, scope, element) {
        if (result == undefined || result.length == 0) {
            console.log("error occured.");
            var options = {
                title: scope.onErrorMessageTitle,
                content: scope.onErrorMessageContent,
                html: true,
                animation: 'true',
                placement: 'top',
                trigger: 'focus',
                template: '<div class="popover popover-error" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'

            };
            element.addClass('has-error');
            element.find('#searchbox').popover(options);
            element.find('#searchbox').focus();
            element.find('#searchbox').addClass('has-error');
        }
    }

    module.directive('uiLookup', ['$timeout','$filter','$rootScope',function ($timeout,$filter,$rootScope) {

        function handleWithFilters(temp, scope, filterPropertyExtractor, getFilterIndex, propertyExtractor, filterWithArgsExtractor, haveArgument, argumentsWithParenthesisExtractor, argumentExtractor, filterNameExtractor, result) {
            for (var i = 0; i < temp.length; i++) {
                var key = temp[i];
                if (scope.ignoreFields) {
                    if (scope.ignoreFields.indexOf(key) >= 0) {
                        continue;
                    }
                }

                if (key.indexOf('$') == 0) {
                    continue;
                }
                var propertyFilterPair = scope.filters.match(filterPropertyExtractor);

                var index = getFilterIndex(propertyFilterPair, temp[i]);
                var valueToShow;
                if (index != -1) {
                    var pairItem = propertyFilterPair[index];
                    var propertyName = pairItem.match(propertyExtractor);
                    var filterPart = pairItem.match(filterWithArgsExtractor);
                    var filterName;
                    var listOfArguments = [];
                    if (haveArgument.test(filterPart[0])) {
                        var argumentsWithParenthesis = filterPart[0].match(argumentsWithParenthesisExtractor);
                        listOfArguments = argumentsWithParenthesis[0].match(argumentExtractor);
                        filterName = pairItem.match(filterNameExtractor)[0];
                    } else {
                        filterName = filterPart[0];
                    }

                    listOfArguments.unshift(result[propertyName[0]]);
                    var myFilter = $filter(filterName).apply(this, listOfArguments);
                    valueToShow = myFilter;
                }else{
                    valueToShow = scope.data[temp[i]];
                }

                var txt = scope.translationPrefix + '.' + temp[i] + '.label';
                scope.dataList.push(
                    {
                        label: $rootScope.translate(txt),
                        content: valueToShow
                    }
                );
            }
        }

        function handleRegularly(temp, scope, propertyList) {
            for (var i = 0; i < temp.length; i++) {
                var key = temp[i];
                if (scope.ignoreFields) {
                    if (scope.ignoreFields.indexOf(key) >= 0) {
                        continue;
                    }
                }

                if (key.indexOf('$') != 0) {
                    propertyList.push(key);
                }
            }
            for (var i = 0; i < propertyList.length; i++) {
                var txt = scope.translationPrefix + '.' + propertyList[i] + '.label';
                scope.dataList.push(
                    {
                        label: $rootScope.translate(txt),
                        content: scope.data[propertyList[i]]
                    }
                );
            }
        }

        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/lookup.html',
            scope:{
                service:"@",
                label:"@",
                translationPrefix:'@',
                labelSize:"@",
                orientation:"@",
                title:"@",
                model:"=",
                addFunction:"&",
                searchFunction:"&",
                ignoreFields:"@",
                searchIcon:"@",
                addIcon:"@",
                onErrorMessageContent:'@',
                onErrorMessageTitle:'@',
                data:'=',
                filters:'@',
                functionName:'@'
            },
            link: function (scope, element, attr) {
                var filterPropertyExtractor = /(\w+):(\w+)[(]*(\w+)(,)*(\w+)*[)]*/g;
                var propertyExtractor = /(\w+)(?=:)/;
                var filterWithArgsExtractor = /((?!(\w+):)(\w+)[(]*(\w+)*(,)*(\w)*[)]*)/g;
                var argumentsWithParenthesisExtractor = /([(])((\w+)+(,)*)*([)])/;
                var haveArgument = /(\w+)(?![(])((\w+)+(,)*)*(?=[)])/g;
                var argumentExtractor = /(\w+)/g;
                var filterNameExtractor = /(\w+)(?=[(])/g;

                scope.showBody = false;
                scope.dataList = [];

                scope.addItem = function(){
                    exitErrorMode(element);
                    scope.addFunction().then(
                        /*resolve area*/
                        function(){
                            scope.$emit('ItemAdditionCompleted');

                        },
                        /*error area*/
                        function(){
                            scope.$emit('ItemAdditionFailed');
                            console.log("Item not added");
                        }
                    )
                };
                scope.findItem = function(){
                    scope.showBody = true;
                    scope.showDetail = true;
                    emptyDataList(scope);
                    exitErrorMode(element);
                    console.log("inside find item");
//                    console.log(scope.searchFunction);
                    console.log(scope);
                    scope.searchFunction().then(
                        /*on resolve*/
                        function(result){
                            scope.$emit('DataExtractionCompleted');
                            scope.data = result;
                            var propertyList = [];
                            var temp=Object.keys(scope.data);
                            if(attr.filters){
                                handleWithFilters.call(this, temp, scope, filterPropertyExtractor, getFilterIndex, propertyExtractor, filterWithArgsExtractor, haveArgument, argumentsWithParenthesisExtractor, argumentExtractor, filterNameExtractor, result);
                            }else{
                                handleRegularly(temp, scope, propertyList);
                            }
                        },
                        /*on resolve*/
                        function(){
                            scope.$emit('DataExtractionFailed');
                            console.log("Item not extracted to lookup");
                        }
                    );

                };


                scope.showDetail = false;
                var service = module.$injector.get(attr.service);
//                var dataList =[];


//                element.find('#searchbox').keyup(function(event){
//                    if(event.keyCode == '13'){
//                        scope.trigger();
//                    }
//                });


                scope.trigger = function(){

                    if(angular.isUndefined(scope.model)){
                        return;
                    }
                    var relatedFunction = scope.functionName;
                    emptyDataList(scope);
                    scope.showBody = true;
                    service[relatedFunction]({id: scope.model}).$promise.then(
                        /*on success */
                        function (result) {
                            scope.$emit('DataExtractionCompleted');
                            scope.data = result;
                            emptyDataList(scope);
                            exitErrorMode(element);
                            enterErrorMode(result, scope, element);
                            var propertyList = [];
                            var temp=Object.keys(scope.data);
                            if(attr.filters){
                                handleWithFilters.call(this, temp, scope, filterPropertyExtractor, getFilterIndex, propertyExtractor, filterWithArgsExtractor, haveArgument, argumentsWithParenthesisExtractor, argumentExtractor, filterNameExtractor, result);
                            }else{
                                handleRegularly(temp, scope, propertyList);
                            }

                            scope.showDetail = true ;

                        },
                        /*on error*/
                        function (error) {
                            scope.$emit('DataExtractionFailed');
                            var options = {
                                title:scope.onErrorMessageTitle,
                                content: scope.onErrorMessageContent,
                                html:true,
                                animation:'true',
                                placement:'top',
                                trigger:'focus',
                                template: '<div class="popover popover-error" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'

                            };
                            element.addClass('has-error');
                            element.find('#searchbox').popover(options);
                            element.find('#searchbox').focus();
                            element.find('#searchbox').addClass('has-error');

                        });
                };

                var getFilterIndex = function(list,item){
                    for (var i = 0; i < list.length; i++) {
                        var obj = list[i];
                        var propertyName = obj.match(propertyExtractor);
                        if(propertyName[0] == item){
                            return i;
                        }
                    }
                    return -1;
                }


            }
        }
    }]);
})(angular.module('lego.base'));

/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive('loading', ['$http', function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs) {
                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };
                scope.$watch(scope.isLoading, function (v) {
                    if (v) {
                        elm.show();
                    } else {
                        elm.hide();
                    }
                });
            }
        };
    }]);
})(angular.module("lego.base"));
/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive('focusOn', ['$timeout', function ($timeout) {
        return function (scope, element, attrs) {
            scope.$on("key_down", function (event, args) {
                if (attrs.focusOn.toLowerCase() == args.keyPressed.toLowerCase()) {
                    $timeout(function () {
                        element[0].focus();
                    });

                }
            });
        };
    }]);
})(angular.module("lego.base"));
/**
 * Created by mokaram on 07/04/2016.
 */
(function (module) {

    module.directive("exelector", ["$q", "$http", "$timeout", function ($q, $http, $timeout) {
        function checkForm(formCtrl) {
            if (angular.isUndefined(formCtrl)) {
                throw new Error("Validation cannot be performed on an element that is outside a form");
            }
            if (angular.isUndefined(formCtrl.$name)) {
                throw new Error("Form name must be specified for validation to work");
            }
        }

        function setMainType(scope) {
            scope.data = [];
            if (scope.mainType != undefined) {
                if (scope.detailFields == undefined) {
                    scope.detailFields = "description";
                }

                scope.fieldsToSelect = "description";
                scope.bindableField = "code";

                var gpService = module.$injector.get('GeneralParameterRestService');
                gpService.search({mainType: scope.mainType}).$promise.then(
                    function (success) {
                        $timeout(function () {
                            scope.data = success;
                        });
                    }
                );
            } else {
                scope.data = scope.sourceItems;
            }
        }

        function initialize(scope) {
            if (!scope.labelSize) {
                scope.labelSize = "3";
            }
            if (!scope.orientation) {
                scope.orientation = "horizontal";
            }

            if (!scope.label) {
                scope.label = scope.$parent.translationPrefix + "." + scope.name + ".label";
            }

            var detailFieldsArray = scope.detailFields.split(",");
            scope.selectedFields = scope.fieldsToSelect.split(",");
            scope.propsFilter = "";// "id: $select.search, name: $select.search";
            scope.fieldNames = [];
            scope.item = {};

            for (var i = 0; i < detailFieldsArray.length; i++) {
                var obj = detailFieldsArray[i];
                scope.fieldNames.push(obj);
                scope.propsFilter += obj + ": $select.search,";
            }

        }

        function setValidators(scope, element, ngModel) {
            if (scope.notNull === "true") {
                ngModel.$validators.notNull = function (modelValue, viewValue) {
                    var value = modelValue || viewValue;
                    //var isValid = angular.isObject(value);
                    var isValid = angular.isDefined(value);

                    ngModel.$setValidity('NotNull', isValid);

                    if (!isValid && ngModel.$touched) {
                        element.addClass('has-error');
                    }
                    else {
                        element.removeClass('has-error');
                    }
                    return isValid;
                };

            }

        }

        function setWatchers(scope, ngModel) {
            scope.$watch('item.selected', function (newVal, oldVal) {
                if (!angular.equals(newVal, oldVal)) {
                    $timeout(function () {
                        ngModel.$setTouched();

                        if (scope.bindableField != undefined && typeof newVal !== 'string' && Object.keys(newVal).length > 0) {
                            ngModel.$setViewValue(newVal[scope.bindableField]);
                        } else {
                            ngModel.$setViewValue(newVal);
                        }
                    });
                }
            });

            scope.$watch('sourceItems', function (newVal) {
                scope.data = newVal;
            });

            scope.$watch('data', function (newVal) {
                if (newVal != undefined) {
                    if (typeof ngModel.$modelValue === 'string') {
                        if (scope.bindableField != undefined) {
                            for (var i = 0; i < newVal.length; i++) {
                                var item = newVal[i];
                                if (item[scope.bindableField] == ngModel.$modelValue) {
                                    scope.item.selected = item;
                                }
                            }
                        }
                    }
                }
            });

            ngModel.$render = function () {
                scope.item.selected = ngModel.$modelValue;
            }
        }

        function setEvents(element, ngModel) {
            angular.element(element).on('blur', 'div.ui-select-container', function () {
                ngModel.$setTouched();
                ngModel.$validate();
            });
        }

        return {
            restrict: "E",
            require: ['ngModel', '?^form'],
            //replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/exelector-directive.html',
            scope: {
                mainType: "@",
                label: "@",
                placeholder: "@",
                name: "@",
                orientation: "@",
                notNull: "@",
                labelSize: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@"
            },
            link: function (scope, element, attr, ctrls) {

                var ngModel = ctrls[0];
                var form = ctrls[1];

                checkForm(form);
                setMainType(scope);
                initialize(scope);
                setValidators(scope, element, ngModel);
                setWatchers(scope, ngModel);
                setEvents(element, ngModel);
                scope.propsFilter = scope.propsFilter.substring(0, scope.propsFilter.length - 1);
            }
        };
    }]);
})(angular.module('lego.base'));
/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive('doAction', ['$timeout', function ($timeout) {

        return function (scope, element, attrs) {
            var split = attrs.doAction.split(":");
            var shortkey = split[0];
            var action = split[1];
            var condition = undefined;
            if (split[1].indexOf('?') >= 0) {
                split = split[1].split("?");
                condition = split[0];
                action = split[1];
            }

            scope.$on("key_down", function (event, args) {
                if (args.keyPressed.toLowerCase() == shortkey.toLocaleLowerCase()) {
                    $timeout(function () {
                        if ((condition != undefined && scope.$eval(condition)) || condition == undefined) {
                            scope.$eval(action);
                        }
                    });
                }
            });
        };
    }]);
})(angular.module("lego.base"));
/**
 * Created by mokaram on 06/26/2016.
 */
(function (module) {

    module.directive("checklist", [function () {
        return {
            restrict: "E",
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkList.html',
            scope: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: "=",
                mainType: "@",
                name: "@",
                model: "=",
                notNull: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@",
                header: "@"
            },
            link: function (scope, attributes) {
                scope.$watch('model', function (newValue, oldValue) {
                    if (typeof newValue === 'string') {
                        if (!find(scope.ngModel, newValue)) {
                            scope.ngModel.push({text: newValue});
                        }
                    }
                });
                scope.translationPrefix = attributes.translationPrefix;
                if (!scope.label) {
                    scope.label = scope.translationPrefix + "." + scope.name + ".label";
                }
                if (!scope.labelSize) {
                    scope.labelSize = "3";
                }
            }
        }
    }
    ]);
    function find(model, newVal) {
        for (var i = 0; i < model.length; i++) {
            if (model[i].text === newVal) {
                return true
            }
        }
        return false
    };
})(angular.module('lego.base'));


/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive('autoFocus', ['$timeout', function ($timeout) {
        return function (scope, element, attrs) {
            scope.$watch(attrs.autoFocus, function (val) {
                if (angular.isDefined(val) && val == 'true') {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
        };
    }]);
})(angular.module("lego.base"));
/**
 * Created by keshvari on 9/8/14.
 */
(function (module) {
    module.directive('uiMasterViewer', ['$timeout', '$filter', '$rootScope', function ($timeout, $filter, $rootScope) {

        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/MasterViewer.html',
            scope: {
                label: "@",
                labelSize: "@",
                translationPrefix: '@',
                orientation: "@",
                model: "=",
                onErrorMessageContent: '@',
                onErrorMessageTitle: '@',
                data: '=',
                filters: '@',
                palceholder: '@',
                ignoreFields: '@'
            },
            link: function (scope, element, attr) {
                scope.$watch('data', function (newValue, oldValue) {
                    if (!angular.isUndefined(scope.data) && scope.data != null && checkDataIsEmpty(scope.data) != 0) {
                        // accepts texts like field1=Filter1:'Param1':'Param2',field2=Filter2:'Param1':'Param2'
                        var filterPropertyExtractor = /((\w+)=(\w+)(:((')(\w|\d)+('))|(\w+))+)/g;
                        //var filterPropertyExtractor = /(\w+):(\w+)[(]*(\w+)(,)*(\w+)*[)]*/g;
                        var propertyExtractor = /(\w+)(?=:)/;
                        var filterWithArgsExtractor = /((?!(\w+):)(\w+)[(]*(\w+)*(,)*(\w)*[)]*)/g;
                        var argumentsWithParenthesisExtractor = /([(])((\w+)+(,)*)*([)])/;
                        var haveArgument = /(\w+)(?![(])((\w+)+(,)*)*(?=[)])/g;
                        var argumentExtractor = /(\w+)/g;
                        var filterNameExtractor = /(\w+)(?=[(])/g;
                        scope.dataList = [];
                        /*contains all propertyNames in json object*/
                        var propertyList = [];
                        var temp = Object.keys(scope.data);
                        for (var i = 0; i < temp.length; i++) {
                            var key = temp[i];
                            if (scope.ignoreFields) {
                                if (scope.ignoreFields.indexOf(key) >= 0) {
                                    continue;
                                }
                            }

                            if (key.indexOf('$') != 0) {
                                propertyList.push(key);
                            }
                        }
                        scope.showPlaceHolder = false;
                        if (attr.filters) {
                            for (var s = 0; s < propertyList.length; s++) {
                                var text = scope.translationPrefix + '.' + propertyList[s] + '.label';
                                scope.dataList.push({
                                    label: $rootScope.translate(text),
                                    content: scope.data[propertyList[s]]
                                });
                            }

                            var tokens = attr.filters.split(",");
                            for (var j = 0; j < tokens.length; j++) {
                                var token = tokens[j];
                                var operands = token.split("=");
                                var fieldName = operands[0];
                                var others = operands[1];
                                var filters = others.split(":");
                                var filterName = filters[0];
                                var indexOfItem = propertyList.indexOf(fieldName);
                                var parameters = [];
                                parameters.push(scope.dataList[indexOfItem].content);
                                parameters = parameters.concat(filters.slice(1));
                                scope.dataList[indexOfItem].content = $filter(filterName).apply(this, parameters);
                                parameters.length = 0;
                            }

                            //for(var i = 0; i < propertyList.length; i++) {
                            //    var txt = scope.translationPrefix + '.' + propertyList[i] + '.label';
                            //    scope.dataList.push(
                            //        {
                            //            label: $rootScope.translate(txt),
                            //            content: scope.data[propertyList[i]]
                            //        }
                            //    );
                            //}
                            ///*if filter should be applied*/
                            //var propertyFilterPair = scope.filters.match(filterPropertyExtractor);
                            //for (var i = 0; i < propertyFilterPair.length; i++) {
                            //    var pairItem = propertyFilterPair[i];
                            //    var propertyName = pairItem.match(propertyExtractor);
                            //    if(propertyList.indexOf(propertyName[i])){
                            //        var filterPart = pairItem.match(filterWithArgsExtractor);
                            //        var filterName;
                            //        var listOfArguments = [];
                            //        if(haveArgument.test(filterPart[0])){
                            //            var argumentsWithParenthesis = filterPart[0].match(argumentsWithParenthesisExtractor);
                            //            listOfArguments  = argumentsWithParenthesis[0].match(argumentExtractor);
                            //            filterName = pairItem.match(filterNameExtractor)[0];
                            //        }else{
                            //            filterName = filterPart[0];
                            //        }
                            //        /*trying to find the item in list to applying the filter on it*/
                            //        var indexOfItem = propertyList.indexOf(propertyName[i]);
                            //        listOfArguments.unshift(scope.dataList[indexOfItem].content);
                            //        scope.dataList[indexOfItem].content = $filter(filterName).apply(this, listOfArguments);
                            //
                            //
                            //    }else{
                            //        console.warn( i +"th property in filters  is not found in results");
                            //    }
                            //}
                        } else {
                            /*if filter not used*/
                            for (var i = 0; i < propertyList.length; i++) {
                                var txt = scope.translationPrefix + '.' + propertyList[i] + '.label';
                                scope.dataList.push(
                                    {
                                        label: $rootScope.translate(txt),
                                        content: scope.data[propertyList[i]]
                                    }
                                );

                            }

                        }
                        $timeout(function () {
                            scope.$apply();
                        })

                    } else {
                        scope.showPlaceHolder = true;
                    }

                });
            }
        }
    }]);
})(angular.module('lego.base'));

var checkDataIsEmpty = function (toBeCkecked) {
    var counter = 0;
    for (var key in toBeCkecked) {
        ++counter;
    }
    return counter;
};
/**
 * Created by mokaram on 07/17/2016.
 */
(function (module) {

    module.directive('datePicker', [function () {

        function updateModel(ngModel, scope) {
            return function (event) {
                ngModel.$setViewValue(event);
            }
        }

        function setupPicker(scope, element, attrs, ngModel) {
            var control = element.find('input').first().persianDatepicker({
                observer: true,
                //inputDelay: attrs.updateDelay || 800,
                onSelect: updateModel(ngModel),
                format: 'YYYY/MM/DD',
                timePicker: {
                    enabled: false
                }
            });

            element.find('span').click(function () {
                clearValue(control, ngModel);
            });

            control.blur(function () {
                ngModel.$setTouched();
                scope.$apply();
            });
            return control
        }

        function setDate(control, ngModel) {
            control.persianDatepicker('setDate', ngModel.$viewValue);
        }

        function clearValue(control, ngModel) {
            ngModel.$setViewValue(null);
            control.val('');
        }

        return {
            transclude: false,
            restrict: 'E',
            template: '<input type="text" ng-model="sample" ui-mask="9999/99/99" ui-mask-placeholder ui-mask-placeholder-char="_" >',
            //'<span class="input-group-addon"><i class="icon-reload icons"></i></span>',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (angular.isUndefined(ngModel)) return;

                var control = setupPicker(scope, element, attrs, ngModel);

                ngModel.$render = function () {
                    if (angular.isUndefined(ngModel.$viewValue)) {
                        clearValue(control, ngModel);
                        return
                    }
                    if (typeof ngModel.$viewValue === 'string') {
                        control.persianDatepicker('setDate', new Date(ngModel.$viewValue));
                    }
                    else {
                        control.persianDatepicker('setDate', ngModel.$viewValue);
                    }
                }
            }
        }
    }]);
})(angular.module('lego.base'));
(function (module) {

    module.controller("PrintController", PrintController);

    PrintController.$inject = ["url", 'model', "FileUtils", "$http", "$translate"];

    function PrintController(url, Model, FileUtils, $http, $translate) {
        var self = this;

        if (url != undefined) {
            self.url = url
        }
        if (Model != undefined) {
            self.Model = Model;
        }
        self.Actions = Actions;

        function Actions() {
            return {
                report: function (x) {
                    var i = self.url + x + "/" + $translate.use();
                    $http.post(i, self.Model, {responseType: "arraybuffer"}).then(function (e) {
                        FileUtils.saveData(e.data, "Report." + x, FileUtils.getMimeType(x))
                    })
                }
            }
        }
    }

})(angular.module("lego.base"));
(function (module) {
    module.controller('LegoController', LegoController);

    LegoController.$inject = ['Storage', 'FormActions'];

    function LegoController(Storage, FormActions) {

        var self = this;


        self.Actions = {
            togglePanelStretch: function () {
                self.panelStretched = !self.panelStretched;
                Storage.writeOnStorage("panelStretched", self.panelStretched);
            },
            searchForm: function () {
                self.operation = "SEARCH";
            },
            createForm: function () {
                self.operation = "CREATE";
            },
            editForm: function () {
                self.operation = "EDIT";
            },
            showForm: function () {
                self.operation = "SHOW";
            }
        };

        FormActions.subscribe(function (msg) {
            self.operation = msg;
        });

        self.$onInit = function () {
            var value = Storage.readStorageValue("panelStretched");
            self.panelStretched = value == undefined ? false : value != 'false';
            self.operation = "SEARCH";
        };

        $('link[data-direction]').each(function () {
            var $this = $(this);
            $this.attr('rel', null);
            if ($this.attr('data-direction') == "rtl") {
                $this.attr('rel', 'stylesheet');
            }
        });
    }
})(angular.module("lego.base"));
(function (module) {
    module.constant("LegoConstants", {
        printButtons: [
            {type: "pdf", tooltip: "Pdf", imageSrc: "app/resources/images/mime/pdf.png"},
            {type: "docx", tooltip: "Microsoft Word", imageSrc: "app/resources/images/mime/docx.png"},
            {type: "html", tooltip: "Html", imageSrc: "app/resources/images/mime/html.png"},
            {type: "xlsx", tooltip: "Microsoft excel", imageSrc: "app/resources/images/mime/xlsx.png"},
            {type: "csv", tooltip: "CSV", imageSrc: "app/resources/images/mime/txt.png"}
        ]
    })
})(angular.module("lego.base"));
(function (module) {
    module.component('splWaiting', SplWaitingConfig());

    function SplWaitingConfig() {
        return {
            templateUrl: "/app/modules/base/templates/waiting.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));
(function (module) {
    module.component("splValidationMessages", {
            templateUrl: "/app/modules/base/templates/validationMessages.html",
        transclude: true,
            require: {form: "^form"},
            bindings: {
                name: "@"
            },
            controller: Controller
        }
    );
    Controller.$inject = ['Utils'];
    function Controller(Utils) {
        var self = this;


        self.$onInit = function () {
            self.form = this.form;
        };

        self.interacted = interacted;
        self.format = format;
        self.translate = Utils.translate;


        function format(fmt, arr) {
            return fmt.replace(/{(\d+)}/g, function (match, number) {
                return typeof arr[number] != 'undefined' ? arr[number] : match;
            });
        }

        function interacted(field) {
            return !self.form.saved && (field.$dirty || self.form.submitted);
        }
    }

})(angular.module("lego.base"));
(function (module) {
    module.component("splTopNavbar", splTopNavbar());

    function splTopNavbar() {
        return {
            templateUrl: "/app/modules/base/templates/topnavbar.html",
            transclude: true,
            require: {parent: '^ngController'},
            controller: Controller
        };
        function Controller() {
            var self = this;
            self.$onInit = function () {
                self.parent = this.parent;
            }
        }
    }


})(angular.module("lego.base"));
(function (module) {

    module.component("splTestableBody", splTestableBodyConfig());

    function splTestableBodyConfig() {
        return {
            templateUrl: "/app/modules/base/templates/testable-body.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    Controller.$inject = [];
    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));
/**
 * Created by mokaram on 07/16/2016.
 */

(function (module) {

    module.component("splYearPicker", YearPicker());

    function YearPicker() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/yearPicker.html',
            require: {ngModel: '?ngModel', parent: "^splTranslationPrefix"},
            bindings: {
                ngModel: "=",
                header : "@"
            },
            controller: Controller
        }
    }

    function Controller($element) {
        var self = this;
        var element = $element;

        self.$onInit = function () {
            if (angular.isUndefined(self.ngModel)) return;
            var control = setupPicker(element, self.ngModel);
            self.ngModel.$render = function () {
                if (angular.isUndefined(self.ngModel.$viewValue)) {
                    clearValue(control, self.ngModel);
                    return
                }
                control.persianDatepicker('setDate', self.ngModel.$viewValue);
            }
        }
    }

    function updateModel(ngModel) {
        return function (event) {
            ngModel.$setViewValue(event);
        }
    }

    function setupPicker(element, ngModel) {
        var control = element.find('input').persianDatepicker({
            format: 'YYYY',
            observer:true,
            dayPicker: {
                enabled: false
            },
            monthPicker: {
                enabled: false
            },
            yearPicker: {
                enabled: true,
                onSelect: updateModel(ngModel)
            }
        });

        element.find('span').click(function () {
            clearValue(control, ngModel);
        });

        //control.blur(function(){
        //    ngModel.$setTouched();
        //    scope.$apply();
        //});

        return control
    }

    function setDate(control, ngModel) {
        control.persianDatepicker('setDate', ngModel.$viewValue);
    }

    function clearValue(control, ngModel) {
        ngModel.$setViewValue(null);
        control.val('');
    }

})(angular.module('lego.base'));
/**
 * Created by mokaram on 07/17/2016.
 */
(function (module) {

    module.component("splTimePicker", TimePicker());

    function TimePicker() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/timePicker.html',
            require: {ngModel: '?ngModel', parent: "^splTranslationPrefix"},
            bindings: {
                ngModel: "=",
                header : "@"
            },
            controller: Controller
        }
    }

    function Controller($element) {
        var self = this;
        var element = $element;

        self.$onInit = function () {
            if (angular.isUndefined(self.ngModel)) return;
            var control = setupPicker(element, self.ngModel);
            self.ngModel.$render = function () {
                if (angular.isUndefined(self.ngModel.$viewValue)) {
                    clearValue(control, self.ngModel);
                    return
                }
                control.persianDatepicker('setDate', self.ngModel.$viewValue);
            }
        }
    }

    function updateModel(ngModel) {
        return function (event) {
            ngModel.$setViewValue(event);
        }
    }

    function setupPicker(element, ngModel) {
        var control = element.find('input').persianDatepicker({
            onSelect: updateModel(ngModel),
            observer:true,
            format: "HH:mm:ss a",
            onlyTimePicker: true,
        });

        element.find('span').click(function () {
            clearValue(control, ngModel);
        });

        //control.blur(function(){
        //    ngModel.$setTouched();
        //    scope.$apply();
        //});

        return control
    }

    function setDate(control, ngModel) {
        control.persianDatepicker('setDate', ngModel.$viewValue);
    }

    function clearValue(control, ngModel) {
        ngModel.$setViewValue(null);
        control.val('');
    }

})(angular.module('lego.base'));
(function (module) {
    module.component('splTextarea', SplTextareaConfig());

    function SplTextareaConfig() {
        return {
            templateUrl: '/app/modules/base/templates/textarea.html',
            transclude: false,
            bindings: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: "=",
                disabled: "=",
                notNull: "=",
                tailable: "="
            },
            require: {parent: "^splTranslationPrefix"},
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }
            if (!self.labelSize) {
                self.labelSize = "3";
            }

        }
    }
})(angular.module("lego.base"));
/**
 * Created by mokaram on 07/16/2016.
 */
(function (module) {

    module.component("splMonthPicker", MonthPicker());

    function MonthPicker() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/monthPicker.html',
            require: {ngModel: '?ngModel', parent: "^splTranslationPrefix"},
            bindings: {
                ngModel: "=",
                header : "@"
            },
            controller: Controller
        }
    }

    function Controller($element) {
        var self = this;
        var element = $element;

        self.$onInit = function () {
            if (angular.isUndefined(self.ngModel)) return;
            var control = setupPicker(element, self.ngModel);
            self.ngModel.$render = function () {
                if (angular.isUndefined(self.ngModel.$viewValue)) {
                    clearValue(control, self.ngModel);
                    return
                }
                control.persianDatepicker('setDate', self.ngModel.$viewValue);
            }
        }
    }

    function updateModel(ngModel) {
        return function (event) {
            ngModel.$setViewValue(event);
        }
    }

    function setupPicker(element, ngModel) {
        var control = element.find('input').persianDatepicker({
            format: " MMMM YYYY",
            observer: true,
            yearPicker: {
                enabled: false
            },
            monthPicker: {
                enabled: true,
                onSelect: updateModel(ngModel)
            },
            dayPicker: {
                enabled: false
            }
        });

        element.find('span').click(function () {
            clearValue(control, ngModel);
        });

        //control.blur(function(){
        //    ngModel.$setTouched();
        //    scope.$apply();
        //});

        return control
    }

    function setDate(control, ngModel) {
        control.persianDatepicker('setDate', ngModel.$viewValue);
    }

    function clearValue(control, ngModel) {
        ngModel.$setViewValue(null);
        control.val('');
    }

})(angular.module('lego.base'));
/**
 *         @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 7/24/14
 *         Time: 5:54 PM
 */
(function (module) {

    module.component("splInputText", InputText());

    function InputText() {
        return {
        templateUrl: '/app/modules/base/templates/inputText.html',
        require: {parent: "^splTranslationPrefix"},
        bindings: {
            type: "@",
            label: "@",
            labelSize: "@",
            placeholder: "@",
            name: "@",
            notNull: "=",
            ngModel: "=",
            disabled: "=",
            typeAhead: "@",
            autofocus: "=",
            tooltip: "@"
        },
        controller: Controller

        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;

            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }

            if (!self.labelSize) {
                self.labelSize = "3";
            }
            if (!self.orientation) {
                self.orientation = "horizontal";
            }
        };

        // todo uncomment
        // self.$onChanges = function ($element) {
        //     if (self.typeAhead != undefined) {
        //         self.getItems = function (viewValue) {
        //             return self.parent.Action.searchColumn(self.typeAhead, viewValue);
        //         };
        //     }
        // };
        // self.$postLink = function ($element) {
        //
        // }
    }

})(angular.module('lego.base'));
(function (module) {
    module.component('splInputRadio', SplInputRadio());

    function SplInputRadio() {
        return {
            templateUrl: '/app/modules/base/templates/radio.html',
            transclude: false,
            bindings: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: '=',
                disabled: "="
            },
            require: {parent: "^splTranslationPrefix"},
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));
/**
 * Created by mokaram on 07/09/2016.
 */

(function (module) {

    module.component("splInputChecklist", InputChecklist());

    function InputChecklist() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkList-comp.html',
            require: {parent: "^splTranslationPrefix"},
            bindings: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: "=",
                mainType: "@",
                name: "@",
                model: "=",
                notNull: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@",
                header: "@"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;

        self.$onInit = function () {
            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }
            if (!self.labelSize) {
                self.labelSize = "3";
            }
            setWatchers(self);

        };

        function setWatchers(self) {

            addProperty(self, "model", addModel);

            function addModel(value) {
                if(typeof value ==='string'){
                    if (!find(self.ngModel, value)) {
                        self.ngModel.push({text: value});
                    }
                }
            }
        }
    }

    function find(model, newVal) {
        for (var i = 0; i < model.length; i++) {
            if (model[i].text === newVal) {
                return true
            }
        }
        return false
    }

    function addProperty(self, name, setter) {
        var initValue = self[name];
        Object.defineProperty(self, name, {
            get: function () {
                return self["_" + name];
            },
            set: function (value) {
                setter(value);
                self["_" + name] = value;
            },
            enumerable: true,
            configurable: true
        });

        self[name] = initValue;
    }
})(angular.module('lego.base'));


/**
 * Created by mokaram on 06/26/2016.
 */

(function (module) {

    module.component("splInputCheckbox", InputCheckbox());

    function InputCheckbox() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkbox.html',
            require: {parent: "^splTranslationPrefix"},
            bindings: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: '=',
                disabled: "@",
                selection: "@",
                visible: "@",
                type: "@"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            if (!self.labelSize) {
                self.labelSize = "3";
            }

        }
    }
})(angular.module('lego.base'));
/**
 *         @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/30/16
 *         Time: 8:48 PM
 */

(function (module) {

    module.component("splExelector", exelector());

    function exelector() {
        return {
            require: {ngModel: 'ngModel', parent: "^splTranslationPrefix"},
            transclude: false,
            templateUrl: '/app/modules/base/templates/exelector.html',
            bindings: {
                mainType: "@",
                label: "@",
                placeholder: "@",
                name: "@",
                notNull: "@",
                labelSize: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@"
            },
            controller: Controller
        }
    }

    Controller.$inject = ['$injector'];

    function Controller($injector) {
        var self = this;

        self.$onInit = function () {
            self.ngModel = this.ngModel;
            setMainType(self);
            initialize(self);
            setWatchers(self, this.ngModel);
            self.propsFilter = self.propsFilter.substring(0, self.propsFilter.length - 1);
        };

        function setMainType(self) {
            self.data = [];
            if (self.mainType != undefined) {
                if (self.detailFields == undefined) {
                    self.detailFields = "description";
                }

                self.fieldsToSelect = "description";
                self.bindableField = "code";

                var gpService = $injector.get('GeneralParameterRestService');
                gpService.search({mainType: self.mainType}).$promise.then(
                    function (success) {
                        self.data = success;
                    }
                );
            } else {
            self.data = self.sourceItems;
            }
        }

        function initialize(self) {
            if (!self.labelSize) {
                self.labelSize = "3";
            }
            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }

            var detailFieldsArray = self.detailFields.split(",");
            self.selectedFields = self.fieldsToSelect.split(",");
            self.propsFilter = "";// "id: $select.search, name: $select.search";
            self.fieldNames = [];
            self.item = {};

            for (var i = 0; i < detailFieldsArray.length; i++) {
                var obj = detailFieldsArray[i];
                self.fieldNames.push(obj);
                self.propsFilter += obj + ": $select.search,";
            }
        }

        function setWatchers(self, ngModel) {
            addProperty(self, "data", dataSetter);
            addProperty(self, "sourceItems", sourceItemsSetter);
            addProperty(self.item, "selected", selectedSetter);

            function selectedSetter(value) {
                if (self.bindableField != undefined && value != undefined && typeof value !== 'string' && Object.keys(value).length > 0) {
                    ngModel.$setViewValue(value[self.bindableField]);
                } else {
                    ngModel.$setViewValue(value);
                }
            }

            function dataSetter(value) {
                if (typeof ngModel.$modelValue === 'string') {
                    if (self.bindableField != undefined) {
                        for (var i = 0; i < value.length; i++) {
                            var item = value[i];
                            if (item[self.bindableField] == ngModel.$modelValue) {
                                self.item.selected = item;
                            }
                        }
                    }
                }
            }

            function sourceItemsSetter(value) {
                self.data = value;
            }

            ngModel.$render = function () {
                self.item.selected = ngModel.$modelValue;
            }
        }
    }

    function addProperty(self, name, setter) {
        var initValue = self[name];
        Object.defineProperty(self, name, {
            get: function () {
                return self["_" + name];
            },
            set: function (value) {
                setter(value);
                self["_" + name] = value;
            },
            enumerable: true,
            configurable: true
        });
        self[name] = initValue;
    }
})(angular.module('lego.base'));
/**
 * Created by mokaram on 07/13/2016.
 */

(function (module) {

    module.component("splDateTimePicker", DateTimePicker());

    function DateTimePicker() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/dateTimePicker.html',
            require: {ngModel: '?ngModel', parent: "^splTranslationPrefix"},
            bindings: {
                ngModel: "=",
                header: "@"
            },
            controller: Controller
        }
    }

    function Controller($element) {

        var self = this;
        var element = $element;
        var x = element.find('input')[0];



        self.$onInit = function () {
            if (angular.isUndefined(self.ngModel)) {
                return
            }

            var control = setupPicker(element, self.ngModel);
            //x.onkeydown = updateModel(self.ngModel);

            self.ngModel.$render = function () {
                if (angular.isUndefined(self.ngModel.$viewValue)) {
                    clearValue(control, self.ngModel);
                    return;
                }

                control.persianDatepicker('setDate', self.ngModel.$viewValue);

            };


        };
    }

    function updateModel(ngModel) {
        return function () {
            ngModel.$setViewValue(this.state.selected.unixDate);
        }
    }

    function setupPicker(element, ngModel) {
        var control = element.find('input').first().persianDatepicker({
            observer: true,
            onSelect: updateModel(ngModel),
            format: 'YYYY MM DD HH:mm:ss',
            persianDigit: false,
            timePicker: {
                enabled: true
            },
            //autoClose: true
        });

        element.find('span').click(function () {
            clearValue(control, ngModel);
        });

        //control.blur(function(){
        //    ngModel.$setTouched();
        //    scope.$apply();
        //});

        return control
    }

    function clearValue(control, ngModel) {
        ngModel.$setViewValue(null);
        control.val('');
    }

    function setDate(control, ngModel) {
        control.persianDatepicker('setDate', ngModel.$viewValue);
    }

    //function addProperty(self, name, setter) {
    //    var initialValue = self[name];
    //    Object.defineProperty(self, name, {
    //        get: function () {
    //            return self["_" + name];
    //        },
    //        set: function (value) {
    //            setter(value);
    //            self["_" + name] = value;
    //        },
    //        enumerable: true,
    //        configurable: true
    //    });
    //    self[name] = initialValue;
    //}
})(angular.module('lego.base'));
/**
 * Created by mokaram on 07/13/2016.
 */

(function (module) {

    module.component("splDatePicker", DatePicker());
    function DatePicker() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/datePicker.html',
            require: {ngModel: '?ngModel', parent: "^splTranslationPrefix"},
            bindings: {
                ngModel: "=",
                header : "@"
            },
            controller: Controller
        }
    }

    function Controller($element) {
        var self = this;
        var element = $element;

        self.$onInit = function () {
            if (angular.isUndefined(self.ngModel)) return;

            var control = setupPicker(element , self.ngModel);

            self.ngModel.$render = function () {
                if (angular.isUndefined(self.ngModel.$viewValue)) {
                    clearValue(control, self.ngModel);
                    return
                }
                if (typeof self.ngModel.$viewValue === 'string') {
                    control.persianDatepicker('setDate', new Date(self.ngModel.$viewValue));
                }
                else {
                    control.persianDatepicker('setDate', self.ngModel.$viewValue);
                }
            }

        }
    }

    function updateModel(ngModel) {
        return function (event) {
            ngModel.$setViewValue(event);
        }
    }

    function setupPicker(element, ngModel) {
        var control = element.find('input').first().persianDatepicker({
            observer: true,
            //inputDelay: attrs.updateDelay || 800,
            onSelect: updateModel(ngModel),
            format: 'YYYY/MM/DD',
            timePicker: {
                enabled: false
            }
        });

        element.find('span').click(function () {
            clearValue(control, ngModel);
        });

        //control.blur(function () {
        //    ngModel.$setTouched();
        //    scope.$apply();
        //});

        return control
    }

    function setDate(control, ngModel) {
        control.persianDatepicker('setDate', ngModel.$viewValue);
    }

    function clearValue(control, ngModel) {
        ngModel.$setViewValue(null);
        control.val('');
    }

})(angular.module('lego.base'));
(function (module) {
    module.component("splShowPage", {
        templateUrl: "/app/modules/base/templates/showpage.html",
        transclude: true,
        require: {
            parent: "^ngController",
            popup: "^splPopup"
        },
        controller: Controller
    });

    function Controller() {
        var self = this;

        self.cancel = function () {
            self.popup.controller.Actions.cancel();
        };
    }
})(angular.module("lego.base"));
(function (module) {

    module.component("splSearchPage", SearchPage());

    function SearchPage() {
        return {
            templateUrl: "/app/modules/base/templates/searchpage.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            self.translationPrefix = self.parent.translationPrefix;
            self.titleKey = "menu." + self.translationPrefix;
        }
    }
})(angular.module("lego.base"));
(function (module) {

    module.component("splPopup", splPopup());

    function splPopup() {
        return {
            templateUrl: "/app/modules/base/templates/popup.html",
            transclude: true,
            controller: Controller,
            bindings: {
                controller: "="
            }
        }
    }

    function Controller() {
        var self = this;
    }

})(angular.module("lego.base"));
(function (module) {

    module.component('splPanel', splPanel());

    function splPanel() {
        return {
            transclude: true,
            templateUrl: '/app/modules/base/templates/panel.html',
            bindings: {
                title: '@',
                type: '@',
                collapsible: "=",
                visible: "@",
                disabled: "="
            },
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            if (angular.isUndefined(self.collapsible)) {
                self.collapsible = "false";
            }
            if (angular.isUndefined(self.type)) {
                self.type = "default";
            }
            self.isOpen = self.collapsible == "true";
            self.visible = angular.isUndefined(self.visible) ? true : self.visible;
            self.disabled = angular.isUndefined(self.disabled) ? false : self.disabled;

            //$timeout(function () {
            //    $scope.$apply(function () {
            //        $scope.visible = angular.isUndefined($scope.visible) ? true : $scope.visible;
            //        $scope.disabled = angular.isUndefined($scope.disabled) ? false : $scope.disabled;
            //    });
            //});
        }
    };

})(angular.module('lego.base'));

(function (module) {
    module.component("splMainPage", splMainPage());

    function splMainPage() {
        return {
            templateUrl: "/app/modules/base/templates/mainpage.html",
            transclude: true,
            require: {parent: "^ngController"},
            controller: Controller
        };
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        };

        self.find = function (form) {
            form.submitted = true;
            if (Object.keys(form.$error) <= 0) {
                self.parent.Actions.search()
            }
        }
    }

})(angular.module("lego.base"));
(function (module) {
    module.component('splFooter', SplFooter());

    function SplFooter() {
        return {
            templateUrl: "/app/modules/base/templates/footer.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));
(function (module) {
    module.component('splEditPage', EditPage());

    function EditPage() {
        return {
            templateUrl: "/app/modules/base/templates/editpage.html",
            transclude: true,
            require: {
                parent: "^ngController",
                popup: "^splPopup"
            },
            controller: Controller

        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            self.popup = this.popup;
        };

        self.insert = function (form) {
            form.submitted = true;
            form.saved = false;
            if (Object.keys(form.$error) <= 0) {
                self.popup.controller.Actions.insert();
                form.saved = true;
                form.submitted = false;
            }
        };
    }
})(angular.module("lego.base"));
(function (module) {
    module.component("splCreatePage", CreatePage());

    function CreatePage() {
        return {
            templateUrl: "/app/modules/base/templates/createpage.html",
            transclude: true,
            require: {parent: "^ngController"},
            controller: Controller
        }
    }
    

    function Controller() {
        var self = this;


        self.insert = function (form) {
            form.submitted = true;
            form.saved = false;
            if (Object.keys(form.$error) <= 0) {
                self.parent.Actions.create();
                form.saved = true;
                form.submitted = false;
            }
        };
    }
})(angular.module("lego.base"));
(function (module) {

    module.component("splBody", splBody());

    function splBody() {
        return {
            templateUrl: "/app/modules/base/templates/body.html",
            transclude: true,
            require: {parent: "^ngController"},
            controller: Controller
        }
    }

    Controller.$inject = ['FormActions'];

    function Controller(FormActions) {
        var self = this;
        self.getActions = FormActions.getActions;
        self.signalAction = FormActions.signalAction;

        self.$onInit = function () {
            self.parent = this.parent;
        }
    }

})(angular.module("lego.base"));
(function (module) {

    module.config(Configure);

    Configure.$inject = ['$httpProvider', '$translateProvider', 'toastrConfig','tagsInputConfigProvider'];

    function Configure($httpProvider, $translateProvider, toastrConfig, tagsInputConfigProvider) {
        $translateProvider.preferredLanguage('fa_IR');

        $httpProvider.interceptors.push('HttpInterceptor');

        angular.extend(toastrConfig, {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });

        tagsInputConfigProvider.setDefaults('tagsInput', { placeholder: '' });
        tagsInputConfigProvider.setActiveInterpolation('tagsInput', { placeholder: true });
    }

})(angular.module('lego.base'));