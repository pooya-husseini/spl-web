var appPath = 'app/';
var client = './src/';
var scriptPath = client + '/scripts';
var port = 8080;
var config = {
    buildPath: '',
    styles: [],
    indexPage: client + "index.html",
    browserSync: {
        proxy: 'localhost:' + port,
        port: 3000,
        files: ['**/*.*'],
        ghostMode: { // these are the defaults t,f,t,t
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        logLevel: 'debug',
        logPrefix: 'gulp-patterns',
        notify: true,
        reloadDelay: 1000
    },
    // bower: {
    //     json: require('./bower.json'),
    //     directory: client+'vendors',
    //     ignorePath: './../../'
    // },
    mainFiles: {
        cssFiles: [
            client + "app/css/*.css"
        ],
        templateFiles: [
            client + "app/te/**/*"
        ],
        resourceFiles: [
            client + "app/resources/nasim-regular.woff"
        ]
    },
    jsSources: {
        client: client + "app/**/*.js",
        exclude: ["!/**/app.js", "!/**/all.js"],
        sundry: ['./gulpfile.js', './gulpfile.config.js'],
        sources: [
            client + 'app/**/*.js',
            '!./dist/index.js',
            './dist/templates.js'
        ],
        injectable: [
            scriptPath + '/quick-sidebar.js',
            scriptPath + '/demo.js',
            scriptPath + '/layout.js',
            scriptPath + '/metronic.js',
            client + 'app/**/*.module.js',
            client + 'app/**/*.js',
            '!' + client + '/app/**/all.js',
            '!' + client + '/app/**/app.js',
            '!' + client + '/app/**/utils.js',
            '!' + client + '/app/**/route.js',
            '!' + client + '/app/**/config.js',
        ]
    },
};


module.exports.config = config;
// module.exports.getWireDepOptions = function () {
//     return {
//         bowerJson: config.bower.json,
//         directory: config.bower.directory,
//         ignorePath: config.bower.ignorePath
//     };
// };