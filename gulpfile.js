var gulp = require('gulp');
var gulpConfig = require('./gulp.config');
var $ = require('gulp-load-plugins')({lazy: true});
var concat = require('gulp-concat');
var angularFilesort = require('gulp-angular-filesort'),
    inject = require('gulp-inject');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var copy = require('gulp-contrib-copy');
var templateCache = require('gulp-angular-templatecache');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var mustache = require("gulp-mustache");
var through = require('through2');
var StringDecoder = require('string_decoder').StringDecoder;
var rename = require("gulp-rename");
var template = require('gulp-template');
var fs = require('fs');
var jslint = require('gulp-jslint');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var docco = require("gulp-docco");
var del = require('del');
var gulpSequence = require('gulp-sequence');
var git = require('gulp-git');


// gulp.task('clean-css', function () {
//     del(gulpConfig.config.buildPath + '*.css');
// });
// gulp.task('clean-js', function () {
//     del(gulpConfig.config.buildPath + '*.js');
// });
// gulp.task('clean', ['clean-css', 'clean-js']);

gulp.task('copyResources', function () {
    var target = gulp.src('./src/main/resources/static/index.html');
    var sources = gulp.src(['./src/**/*.js', './src/**/*.css'], {read: false});

    return target.pipe(inject(sources))
        .pipe(inject(gulp.src('./src/**/*.js').pipe(angularFilesort()), {
            ignorePath: '/src/main/resources/static/',
            addRootSlash: false
        })).pipe(angularFilesort()
            .pipe(gulp.dest('./src/main/resources/static/')));
});

gulp.task('inject', function () {
    // var wiredep = require('wiredep').stream;
    var angularFilesort = require('gulp-angular-filesort');
    // var option = gulpConfig.getWireDepOptions();

    return gulp
        .src(gulpConfig.config.indexPage)
        // .pipe(wiredep(option))
        .pipe($.inject(
            gulp.src(gulpConfig.config.jsSources.injectable)
                .pipe(angularFilesort()), {ignorePath: 'src/main/resources/static/', addRootSlash: false}))
        // .pipe(gulp.dest('./src/main/resources/static/'));
        .pipe(gulp.dest('./src/main/resources/static/'));
});

gulp.task('makeTemplateCache', function () {
    return gulp.src('./src/app/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(templateCache("templates.js",
            {
                module: "lego.base",
                templateBody: '$templateCache.put("/app/<%= url %>","<%= contents %>");'
            }
        ))
        .pipe(gulp.dest('./dist/'));
});
gulp.task('copyResourceFiles', function () {
    return gulp.src(gulpConfig.config.mainFiles.resourceFiles)
        .pipe(copy()).pipe(gulp.dest("./dist/css"));
});

gulp.task('copyTemplateFiles', function () {
    return gulp.src(gulpConfig.config.mainFiles.templateFiles)
        .pipe(copy()).pipe(gulp.dest("./dist/te/"));
});
// gulp.task('cleanupJsFiles', ['concatJsFiles'], function () {
//     // return gulp.src('./dist/templates.js')
//     //     .pipe(clean());
// });
gulp.task('concatJsFiles', function () {


    return gulp.src(gulpConfig.config.jsSources.sources)
        .pipe(angularFilesort())
        .pipe(concat('index.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./dist/'));

});
gulp.task('copyCssFiles', function () {
    return gulp.src(gulpConfig.config.mainFiles.cssFiles)
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(copy())
        .pipe(gulp.dest("./dist/css"));
});

gulp.task('clean', function () {
    return gulp.src("./dist/**/.*")
        .pipe(clean());
});

gulp.task('postClean', function () {
    return gulp.src("./dist/templates.js")
        .pipe(clean());
});

gulp.task('gitAdd', function () {
    return gulp.src("./dist/**/*.*")
        .pipe(git.add());
});

gulp.task('release', gulpSequence('clean', ['copyCssFiles', 'copyResourceFiles', 'copyTemplateFiles'], 'makeTemplateCache', 'concatJsFiles', 'postClean', 'gitAdd'));

// gulp.task('release', ['postClean'], function () {
//
// });

gulp.task('jshint', function () {
    return gulp.src('./src/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));

});

gulp.task('doc', function () {
    gulp.src("./src/**/*.js")
        .pipe(docco())
        .pipe(gulp.dest('./documentation-output'));
});

gulp.task('render', function () {

    var decoder = new StringDecoder('utf8');
    gulp.src("./src/app/modules/base/translations/*.json")
        .pipe(through.obj(function (chunk, enc, cb) {
            var contents = decoder.write(chunk.contents).trim();
            var fileNameReg = /(.+)-(.*)\.json/g;
            var fileVars = fileNameReg.exec(chunk.relative);
            console.log(fileVars);
            gulp.src("./src/app/te/mustache/translation.mustache")
                .pipe(mustache({
                    prefix: fileVars[1],
                    translations: contents.substring(1, contents.length - 1),
                    locale: fileVars[2]
                }, {"extension": ".js"}))
                .pipe(rename({
                    basename: fileVars[1] + "-" + fileVars[2]
                }))
                .pipe(gulp.dest("./dist/myFile"));
            cb(null, chunk)
        }));

});


gulp.task('renderRests', function () {
    var decoder = new StringDecoder('utf8');
    gulp.src("./test/*.json")
        .pipe(through.obj(function (chunk, enc, cb) {
            var contents = JSON.parse(decoder.write(chunk.contents).trim());

            var fileName = chunk.relative;

            gulp.src("./src/app/te/mustache/restService.mustache")
                .pipe(mustache({
                    module: contents.module,
                    name: contents.name,
                    url: contents.url,
                    extraOperations: JSON.stringify(contents.extraOperations),
                }, {"extension": ".js"}))
                .pipe(rename({
                    basename: fileName
                }))
                .pipe(gulp.dest("./dist/myFile"));
            cb(null, chunk)
        }));

});

gulp.task('renderCrudServices', function () {
    var decoder = new StringDecoder('utf8');
    gulp.src("./test/*.json")
        .pipe(through.obj(function (chunk, enc, cb) {
            var contents = JSON.parse(decoder.write(chunk.contents).trim());

            var fileName = chunk.relative.replace(".json", "");

            gulp.src("./src/app/te/underscore/crudService.ejs")
                .pipe(template({service: contents}))
                .pipe(rename({
                    basename: fileName,
                    extname: ".js"
                }))
                .pipe(gulp.dest("./dist/myFile"));
            cb(null, chunk)
        }));
});