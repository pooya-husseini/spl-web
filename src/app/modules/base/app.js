(function () {
    'use strict';
    angular.module('lego.base', [
        'ngRoute',
        'ngSanitize',
        'ngAnimate',
        'ngResource',
        'ui.bootstrap',
        'ui.grid',
        'ui.grid.selection',
        'ui.grid.expandable',
        'ui.grid.pagination',
        'ui.grid.exporter',
        'ui.select',
        'ngTagsInput',
        'ui.grid.autoResize',
        'pascalprecht.translate',
        "toastr",
        "ngMessages",
        "ui.router",
        "ngMessages",
        "ui.mask",
        "mgcrea.ngStrap"
    ]);
})();