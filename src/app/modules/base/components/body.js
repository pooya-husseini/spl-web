(function (module) {

    module.component("splBody", splBody());

    function splBody() {
        return {
            templateUrl: "/app/modules/base/templates/body.html",
            transclude: true,
            require: {parent: "^ngController"},
            controller: Controller
        }
    }

    Controller.$inject = ['FormActions'];

    function Controller(FormActions) {
        var self = this;
        self.getActions = FormActions.getActions;
        self.signalAction = FormActions.signalAction;

        self.$onInit = function () {
            self.parent = this.parent;
        }
    }

})(angular.module("lego.base"));