(function (module) {
    module.component("splCreatePage", CreatePage());

    function CreatePage() {
        return {
            templateUrl: "/app/modules/base/templates/createpage.html",
            transclude: true,
            require: {parent: "^ngController"},
            controller: Controller
        }
    }
    

    function Controller() {
        var self = this;


        self.insert = function (form) {
            form.submitted = true;
            form.saved = false;
            if (Object.keys(form.$error) <= 0) {
                self.parent.Actions.create();
                form.saved = true;
                form.submitted = false;
            }
        };
    }
})(angular.module("lego.base"));