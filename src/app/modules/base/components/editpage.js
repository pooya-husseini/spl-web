(function (module) {
    module.component('splEditPage', EditPage());

    function EditPage() {
        return {
            templateUrl: "/app/modules/base/templates/editpage.html",
            transclude: true,
            require: {
                parent: "^ngController",
                popup: "^splPopup"
            },
            controller: Controller

        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            self.popup = this.popup;
        };

        self.insert = function (form) {
            form.submitted = true;
            form.saved = false;
            if (Object.keys(form.$error) <= 0) {
                self.popup.controller.Actions.insert();
                form.saved = true;
                form.submitted = false;
            }
        };
    }
})(angular.module("lego.base"));