(function (module) {
    module.component('splFooter', SplFooter());

    function SplFooter() {
        return {
            templateUrl: "/app/modules/base/templates/footer.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));