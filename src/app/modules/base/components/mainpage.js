(function (module) {
    module.component("splMainPage", splMainPage());

    function splMainPage() {
        return {
            templateUrl: "/app/modules/base/templates/mainpage.html",
            transclude: true,
            require: {parent: "^ngController"},
            controller: Controller
        };
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        };

        self.find = function (form) {
            form.submitted = true;
            if (Object.keys(form.$error) <= 0) {
                self.parent.Actions.search()
            }
        }
    }

})(angular.module("lego.base"));