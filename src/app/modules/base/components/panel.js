(function (module) {

    module.component('splPanel', splPanel());

    function splPanel() {
        return {
            transclude: true,
            templateUrl: '/app/modules/base/templates/panel.html',
            bindings: {
                title: '@',
                type: '@',
                collapsible: "=",
                visible: "@",
                disabled: "="
            },
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            if (angular.isUndefined(self.collapsible)) {
                self.collapsible = "false";
            }
            if (angular.isUndefined(self.type)) {
                self.type = "default";
            }
            self.isOpen = self.collapsible == "true";
            self.visible = angular.isUndefined(self.visible) ? true : self.visible;
            self.disabled = angular.isUndefined(self.disabled) ? false : self.disabled;

            //$timeout(function () {
            //    $scope.$apply(function () {
            //        $scope.visible = angular.isUndefined($scope.visible) ? true : $scope.visible;
            //        $scope.disabled = angular.isUndefined($scope.disabled) ? false : $scope.disabled;
            //    });
            //});
        }
    };

})(angular.module('lego.base'));
