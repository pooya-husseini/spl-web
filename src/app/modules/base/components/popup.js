(function (module) {

    module.component("splPopup", splPopup());

    function splPopup() {
        return {
            templateUrl: "/app/modules/base/templates/popup.html",
            transclude: true,
            controller: Controller,
            bindings: {
                controller: "="
            }
        }
    }

    function Controller() {
        var self = this;
    }

})(angular.module("lego.base"));