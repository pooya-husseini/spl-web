(function (module) {

    module.component("splSearchPage", SearchPage());

    function SearchPage() {
        return {
            templateUrl: "/app/modules/base/templates/searchpage.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            self.translationPrefix = self.parent.translationPrefix;
            self.titleKey = "menu." + self.translationPrefix;
        }
    }
})(angular.module("lego.base"));