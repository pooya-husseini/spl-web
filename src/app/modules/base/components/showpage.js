(function (module) {
    module.component("splShowPage", {
        templateUrl: "/app/modules/base/templates/showpage.html",
        transclude: true,
        require: {
            parent: "^ngController",
            popup: "^splPopup"
        },
        controller: Controller
    });

    function Controller() {
        var self = this;

        self.cancel = function () {
            self.popup.controller.Actions.cancel();
        };
    }
})(angular.module("lego.base"));