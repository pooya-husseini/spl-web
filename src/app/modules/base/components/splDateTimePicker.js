/**
 * Created by mokaram on 07/13/2016.
 */

(function (module) {

    module.component("splDateTimePicker", DateTimePicker());

    function DateTimePicker() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/dateTimePicker.html',
            require: {ngModel: '?ngModel', parent: "^splTranslationPrefix"},
            bindings: {
                ngModel: "=",
                header: "@"
            },
            controller: Controller
        }
    }

    function Controller($element) {

        var self = this;
        var element = $element;
        var x = element.find('input')[0];



        self.$onInit = function () {
            if (angular.isUndefined(self.ngModel)) {
                return
            }

            var control = setupPicker(element, self.ngModel);
            //x.onkeydown = updateModel(self.ngModel);

            self.ngModel.$render = function () {
                if (angular.isUndefined(self.ngModel.$viewValue)) {
                    clearValue(control, self.ngModel);
                    return;
                }

                control.persianDatepicker('setDate', self.ngModel.$viewValue);

            };


        };
    }

    function updateModel(ngModel) {
        return function () {
            ngModel.$setViewValue(this.state.selected.unixDate);
        }
    }

    function setupPicker(element, ngModel) {
        var control = element.find('input').first().persianDatepicker({
            observer: true,
            onSelect: updateModel(ngModel),
            format: 'YYYY MM DD HH:mm:ss',
            persianDigit: false,
            timePicker: {
                enabled: true
            },
            //autoClose: true
        });

        element.find('span').click(function () {
            clearValue(control, ngModel);
        });

        //control.blur(function(){
        //    ngModel.$setTouched();
        //    scope.$apply();
        //});

        return control
    }

    function clearValue(control, ngModel) {
        ngModel.$setViewValue(null);
        control.val('');
    }

    function setDate(control, ngModel) {
        control.persianDatepicker('setDate', ngModel.$viewValue);
    }

    //function addProperty(self, name, setter) {
    //    var initialValue = self[name];
    //    Object.defineProperty(self, name, {
    //        get: function () {
    //            return self["_" + name];
    //        },
    //        set: function (value) {
    //            setter(value);
    //            self["_" + name] = value;
    //        },
    //        enumerable: true,
    //        configurable: true
    //    });
    //    self[name] = initialValue;
    //}
})(angular.module('lego.base'));