/**
 *         @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 1/30/16
 *         Time: 8:48 PM
 */

(function (module) {

    module.component("splExelector", exelector());

    function exelector() {
        return {
            require: {ngModel: 'ngModel', parent: "^splTranslationPrefix"},
            transclude: false,
            templateUrl: '/app/modules/base/templates/exelector.html',
            bindings: {
                mainType: "@",
                label: "@",
                placeholder: "@",
                name: "@",
                notNull: "@",
                labelSize: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@"
            },
            controller: Controller
        }
    }

    Controller.$inject = ['$injector'];

    function Controller($injector) {
        var self = this;

        self.$onInit = function () {
            self.ngModel = this.ngModel;
            setMainType(self);
            initialize(self);
            setWatchers(self, this.ngModel);
            self.propsFilter = self.propsFilter.substring(0, self.propsFilter.length - 1);
        };

        function setMainType(self) {
            self.data = [];
            if (self.mainType != undefined) {
                if (self.detailFields == undefined) {
                    self.detailFields = "description";
                }

                self.fieldsToSelect = "description";
                self.bindableField = "code";

                var gpService = $injector.get('GeneralParameterRestService');
                gpService.search({mainType: self.mainType}).$promise.then(
                    function (success) {
                        self.data = success;
                    }
                );
            } else {
            self.data = self.sourceItems;
            }
        }

        function initialize(self) {
            if (!self.labelSize) {
                self.labelSize = "3";
            }
            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }

            var detailFieldsArray = self.detailFields.split(",");
            self.selectedFields = self.fieldsToSelect.split(",");
            self.propsFilter = "";// "id: $select.search, name: $select.search";
            self.fieldNames = [];
            self.item = {};

            for (var i = 0; i < detailFieldsArray.length; i++) {
                var obj = detailFieldsArray[i];
                self.fieldNames.push(obj);
                self.propsFilter += obj + ": $select.search,";
            }
        }

        function setWatchers(self, ngModel) {
            addProperty(self, "data", dataSetter);
            addProperty(self, "sourceItems", sourceItemsSetter);
            addProperty(self.item, "selected", selectedSetter);

            function selectedSetter(value) {
                if (self.bindableField != undefined && value != undefined && typeof value !== 'string' && Object.keys(value).length > 0) {
                    ngModel.$setViewValue(value[self.bindableField]);
                } else {
                    ngModel.$setViewValue(value);
                }
            }

            function dataSetter(value) {
                if (typeof ngModel.$modelValue === 'string') {
                    if (self.bindableField != undefined) {
                        for (var i = 0; i < value.length; i++) {
                            var item = value[i];
                            if (item[self.bindableField] == ngModel.$modelValue) {
                                self.item.selected = item;
                            }
                        }
                    }
                }
            }

            function sourceItemsSetter(value) {
                self.data = value;
            }

            ngModel.$render = function () {
                self.item.selected = ngModel.$modelValue;
            }
        }
    }

    function addProperty(self, name, setter) {
        var initValue = self[name];
        Object.defineProperty(self, name, {
            get: function () {
                return self["_" + name];
            },
            set: function (value) {
                setter(value);
                self["_" + name] = value;
            },
            enumerable: true,
            configurable: true
        });
        self[name] = initValue;
    }
})(angular.module('lego.base'));