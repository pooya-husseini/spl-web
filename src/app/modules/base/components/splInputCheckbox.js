/**
 * Created by mokaram on 06/26/2016.
 */

(function (module) {

    module.component("splInputCheckbox", InputCheckbox());

    function InputCheckbox() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkbox.html',
            require: {parent: "^splTranslationPrefix"},
            bindings: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: '=',
                disabled: "@",
                selection: "@",
                visible: "@",
                type: "@"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            if (!self.labelSize) {
                self.labelSize = "3";
            }

        }
    }
})(angular.module('lego.base'));