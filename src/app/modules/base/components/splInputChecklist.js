/**
 * Created by mokaram on 07/09/2016.
 */

(function (module) {

    module.component("splInputChecklist", InputChecklist());

    function InputChecklist() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkList-comp.html',
            require: {parent: "^splTranslationPrefix"},
            bindings: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: "=",
                mainType: "@",
                name: "@",
                model: "=",
                notNull: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@",
                header: "@"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;

        self.$onInit = function () {
            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }
            if (!self.labelSize) {
                self.labelSize = "3";
            }
            setWatchers(self);

        };

        function setWatchers(self) {

            addProperty(self, "model", addModel);

            function addModel(value) {
                if(typeof value ==='string'){
                    if (!find(self.ngModel, value)) {
                        self.ngModel.push({text: value});
                    }
                }
            }
        }
    }

    function find(model, newVal) {
        for (var i = 0; i < model.length; i++) {
            if (model[i].text === newVal) {
                return true
            }
        }
        return false
    }

    function addProperty(self, name, setter) {
        var initValue = self[name];
        Object.defineProperty(self, name, {
            get: function () {
                return self["_" + name];
            },
            set: function (value) {
                setter(value);
                self["_" + name] = value;
            },
            enumerable: true,
            configurable: true
        });

        self[name] = initValue;
    }
})(angular.module('lego.base'));

