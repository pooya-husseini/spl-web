(function (module) {
    module.component('splInputRadio', SplInputRadio());

    function SplInputRadio() {
        return {
            templateUrl: '/app/modules/base/templates/radio.html',
            transclude: false,
            bindings: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: '=',
                disabled: "="
            },
            require: {parent: "^splTranslationPrefix"},
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));