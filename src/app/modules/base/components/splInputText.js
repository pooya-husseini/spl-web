/**
 *         @author : Pooya Hosseini
 *         Email : info@pooya-hfp.ir,pooya.husseini@gmail.com
 *         Date: 7/24/14
 *         Time: 5:54 PM
 */
(function (module) {

    module.component("splInputText", InputText());

    function InputText() {
        return {
        templateUrl: '/app/modules/base/templates/inputText.html',
        require: {parent: "^splTranslationPrefix"},
        bindings: {
            type: "@",
            label: "@",
            labelSize: "@",
            placeholder: "@",
            name: "@",
            notNull: "=",
            ngModel: "=",
            disabled: "=",
            typeAhead: "@",
            autofocus: "=",
            tooltip: "@"
        },
        controller: Controller

        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;

            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }

            if (!self.labelSize) {
                self.labelSize = "3";
            }
            if (!self.orientation) {
                self.orientation = "horizontal";
            }
        };

        // todo uncomment
        // self.$onChanges = function ($element) {
        //     if (self.typeAhead != undefined) {
        //         self.getItems = function (viewValue) {
        //             return self.parent.Action.searchColumn(self.typeAhead, viewValue);
        //         };
        //     }
        // };
        // self.$postLink = function ($element) {
        //
        // }
    }

})(angular.module('lego.base'));