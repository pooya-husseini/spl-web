(function (module) {
    module.component('splTextarea', SplTextareaConfig());

    function SplTextareaConfig() {
        return {
            templateUrl: '/app/modules/base/templates/textarea.html',
            transclude: false,
            bindings: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: "=",
                disabled: "=",
                notNull: "=",
                tailable: "="
            },
            require: {parent: "^splTranslationPrefix"},
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
            if (!self.label) {
                self.label = self.parent.translationPrefix + "." + self.name + ".label";
            }
            if (!self.labelSize) {
                self.labelSize = "3";
            }

        }
    }
})(angular.module("lego.base"));