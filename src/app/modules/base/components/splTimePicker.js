/**
 * Created by mokaram on 07/17/2016.
 */
(function (module) {

    module.component("splTimePicker", TimePicker());

    function TimePicker() {
        return {
            transclude: false,
            templateUrl: '/app/modules/base/templates/timePicker.html',
            require: {ngModel: '?ngModel', parent: "^splTranslationPrefix"},
            bindings: {
                ngModel: "=",
                header : "@"
            },
            controller: Controller
        }
    }

    function Controller($element) {
        var self = this;
        var element = $element;

        self.$onInit = function () {
            if (angular.isUndefined(self.ngModel)) return;
            var control = setupPicker(element, self.ngModel);
            self.ngModel.$render = function () {
                if (angular.isUndefined(self.ngModel.$viewValue)) {
                    clearValue(control, self.ngModel);
                    return
                }
                control.persianDatepicker('setDate', self.ngModel.$viewValue);
            }
        }
    }

    function updateModel(ngModel) {
        return function (event) {
            ngModel.$setViewValue(event);
        }
    }

    function setupPicker(element, ngModel) {
        var control = element.find('input').persianDatepicker({
            onSelect: updateModel(ngModel),
            observer:true,
            format: "HH:mm:ss a",
            onlyTimePicker: true,
        });

        element.find('span').click(function () {
            clearValue(control, ngModel);
        });

        //control.blur(function(){
        //    ngModel.$setTouched();
        //    scope.$apply();
        //});

        return control
    }

    function setDate(control, ngModel) {
        control.persianDatepicker('setDate', ngModel.$viewValue);
    }

    function clearValue(control, ngModel) {
        ngModel.$setViewValue(null);
        control.val('');
    }

})(angular.module('lego.base'));