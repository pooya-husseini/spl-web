(function (module) {

    module.component("splTestableBody", splTestableBodyConfig());

    function splTestableBodyConfig() {
        return {
            templateUrl: "/app/modules/base/templates/testable-body.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    Controller.$inject = [];
    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));