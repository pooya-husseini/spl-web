(function (module) {
    module.component("splTopNavbar", splTopNavbar());

    function splTopNavbar() {
        return {
            templateUrl: "/app/modules/base/templates/topnavbar.html",
            transclude: true,
            require: {parent: '^ngController'},
            controller: Controller
        };
        function Controller() {
            var self = this;
            self.$onInit = function () {
                self.parent = this.parent;
            }
        }
    }


})(angular.module("lego.base"));