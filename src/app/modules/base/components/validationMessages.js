(function (module) {
    module.component("splValidationMessages", {
            templateUrl: "/app/modules/base/templates/validationMessages.html",
        transclude: true,
            require: {form: "^form"},
            bindings: {
                name: "@"
            },
            controller: Controller
        }
    );
    Controller.$inject = ['Utils'];
    function Controller(Utils) {
        var self = this;


        self.$onInit = function () {
            self.form = this.form;
        };

        self.interacted = interacted;
        self.format = format;
        self.translate = Utils.translate;


        function format(fmt, arr) {
            return fmt.replace(/{(\d+)}/g, function (match, number) {
                return typeof arr[number] != 'undefined' ? arr[number] : match;
            });
        }

        function interacted(field) {
            return !self.form.saved && (field.$dirty || self.form.submitted);
        }
    }

})(angular.module("lego.base"));