(function (module) {
    module.component('splWaiting', SplWaitingConfig());

    function SplWaitingConfig() {
        return {
            templateUrl: "/app/modules/base/templates/waiting.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    function Controller() {
        var self = this;
        self.$onInit = function () {
            self.parent = this.parent;
        }
    }
})(angular.module("lego.base"));