(function (module) {

    module.config(Configure);

    Configure.$inject = ['$httpProvider', '$translateProvider', 'toastrConfig','tagsInputConfigProvider'];

    function Configure($httpProvider, $translateProvider, toastrConfig, tagsInputConfigProvider) {
        $translateProvider.preferredLanguage('fa_IR');

        $httpProvider.interceptors.push('HttpInterceptor');

        angular.extend(toastrConfig, {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });

        tagsInputConfigProvider.setDefaults('tagsInput', { placeholder: '' });
        tagsInputConfigProvider.setActiveInterpolation('tagsInput', { placeholder: true });
    }

})(angular.module('lego.base'));