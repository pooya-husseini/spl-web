(function (module) {
    module.constant("LegoConstants", {
        printButtons: [
            {type: "pdf", tooltip: "Pdf", imageSrc: "app/resources/images/mime/pdf.png"},
            {type: "docx", tooltip: "Microsoft Word", imageSrc: "app/resources/images/mime/docx.png"},
            {type: "html", tooltip: "Html", imageSrc: "app/resources/images/mime/html.png"},
            {type: "xlsx", tooltip: "Microsoft excel", imageSrc: "app/resources/images/mime/xlsx.png"},
            {type: "csv", tooltip: "CSV", imageSrc: "app/resources/images/mime/txt.png"}
        ]
    })
})(angular.module("lego.base"));