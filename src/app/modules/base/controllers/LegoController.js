(function (module) {
    module.controller('LegoController', LegoController);

    LegoController.$inject = ['Storage', 'FormActions'];

    function LegoController(Storage, FormActions) {

        var self = this;


        self.Actions = {
            togglePanelStretch: function () {
                self.panelStretched = !self.panelStretched;
                Storage.writeOnStorage("panelStretched", self.panelStretched);
            },
            searchForm: function () {
                self.operation = "SEARCH";
            },
            createForm: function () {
                self.operation = "CREATE";
            },
            editForm: function () {
                self.operation = "EDIT";
            },
            showForm: function () {
                self.operation = "SHOW";
            }
        };

        FormActions.subscribe(function (msg) {
            self.operation = msg;
        });

        self.$onInit = function () {
            var value = Storage.readStorageValue("panelStretched");
            self.panelStretched = value == undefined ? false : value != 'false';
            self.operation = "SEARCH";
        };

        $('link[data-direction]').each(function () {
            var $this = $(this);
            $this.attr('rel', null);
            if ($this.attr('data-direction') == "rtl") {
                $this.attr('rel', 'stylesheet');
            }
        });
    }
})(angular.module("lego.base"));