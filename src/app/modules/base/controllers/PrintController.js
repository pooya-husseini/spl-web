(function (module) {

    module.controller("PrintController", PrintController);

    PrintController.$inject = ["url", 'model', "FileUtils", "$http", "$translate"];

    function PrintController(url, Model, FileUtils, $http, $translate) {
        var self = this;

        if (url != undefined) {
            self.url = url
        }
        if (Model != undefined) {
            self.Model = Model;
        }
        self.Actions = Actions;

        function Actions() {
            return {
                report: function (x) {
                    var i = self.url + x + "/" + $translate.use();
                    $http.post(i, self.Model, {responseType: "arraybuffer"}).then(function (e) {
                        FileUtils.saveData(e.data, "Report." + x, FileUtils.getMimeType(x))
                    })
                }
            }
        }
    }

})(angular.module("lego.base"));