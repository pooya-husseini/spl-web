/**
 * Created by mokaram on 07/17/2016.
 */
(function (module) {

    module.directive('datePicker', [function () {

        function updateModel(ngModel, scope) {
            return function (event) {
                ngModel.$setViewValue(event);
            }
        }

        function setupPicker(scope, element, attrs, ngModel) {
            var control = element.find('input').first().persianDatepicker({
                observer: true,
                //inputDelay: attrs.updateDelay || 800,
                onSelect: updateModel(ngModel),
                format: 'YYYY/MM/DD',
                timePicker: {
                    enabled: false
                }
            });

            element.find('span').click(function () {
                clearValue(control, ngModel);
            });

            control.blur(function () {
                ngModel.$setTouched();
                scope.$apply();
            });
            return control
        }

        function setDate(control, ngModel) {
            control.persianDatepicker('setDate', ngModel.$viewValue);
        }

        function clearValue(control, ngModel) {
            ngModel.$setViewValue(null);
            control.val('');
        }

        return {
            transclude: false,
            restrict: 'E',
            template: '<input type="text" ng-model="sample" ui-mask="9999/99/99" ui-mask-placeholder ui-mask-placeholder-char="_" >',
            //'<span class="input-group-addon"><i class="icon-reload icons"></i></span>',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (angular.isUndefined(ngModel)) return;

                var control = setupPicker(scope, element, attrs, ngModel);

                ngModel.$render = function () {
                    if (angular.isUndefined(ngModel.$viewValue)) {
                        clearValue(control, ngModel);
                        return
                    }
                    if (typeof ngModel.$viewValue === 'string') {
                        control.persianDatepicker('setDate', new Date(ngModel.$viewValue));
                    }
                    else {
                        control.persianDatepicker('setDate', ngModel.$viewValue);
                    }
                }
            }
        }
    }]);
})(angular.module('lego.base'));