/**
 * Created by keshvari on 9/8/14.
 */
(function (module) {
    module.directive('uiMasterViewer', ['$timeout', '$filter', '$rootScope', function ($timeout, $filter, $rootScope) {

        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/MasterViewer.html',
            scope: {
                label: "@",
                labelSize: "@",
                translationPrefix: '@',
                orientation: "@",
                model: "=",
                onErrorMessageContent: '@',
                onErrorMessageTitle: '@',
                data: '=',
                filters: '@',
                palceholder: '@',
                ignoreFields: '@'
            },
            link: function (scope, element, attr) {
                scope.$watch('data', function (newValue, oldValue) {
                    if (!angular.isUndefined(scope.data) && scope.data != null && checkDataIsEmpty(scope.data) != 0) {
                        // accepts texts like field1=Filter1:'Param1':'Param2',field2=Filter2:'Param1':'Param2'
                        var filterPropertyExtractor = /((\w+)=(\w+)(:((')(\w|\d)+('))|(\w+))+)/g;
                        //var filterPropertyExtractor = /(\w+):(\w+)[(]*(\w+)(,)*(\w+)*[)]*/g;
                        var propertyExtractor = /(\w+)(?=:)/;
                        var filterWithArgsExtractor = /((?!(\w+):)(\w+)[(]*(\w+)*(,)*(\w)*[)]*)/g;
                        var argumentsWithParenthesisExtractor = /([(])((\w+)+(,)*)*([)])/;
                        var haveArgument = /(\w+)(?![(])((\w+)+(,)*)*(?=[)])/g;
                        var argumentExtractor = /(\w+)/g;
                        var filterNameExtractor = /(\w+)(?=[(])/g;
                        scope.dataList = [];
                        /*contains all propertyNames in json object*/
                        var propertyList = [];
                        var temp = Object.keys(scope.data);
                        for (var i = 0; i < temp.length; i++) {
                            var key = temp[i];
                            if (scope.ignoreFields) {
                                if (scope.ignoreFields.indexOf(key) >= 0) {
                                    continue;
                                }
                            }

                            if (key.indexOf('$') != 0) {
                                propertyList.push(key);
                            }
                        }
                        scope.showPlaceHolder = false;
                        if (attr.filters) {
                            for (var s = 0; s < propertyList.length; s++) {
                                var text = scope.translationPrefix + '.' + propertyList[s] + '.label';
                                scope.dataList.push({
                                    label: $rootScope.translate(text),
                                    content: scope.data[propertyList[s]]
                                });
                            }

                            var tokens = attr.filters.split(",");
                            for (var j = 0; j < tokens.length; j++) {
                                var token = tokens[j];
                                var operands = token.split("=");
                                var fieldName = operands[0];
                                var others = operands[1];
                                var filters = others.split(":");
                                var filterName = filters[0];
                                var indexOfItem = propertyList.indexOf(fieldName);
                                var parameters = [];
                                parameters.push(scope.dataList[indexOfItem].content);
                                parameters = parameters.concat(filters.slice(1));
                                scope.dataList[indexOfItem].content = $filter(filterName).apply(this, parameters);
                                parameters.length = 0;
                            }

                            //for(var i = 0; i < propertyList.length; i++) {
                            //    var txt = scope.translationPrefix + '.' + propertyList[i] + '.label';
                            //    scope.dataList.push(
                            //        {
                            //            label: $rootScope.translate(txt),
                            //            content: scope.data[propertyList[i]]
                            //        }
                            //    );
                            //}
                            ///*if filter should be applied*/
                            //var propertyFilterPair = scope.filters.match(filterPropertyExtractor);
                            //for (var i = 0; i < propertyFilterPair.length; i++) {
                            //    var pairItem = propertyFilterPair[i];
                            //    var propertyName = pairItem.match(propertyExtractor);
                            //    if(propertyList.indexOf(propertyName[i])){
                            //        var filterPart = pairItem.match(filterWithArgsExtractor);
                            //        var filterName;
                            //        var listOfArguments = [];
                            //        if(haveArgument.test(filterPart[0])){
                            //            var argumentsWithParenthesis = filterPart[0].match(argumentsWithParenthesisExtractor);
                            //            listOfArguments  = argumentsWithParenthesis[0].match(argumentExtractor);
                            //            filterName = pairItem.match(filterNameExtractor)[0];
                            //        }else{
                            //            filterName = filterPart[0];
                            //        }
                            //        /*trying to find the item in list to applying the filter on it*/
                            //        var indexOfItem = propertyList.indexOf(propertyName[i]);
                            //        listOfArguments.unshift(scope.dataList[indexOfItem].content);
                            //        scope.dataList[indexOfItem].content = $filter(filterName).apply(this, listOfArguments);
                            //
                            //
                            //    }else{
                            //        console.warn( i +"th property in filters  is not found in results");
                            //    }
                            //}
                        } else {
                            /*if filter not used*/
                            for (var i = 0; i < propertyList.length; i++) {
                                var txt = scope.translationPrefix + '.' + propertyList[i] + '.label';
                                scope.dataList.push(
                                    {
                                        label: $rootScope.translate(txt),
                                        content: scope.data[propertyList[i]]
                                    }
                                );

                            }

                        }
                        $timeout(function () {
                            scope.$apply();
                        })

                    } else {
                        scope.showPlaceHolder = true;
                    }

                });
            }
        }
    }]);
})(angular.module('lego.base'));

var checkDataIsEmpty = function (toBeCkecked) {
    var counter = 0;
    for (var key in toBeCkecked) {
        ++counter;
    }
    return counter;
};