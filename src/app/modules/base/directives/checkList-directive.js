/**
 * Created by mokaram on 06/26/2016.
 */
(function (module) {

    module.directive("checklist", [function () {
        return {
            restrict: "E",
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkList.html',
            scope: {
                label: "@",
                placeholder: "@",
                id: "@",
                labelSize: "@",
                ngModel: "=",
                mainType: "@",
                name: "@",
                model: "=",
                notNull: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@",
                header: "@"
            },
            link: function (scope, attributes) {
                scope.$watch('model', function (newValue, oldValue) {
                    if (typeof newValue === 'string') {
                        if (!find(scope.ngModel, newValue)) {
                            scope.ngModel.push({text: newValue});
                        }
                    }
                });
                scope.translationPrefix = attributes.translationPrefix;
                if (!scope.label) {
                    scope.label = scope.translationPrefix + "." + scope.name + ".label";
                }
                if (!scope.labelSize) {
                    scope.labelSize = "3";
                }
            }
        }
    }
    ]);
    function find(model, newVal) {
        for (var i = 0; i < model.length; i++) {
            if (model[i].text === newVal) {
                return true
            }
        }
        return false
    };
})(angular.module('lego.base'));

