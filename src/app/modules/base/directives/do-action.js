/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive('doAction', ['$timeout', function ($timeout) {

        return function (scope, element, attrs) {
            var split = attrs.doAction.split(":");
            var shortkey = split[0];
            var action = split[1];
            var condition = undefined;
            if (split[1].indexOf('?') >= 0) {
                split = split[1].split("?");
                condition = split[0];
                action = split[1];
            }

            scope.$on("key_down", function (event, args) {
                if (args.keyPressed.toLowerCase() == shortkey.toLocaleLowerCase()) {
                    $timeout(function () {
                        if ((condition != undefined && scope.$eval(condition)) || condition == undefined) {
                            scope.$eval(action);
                        }
                    });
                }
            });
        };
    }]);
})(angular.module("lego.base"));