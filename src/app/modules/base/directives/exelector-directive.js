/**
 * Created by mokaram on 07/04/2016.
 */
(function (module) {

    module.directive("exelector", ["$q", "$http", "$timeout", function ($q, $http, $timeout) {
        function checkForm(formCtrl) {
            if (angular.isUndefined(formCtrl)) {
                throw new Error("Validation cannot be performed on an element that is outside a form");
            }
            if (angular.isUndefined(formCtrl.$name)) {
                throw new Error("Form name must be specified for validation to work");
            }
        }

        function setMainType(scope) {
            scope.data = [];
            if (scope.mainType != undefined) {
                if (scope.detailFields == undefined) {
                    scope.detailFields = "description";
                }

                scope.fieldsToSelect = "description";
                scope.bindableField = "code";

                var gpService = module.$injector.get('GeneralParameterRestService');
                gpService.search({mainType: scope.mainType}).$promise.then(
                    function (success) {
                        $timeout(function () {
                            scope.data = success;
                        });
                    }
                );
            } else {
                scope.data = scope.sourceItems;
            }
        }

        function initialize(scope) {
            if (!scope.labelSize) {
                scope.labelSize = "3";
            }
            if (!scope.orientation) {
                scope.orientation = "horizontal";
            }

            if (!scope.label) {
                scope.label = scope.$parent.translationPrefix + "." + scope.name + ".label";
            }

            var detailFieldsArray = scope.detailFields.split(",");
            scope.selectedFields = scope.fieldsToSelect.split(",");
            scope.propsFilter = "";// "id: $select.search, name: $select.search";
            scope.fieldNames = [];
            scope.item = {};

            for (var i = 0; i < detailFieldsArray.length; i++) {
                var obj = detailFieldsArray[i];
                scope.fieldNames.push(obj);
                scope.propsFilter += obj + ": $select.search,";
            }

        }

        function setValidators(scope, element, ngModel) {
            if (scope.notNull === "true") {
                ngModel.$validators.notNull = function (modelValue, viewValue) {
                    var value = modelValue || viewValue;
                    //var isValid = angular.isObject(value);
                    var isValid = angular.isDefined(value);

                    ngModel.$setValidity('NotNull', isValid);

                    if (!isValid && ngModel.$touched) {
                        element.addClass('has-error');
                    }
                    else {
                        element.removeClass('has-error');
                    }
                    return isValid;
                };

            }

        }

        function setWatchers(scope, ngModel) {
            scope.$watch('item.selected', function (newVal, oldVal) {
                if (!angular.equals(newVal, oldVal)) {
                    $timeout(function () {
                        ngModel.$setTouched();

                        if (scope.bindableField != undefined && typeof newVal !== 'string' && Object.keys(newVal).length > 0) {
                            ngModel.$setViewValue(newVal[scope.bindableField]);
                        } else {
                            ngModel.$setViewValue(newVal);
                        }
                    });
                }
            });

            scope.$watch('sourceItems', function (newVal) {
                scope.data = newVal;
            });

            scope.$watch('data', function (newVal) {
                if (newVal != undefined) {
                    if (typeof ngModel.$modelValue === 'string') {
                        if (scope.bindableField != undefined) {
                            for (var i = 0; i < newVal.length; i++) {
                                var item = newVal[i];
                                if (item[scope.bindableField] == ngModel.$modelValue) {
                                    scope.item.selected = item;
                                }
                            }
                        }
                    }
                }
            });

            ngModel.$render = function () {
                scope.item.selected = ngModel.$modelValue;
            }
        }

        function setEvents(element, ngModel) {
            angular.element(element).on('blur', 'div.ui-select-container', function () {
                ngModel.$setTouched();
                ngModel.$validate();
            });
        }

        return {
            restrict: "E",
            require: ['ngModel', '?^form'],
            //replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/exelector-directive.html',
            scope: {
                mainType: "@",
                label: "@",
                placeholder: "@",
                name: "@",
                orientation: "@",
                notNull: "@",
                labelSize: "@",
                fieldsToSelect: '@',
                headerField: '@',
                detailFields: '@',
                disabled: "=",
                sourceItems: "=",
                bindableField: "@"
            },
            link: function (scope, element, attr, ctrls) {

                var ngModel = ctrls[0];
                var form = ctrls[1];

                checkForm(form);
                setMainType(scope);
                initialize(scope);
                setValidators(scope, element, ngModel);
                setWatchers(scope, ngModel);
                setEvents(element, ngModel);
                scope.propsFilter = scope.propsFilter.substring(0, scope.propsFilter.length - 1);
            }
        };
    }]);
})(angular.module('lego.base'));