/**
 * Created by pooya on 16.03.15.
 */
(function (module) {
    module.directive('focusOn', ['$timeout', function ($timeout) {
        return function (scope, element, attrs) {
            scope.$on("key_down", function (event, args) {
                if (attrs.focusOn.toLowerCase() == args.keyPressed.toLowerCase()) {
                    $timeout(function () {
                        element[0].focus();
                    });

                }
            });
        };
    }]);
})(angular.module("lego.base"));