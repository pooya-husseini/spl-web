/**
 * Created by keshvari on 9/8/14.
 */
(function (module) {
    function emptyDataList(scope) {
        if (scope.showDetail == true) {
            /*removing last items*/
            while (scope.dataList.length > 0) {
                scope.dataList.pop();
            }
        }
    }

    function exitErrorMode(element) {
        element.removeClass('has-error');
        angular.element('.popover').remove();
    }

    function enterErrorMode(result, scope, element) {
        if (result == undefined || result.length == 0) {
            console.log("error occured.");
            var options = {
                title: scope.onErrorMessageTitle,
                content: scope.onErrorMessageContent,
                html: true,
                animation: 'true',
                placement: 'top',
                trigger: 'focus',
                template: '<div class="popover popover-error" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'

            };
            element.addClass('has-error');
            element.find('#searchbox').popover(options);
            element.find('#searchbox').focus();
            element.find('#searchbox').addClass('has-error');
        }
    }

    module.directive('uiLookup', ['$timeout','$filter','$rootScope',function ($timeout,$filter,$rootScope) {

        function handleWithFilters(temp, scope, filterPropertyExtractor, getFilterIndex, propertyExtractor, filterWithArgsExtractor, haveArgument, argumentsWithParenthesisExtractor, argumentExtractor, filterNameExtractor, result) {
            for (var i = 0; i < temp.length; i++) {
                var key = temp[i];
                if (scope.ignoreFields) {
                    if (scope.ignoreFields.indexOf(key) >= 0) {
                        continue;
                    }
                }

                if (key.indexOf('$') == 0) {
                    continue;
                }
                var propertyFilterPair = scope.filters.match(filterPropertyExtractor);

                var index = getFilterIndex(propertyFilterPair, temp[i]);
                var valueToShow;
                if (index != -1) {
                    var pairItem = propertyFilterPair[index];
                    var propertyName = pairItem.match(propertyExtractor);
                    var filterPart = pairItem.match(filterWithArgsExtractor);
                    var filterName;
                    var listOfArguments = [];
                    if (haveArgument.test(filterPart[0])) {
                        var argumentsWithParenthesis = filterPart[0].match(argumentsWithParenthesisExtractor);
                        listOfArguments = argumentsWithParenthesis[0].match(argumentExtractor);
                        filterName = pairItem.match(filterNameExtractor)[0];
                    } else {
                        filterName = filterPart[0];
                    }

                    listOfArguments.unshift(result[propertyName[0]]);
                    var myFilter = $filter(filterName).apply(this, listOfArguments);
                    valueToShow = myFilter;
                }else{
                    valueToShow = scope.data[temp[i]];
                }

                var txt = scope.translationPrefix + '.' + temp[i] + '.label';
                scope.dataList.push(
                    {
                        label: $rootScope.translate(txt),
                        content: valueToShow
                    }
                );
            }
        }

        function handleRegularly(temp, scope, propertyList) {
            for (var i = 0; i < temp.length; i++) {
                var key = temp[i];
                if (scope.ignoreFields) {
                    if (scope.ignoreFields.indexOf(key) >= 0) {
                        continue;
                    }
                }

                if (key.indexOf('$') != 0) {
                    propertyList.push(key);
                }
            }
            for (var i = 0; i < propertyList.length; i++) {
                var txt = scope.translationPrefix + '.' + propertyList[i] + '.label';
                scope.dataList.push(
                    {
                        label: $rootScope.translate(txt),
                        content: scope.data[propertyList[i]]
                    }
                );
            }
        }

        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/lookup.html',
            scope:{
                service:"@",
                label:"@",
                translationPrefix:'@',
                labelSize:"@",
                orientation:"@",
                title:"@",
                model:"=",
                addFunction:"&",
                searchFunction:"&",
                ignoreFields:"@",
                searchIcon:"@",
                addIcon:"@",
                onErrorMessageContent:'@',
                onErrorMessageTitle:'@',
                data:'=',
                filters:'@',
                functionName:'@'
            },
            link: function (scope, element, attr) {
                var filterPropertyExtractor = /(\w+):(\w+)[(]*(\w+)(,)*(\w+)*[)]*/g;
                var propertyExtractor = /(\w+)(?=:)/;
                var filterWithArgsExtractor = /((?!(\w+):)(\w+)[(]*(\w+)*(,)*(\w)*[)]*)/g;
                var argumentsWithParenthesisExtractor = /([(])((\w+)+(,)*)*([)])/;
                var haveArgument = /(\w+)(?![(])((\w+)+(,)*)*(?=[)])/g;
                var argumentExtractor = /(\w+)/g;
                var filterNameExtractor = /(\w+)(?=[(])/g;

                scope.showBody = false;
                scope.dataList = [];

                scope.addItem = function(){
                    exitErrorMode(element);
                    scope.addFunction().then(
                        /*resolve area*/
                        function(){
                            scope.$emit('ItemAdditionCompleted');

                        },
                        /*error area*/
                        function(){
                            scope.$emit('ItemAdditionFailed');
                            console.log("Item not added");
                        }
                    )
                };
                scope.findItem = function(){
                    scope.showBody = true;
                    scope.showDetail = true;
                    emptyDataList(scope);
                    exitErrorMode(element);
                    console.log("inside find item");
//                    console.log(scope.searchFunction);
                    console.log(scope);
                    scope.searchFunction().then(
                        /*on resolve*/
                        function(result){
                            scope.$emit('DataExtractionCompleted');
                            scope.data = result;
                            var propertyList = [];
                            var temp=Object.keys(scope.data);
                            if(attr.filters){
                                handleWithFilters.call(this, temp, scope, filterPropertyExtractor, getFilterIndex, propertyExtractor, filterWithArgsExtractor, haveArgument, argumentsWithParenthesisExtractor, argumentExtractor, filterNameExtractor, result);
                            }else{
                                handleRegularly(temp, scope, propertyList);
                            }
                        },
                        /*on resolve*/
                        function(){
                            scope.$emit('DataExtractionFailed');
                            console.log("Item not extracted to lookup");
                        }
                    );

                };


                scope.showDetail = false;
                var service = module.$injector.get(attr.service);
//                var dataList =[];


//                element.find('#searchbox').keyup(function(event){
//                    if(event.keyCode == '13'){
//                        scope.trigger();
//                    }
//                });


                scope.trigger = function(){

                    if(angular.isUndefined(scope.model)){
                        return;
                    }
                    var relatedFunction = scope.functionName;
                    emptyDataList(scope);
                    scope.showBody = true;
                    service[relatedFunction]({id: scope.model}).$promise.then(
                        /*on success */
                        function (result) {
                            scope.$emit('DataExtractionCompleted');
                            scope.data = result;
                            emptyDataList(scope);
                            exitErrorMode(element);
                            enterErrorMode(result, scope, element);
                            var propertyList = [];
                            var temp=Object.keys(scope.data);
                            if(attr.filters){
                                handleWithFilters.call(this, temp, scope, filterPropertyExtractor, getFilterIndex, propertyExtractor, filterWithArgsExtractor, haveArgument, argumentsWithParenthesisExtractor, argumentExtractor, filterNameExtractor, result);
                            }else{
                                handleRegularly(temp, scope, propertyList);
                            }

                            scope.showDetail = true ;

                        },
                        /*on error*/
                        function (error) {
                            scope.$emit('DataExtractionFailed');
                            var options = {
                                title:scope.onErrorMessageTitle,
                                content: scope.onErrorMessageContent,
                                html:true,
                                animation:'true',
                                placement:'top',
                                trigger:'focus',
                                template: '<div class="popover popover-error" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'

                            };
                            element.addClass('has-error');
                            element.find('#searchbox').popover(options);
                            element.find('#searchbox').focus();
                            element.find('#searchbox').addClass('has-error');

                        });
                };

                var getFilterIndex = function(list,item){
                    for (var i = 0; i < list.length; i++) {
                        var obj = list[i];
                        var propertyName = obj.match(propertyExtractor);
                        if(propertyName[0] == item){
                            return i;
                        }
                    }
                    return -1;
                }


            }
        }
    }]);
})(angular.module('lego.base'));
