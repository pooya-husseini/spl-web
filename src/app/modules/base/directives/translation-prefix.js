/**
 * Created by pooya on 6/19/16.
 */


(function (module) {

    module.directive("splTranslationPrefix", [function () {
        return {
            restrict: "A",
            transclude: false,
            scope: false,
            link: function (scope, element, attributes) {
                scope.translationPrefix = attributes.splTranslationPrefix;
            },
            controller: Controller
        }
    }]);

    Controller.$inject = ["$scope"];

    function Controller($scope) {
        var self = this;

        $scope.$watch('translationPrefix', function (newVal, oldVal) {

            self.translationPrefix = newVal;
            
        })
    }
})(angular.module('lego.base'));

