/**
 * Created by keshvari on 7/27/14.
 */
(function (module) {

    module.directive("uiInputChecklist", ["$q", "$http", function ($q, $http) {
        return {
            restrict: "E",
            replace: true,
            transclude: false,
            templateUrl: '/app/modules/base/templates/checkList.html',
            scope: {
                type: "@",
                label: "@",
                placeholder: "@",
                id: "@",
                validation: "@",
                state: "@",
                feedback: "@",
                value: "@",
                orientation: "@",
                labelSize: "@",
                ngModel: '=?',
                descriptor: '&?',
                disabled: "@",
                visible:"@"
            },
            defaults: {
                label: " ",
                placeholder: "",
                validation: "",
                state: "normal",
                feedback: ""
            },
            link: function(scope){
                scope.visible=angular.isDefined(scope.visible)?scope.visible:true;
                scope.disabled=angular.isDefined(scope.disabled)?scope.disabled:false;
            }
        }
    }]);
})(angular.module('lego.base'));