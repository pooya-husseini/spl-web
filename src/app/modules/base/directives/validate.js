/**
 * @author Mohammad Milad Naseri (m.m.naseri@gmail.com)
 * @date 14/8/14 AD
 */
(function (module) {
    'use strict';
    module.provider('uiValidation', ['$injector', function ($injector) {
        var validators = {};
        this.register = function (name, validator) {
            if (angular.isObject(name) && angular.isUndefined(validator)) {
                angular.forEach(name, function (validator, name) {
                    this.register(name, validator);
                }, this);
                return this;
            }
            if (!angular.isString(name)) {
                throw new Error("Validator name must be a string");
            }
            if (angular.isFunction(validator)) {
                if (!angular.isArray(validator.$inject)) {
                    validator.$inject = $injector.annotate(validator);
                }
            } else if (angular.isArray(validator) && validator.length > 0 && angular.isFunction(validator[validator.length - 1])) {
                var annotation = [];
                for (var i = 0; i < validator.length - 1; i++) {
                    annotation.push(validator[i]);
                }
                validator = validator[validator.length - 1];
                validator.$inject = annotation;
            } else {
                throw new Error("Invalid validator object");
            }
            validators[name] = validator;
            return this;
        };
        this.$get = function () {
            //we make a copy to disallow tampering with the validators in run phase
            return angular.extend({}, validators);
        };
        //registering default validators
        //noinspection JSUnusedGlobalSymbols
        this.register({
            //checks that the given element has a value
            required: function (value) {
                return value !== "" && value !== undefined && value !== null && value !== false;
            },
            //checks for the given values to be in order. Value must be between first and second.
            between: function (value, first, second) {
                if (!isNaN(parseFloat(value)) && !isNaN(parseFloat(first)) && !isNaN(parseFloat(second))) {
                    value = parseFloat(value);
                    first = parseFloat(first);
                    second = parseFloat(second);
                }
                return value >= first && value <= second;
            },
            //checks to see value is less than target
            lessThan: function (value, target) {
                if (!isNaN(parseFloat(value)) && !isNaN(parseFloat(target))) {
                    value = parseFloat(value);
                    target = parseFloat(target);
                }
                return value < target;
            },
            //checks to see if value is greater than target
            greaterThan: function (value, target) {
                if (!isNaN(parseFloat(value)) && !isNaN(parseFloat(target))) {
                    value = parseFloat(value);
                    target = parseFloat(target);
                }
                return value > target;
            },
            //performs an identity equality
            equals: function (value, target) {
                if (value == undefined || value == "") {
                    return true;
                }
                return value == target;
            },
            //performs a strict equality
            sameAs: function (value, target) {
                if (value == undefined || value == "") {
                    return true;
                }
                return value === target;
            },
            //matches the given value against a pattern
            pattern: function (value, pattern) {
                if (value == undefined || value == "") {
                    return true;
                }
                return new RegExp("^" + pattern + "$").test(value);
            },
            //matches the given value against a numeric pattern
            number: function (value) {
                if (value == undefined || value == "") {
                    return true;
                } else {
                    return /^[+\-]?(\d|[۰۱۲۳۴۵۶۷۸۹])+(\.(\d|[۰۱۲۳۴۵۶۷۸۹])+)?$/.test(value);
                }
            },
            //checks to see if the given value is an email address
            email: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return /[a-z]+([\._][a-z0-9]+)*([+\-][^@]+)?@(\d+\.\d+\.\d+\.\d+|([a-z0-9]+(-[a-z0-9]+)?\.)*[a-z]{2,})/i.test(value);
            },
            //checks that the value has a length more than the specified value
            minLength: function (value, length) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && value.length >= parseInt(length);
            },
            //checks that the value has a length less than the specified value
            maxLength: function (value, length) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && value.length <= parseInt(length);
            },
            //checks that the given password is strong enough
            strongPassword: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && value.length > 5 && /\d/.test(value) && /[a-z]/.test(value) && /[A-Z]/.test(value) && /[`~!@#$%^&*\(\)\-+=_\[\]\{\}|\\\/?,\.<>]/.test(value);
            },
            persian: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && /^[آ-ی\s]+$/.test(value);
            },
            english: function (value) {
                if (value == undefined || value == "") {
                    return true;
                }
                return angular.isString(value) && /[\s\w\d]+/.test(value);
            },
            //runs a custom validator function on the input data
            custom: function (value, fn) {
                if (!angular.isFunction(fn)) {
                    throw new Error("Given input is not a valid function: " + fn);
                }
                var args = [value];
                for (var i = 2; i < arguments.length; i++) {
                    args.push(arguments[i]);
                }
                return fn.apply(null, args);
            }
        });
    }]);
    module.directive('uiValidation', ['uiValidation', '$parse', '$injector', '$q', 'Utils', function (uiValidation, $parse, $injector, $q, utils) {
        return {
            restrict: "A",
            link: function ($scope, $element, $attrs) {
                if (!$attrs.name) {
                    throw new Error("Element must have a name for validation to work properly");
                }
                var form = $element[0];
                while (form && form.nodeName.toLowerCase() != 'form') {
                    form = form.parentNode;
                }
                if (!form) {
                    throw new Error("Validation cannot be performed on an element that is outside a form");
                }
                if (!form.hasAttribute('name')) {
                    throw new Error("Form name must be specified for validation to work");
                }
                var messages = {};
                if (angular.isDefined($attrs['validationError'])) {
                    messages = $parse($attrs['validationError'])($scope);
                }
                if (!angular.isObject(messages)) {
                    throw new Error("Validation error messages declared through `validationErrors` must be a valid JSON object");
                }
                var $target = ($element[0].nodeName.toLowerCase() == 'input' || $element[0].nodeName.toLowerCase() == 'select' ? $element : $element.find('input').add($element.find('select')));
                $target.on('focus', function () {
                    $scope.$apply(function () {
                        $element.addClass('ui-active');
                    });
                });
                $target.on('blur', function () {
                    $scope.$apply(function () {
                        $element.removeClass('ui-active');
                        $element.addClass('ui-seen');
                        triggerValidation(null, null);
                    });
                });
                $element.addClass('ui-pristine');
                if (!angular.isString($attrs.modelAttribute)) {
                    $attrs.modelAttribute = angular.isDefined($attrs['model']) ? 'model' : (angular.isDefined($attrs['ngModel']) ? 'ngModel' : 'model');
                }
                //find the form hosting the element
                var formName = angular.element(form).attr('name');
                var elementName = $attrs.name;
                if (!angular.isObject($scope[formName])) {
                    $scope[formName] = {};
                }
                if (!angular.isObject($scope[formName].$error)) {
                    $scope[formName].$error = {};
                }
                if (angular.isUndefined($scope[formName].$valid)) {
                    $scope[formName].$valid = true;
                }
                $scope[formName][elementName] = {
                    $valid: true,
                    $error: {}
                };
                var expression = $parse($attrs.uiValidation);
                //set up the context
                var context = Object.create($scope);
                angular.forEach(uiValidation, function (validator, validation) {
                    var message = messages[validation] || 'app.validation.error.' + validation;

                    message = utils.translate(message);

                    // $scope.$emit('bu.ui.text', {
                    //     text: function () {
                    //         return message;
                    //     },
                    //     set: function (newMessage) {
                    //         message = newMessage;
                    //     }
                    // });
                    messages[validation] = message;
                    context[validation] = function () {
                        //because validation is triggered on being linked, this will be applied even when
                        //the directive has just been initialized
                        $element.addClass('validation-' + validation);
                        var args = [$parse($attrs[$attrs.modelAttribute])($scope)];
                        for (var i = 0; i < arguments.length; i++) {
                            args.push(arguments[i]);
                        }
                        var $inject = [];
                        angular.forEach(validator.$inject, function (dependency) {
                            $inject.push(dependency);
                        });
                        $inject.splice(0, args.length);
                        args.splice(0, 0, null);
                        var func = validator.bind.apply(validator, args);
                        func.$inject = $inject;
                        var validated = $q.defer();
                        var result = $injector.invoke(func);
                        if (angular.isObject(result) && angular.isFunction(result.then)) {
                            //while validation is in progress, we will assume that the field is invalid
                            result.then(validated.resolve, validated.reject);
                        } else {
                            validated.resolve(result);
                        }
                        validated.promise.then(function (result) {
                            if (!result) {
                                //we first register the errors with the element
                                $scope[formName][elementName].$error[validation] = true;
                                $scope[formName][elementName].$valid = false;
                                //then lets invalidate the form
                                $scope[formName].$valid = false;
                                if (angular.isArray($scope[formName].$error[validation])) {
                                    var found = false;
                                    angular.forEach($scope[formName].$error[validation], function (value) {
                                        found = !found && value == elementName;
                                    });
                                    if (!found) {
                                        $scope[formName].$error[validation].push(elementName);
                                    }
                                } else {
                                    $scope[formName].$error[validation] = [elementName];
                                }
                            }
                        });
                    };
                });

                function triggerValidation(newValue, oldValue) {
                    if (newValue != oldValue) {
                        $element.removeClass('ui-pristine');
                        $element.addClass('ui-dirty');
                    }
                    //we assume the form to be valid (in regards to the current element)
                    $scope[formName][elementName].$valid = true;
                    var count = 0;
                    angular.forEach($scope[formName].$error, function (value, name) {
                        var errors = [];
                        angular.forEach($scope[formName].$error[name], function (value) {
                            if (value != elementName) {
                                errors.push(value);
                            }
                        });
                        $scope[formName].$error[name] = errors;
                        if (errors.length == 0) {
                            delete $scope[formName].$error[name];
                        } else {
                            count++;
                        }
                    });
                    $scope[formName].$valid = count == 0;
                    angular.forEach($scope[formName][elementName].$error, function (value, name) {
                        delete $scope[formName][elementName].$error[name];
                    });
                    //now let's call the expression to see if the validation is broken
                    expression(context);
                }

                //add necessary watches
                $scope.$watch($attrs[$attrs.modelAttribute], triggerValidation);
                //if a `dependsOn` clause is specified, we will trigger the validation when the
                //`dependsOn` models change, as well
                if (angular.isString($attrs.dependsOn)) {
                    $scope.$watch($attrs.dependsOn, triggerValidation, true);
                }
                $scope.$watch(function () {
                    return $scope[formName][elementName].$error;
                }, function () {
                    $($target).popover('destroy');
                    var errorMessages = displayErrors();
                    if (errorMessages.length > 0 && $element.hasClass('ui-active')) {
                        $($target).popover('show');
                    }
                }, true);
                $scope.$watch(function () {
                    return $element.hasClass('ui-seen');
                }, displayErrors);
                $scope.$watch(function () {
                    return $element.hasClass('ui-dirty');
                }, displayErrors);
                function displayErrors() {
                    $($target).popover('destroy');
                    $element.removeClass('has-error');
                    var errorMessages = [];
                    angular.forEach($scope[formName][elementName].$error, function (invalid, error) {
                        errorMessages.push(messages[error]);
                    });
                    if (errorMessages.length > 0 && ($element.hasClass('ui-dirty') || $element.hasClass('ui-seen'))) {
                        $element.addClass('has-error');

                        $($target).popover({
                            content: '<ul><li>' + errorMessages.join("</li><li>") + '</li></ul>',
                            html: true,
                            placement: 'top',
                            trigger: 'focus',
                            template: '<div class="popover popover-error" style="z-index:10000;" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
                        });
                    }
                    return errorMessages;
                }
            }
        };
    }]);

})(angular.module('lego.base'));
