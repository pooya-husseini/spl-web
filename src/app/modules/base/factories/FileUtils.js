(function (module) {

    var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    var ARGUMENT_NAMES = /([^\s,]+)/g;

    function getParamNames(func) {
        var fnStr = func.toString().replace(STRIP_COMMENTS, '');
        var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
        if (result === null)
            result = [];
        return result;
    }

    module.factory('FileUtils', FileUtils);

    FileUtils.$inject = ['$http'];

    function FileUtils($http) {
        return {
            saveData: (function () {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style.display = "none";
                return function (data, fileName, mimeType) {
                    var blob = new Blob([data], {type: mimeType}),
                        url = window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = fileName;
                    a.click();
                    window.URL.revokeObjectURL(url);
                };
            })(),

            upload: function (item, url) {
                var children = item.getElementsByTagName("input");

                var fileInput = _.find(children, function (item) {
                    return item.className.indexOf('ng-hide') < 0;
                });
                var formData = new FormData();
                formData.append("file", fileInput.files[0]);

                return $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formData,
                    transformRequest: angular.identity
                });
            },
            getMimeType: function (extension) {
                switch (extension) {
                    case "docx":
                        return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    case "xlsx":
                        return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    case "pdf":
                        return "application/pdf";
                    case "html":
                        return "text/html";
                    case "xml":
                        return "application/html";
                    case "csv":
                        return "text/plain";
                }
            }
        };
    }

})(angular.module('lego.base'));