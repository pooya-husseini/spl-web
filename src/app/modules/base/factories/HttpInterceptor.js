(function (module) {

    module.factory('HttpInterceptor', httpInterceptor);

    httpInterceptor.$inject = ['$q', 'Utils'];

    function httpInterceptor($q, utils) {
        return {
            'request': function (config) {
                return config;
            },

            'responseError': function (response) {
                // do something on error
                if (response.status === null || response.status === 500) {
                    var exceptionType = undefined;
                    var exceptionTypeHeader = response.headers("exceptionType");
                    if (exceptionTypeHeader != null) {
                        exceptionType = exceptionTypeHeader;
                    } else {
                        exceptionType = response.data.exception;
                    }
                    //var exceptionType = response.headers("exception_type");
                    var error = {
                        method: response.config.method,
                        url: response.config.url,
                        message: response.data,
                        status: response.status
                    };
                    if (exceptionType == undefined) {
                        utils.notify(utils.translate('app.failed'), "danger");
                    } else {
                        var translation = utils.translate('exception.translation.' + exceptionType);
                        if (translation == undefined) {
                            utils.notify(utils.translate('app.failed'), "danger");
                        } else {
                            utils.notify(translation, "danger");
                        }
                    }
                }
                return $q.reject(response);
            }
        };
    }
})(angular.module('lego.base'));