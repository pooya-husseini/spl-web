/**
 * Created by pooya on 20/12/14.
 */

(function (module) {
    module.factory('FormActions', FormActions);

    function FormActions() {

        var self = this;
        self.subs = [];
        return {
            subscribe: function (cb) {
                self.subs.push(cb);
            },
            setActions: function (actions) {
                self.actions = actions;
            },
            getActions: function () {
                return self.actions;
            },
            signalAction: function (action) {
                _.forEach(self.subs, function (s) {
                    s(action);
                });
            }
        }
    }
})(angular.module("lego.base"));