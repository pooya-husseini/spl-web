(function (module) {

    module.factory('NotificationUtils', NotificationUtils);

    NotificationUtils.$inject = ['$q', '$filter', 'toastr'];

    function NotificationUtils($q, $filter, toastr) {

        function translate(key) {
            return $filter("translate")(key);
        }

        function notifyFunc(msg, type) {


            switch (type) {
                case "warning":
                    //alertify.warning(msg);
                    toastr.warning(msg);
                    break;
                case "normal":
                    toastr.info(msg);
                    break;
                case "danger":
                case "error":

                    toastr.error(msg, {
                        "timeOut": "-1",
                        "extendedTimeOut": "-1"
                    });
                    break;
                case "success":
                    toastr.success(msg);
                    break;
            }

        }

        return {
            notifySuccess: function () {
                return notifyFunc(translate('app.success'), "success");
            },
            notify: function (msg, type) {
                return notifyFunc(msg, type);
            },

            fixedNotify: function (msg, type) {

                switch (type) {
                    case "warning":
                        toastr.warning(msg);
                        break;
                    case "normal":
                        toastr.info(msg);
                        break;
                    case "danger":
                    case "error":
                        toastr.error(msg, {
                            "timeOut": "-1",
                            "extendedTimeOut": "-1"
                        });
                        break;
                    case "success":
                        toastr.success(msg);
                        break;
                }
            },

            question: function (msg) {
                msg = msg || translate('app.question-body');
                var defer = $q.defer();
                alertify
                    .okBtn(translate('app.yes'))
                    .cancelBtn(translate('app.no'))
                    .confirm(msg, function () {
                        defer.resolve();
                    }, function () {
                        defer.reject();
                    });


                return defer.promise;
            }
        };
    }

})(angular.module('lego.base'));