(function (module) {

    module.factory('Operations', operations);

    function closeAllMenus(ctrl) {
        _.forEach(ctrl.menus, function (value) {
            value.expanded = false;
            value.entered = false;
        });
    }

    operations.$inject = ['Utils', 'NotificationUtils', '$timeout', '$location', '$resource', 'ProjectCache', '$uibModal', 'FormActions', '$state', '$q'];

    function operations(utils, NotificationUtils, $timeout, $location, $resource, projectCache, $uibModal, FormActions, $state, $q) {

        return {
            /**
             *
             *
             * @param templateUrl
             * @param controller
             * @param extraOps
             * @returns {{select: ops.select, edit: ops.edit, create: ops.create, show: ops.show}}
             */
            makeBasicCrudService: function (templateUrl, controller, extraOps) {

                function makeModal(crudType, data) {
                    return $uibModal.open({
                        templateUrl: templateUrl,
                        size: 'lg',
                        animation: true,
                        controller: controller,
                        controllerAs: '$ctrl',
                        resolve: {
                            data: function () {
                                return data;
                            }
                        }
                    }).result;
                }

                var ops = {
                    select: function () {
                        return makeModal("SEARCH", undefined);
                    },
                    edit: function (model) {
                        return makeModal("EDIT", model);
                    },
                    create: function (model) {
                        return makeModal("CREATE", model);
                    },

                    show: function (model) {
                        return makeModal("SHOW", model);
                    }
                };

                _.assign(ops, extraOps);

                return ops;
            },

            /**
             *  makes the basic rest operations
             * @param {string} url the url to define operations on it
             * @param {object} extraOperations the extra operations that should added to the result
             * @param {object} cacheableConfig configuration for the transferring data. that contains cacheKeyFunc and cacheKeyValue.
             *  two callback functions to produce the key and value of the cache
             * @returns {$resource} the $resource with the given parameters
             */
            makeBasicRestClient: function (url, extraOperations, cacheableConfig) {
                var readOps = {
                    findById: {method: 'GET', params: {id: '@id'}},
                    searchColumns: {method: 'GET', isArray: true, params: {url: 'searchColumn'}},
                    get: {method: 'GET', isArray: true, params: {url: 'all'}},
                    search: {method: 'POST', isArray: true, params: {url: 'search'}},
                };
                var cacheable = Object.keys(cacheableConfig).length == 2;

                var fullCrudOps = {
                    create: {
                        method: 'POST', params: {url: 'create'},
                        transformRequest: function (data, h) {
                            if (cacheable) {
                                addItemToCache(data);
                            }
                            return JSON.stringify(data);
                        }
                    },
                    update: {
                        method: 'PUT', params: {id: '@id'},
                        transformResponse: function (data, h) {
                            var obj = JSON.parse(data);
                            if (cacheable) {
                                addItemToCache(obj);
                            }
                            return obj;
                        }
                    },
                    delete: {
                        method: 'DELETE', params: {id: '@id'},
                        transformRequest: function (data, h) {
                            return JSON.stringify(data);
                        }
                    }
                };

                var ops = {};

                _.assign(ops, readOps);
                _.assign(ops, fullCrudOps);
                _.assign(ops, extraOperations);

                var resource = $resource(url, {}, ops);

                function addItemToCache(data) {
                    var key = cacheableConfig.cacheKeyFunc(data);
                    // var key = data.code + '.' + data.mainType;
                    var value = cacheableConfig.cacheValueFunc(data);
                    // var value = data.description;
                    projectCache.put(key, value);
                }

                function removeItemFromCache(data) {
                    var key = cacheableConfig.cacheKeyFunc(data);
                    // var key = data.code + '.' + data.mainType;
                    projectCache.remove(key);
                }

                if (cacheable) {
                    resource.get().$promise.then(function (success) {
                        for (var index in success) {
                            if (success.hasOwnProperty(index)) {
                                addItemToCache(success[index]);
                            }
                        }
                    });
                }

                return resource;
            },

            initialize: function (ctrl, preModel) {

                FormActions.setActions(["SEARCH", "CREATE"]);

                ctrl.operation = "SEARCH";

                FormActions.subscribe(function (action) {
                    ctrl.operation = action;
                });


                ctrl.Model = {};
                ctrl.Search = {};
                ctrl.selected = {};

                if (preModel != undefined) {
                    ctrl.Model = utils.cloneObject(preModel);
                }

                window.setTimeout(function () {
                    $(window).resize();
                    $(window).resize();
                }, 1000);

                if (utils.isFunction(ctrl.$onInit)) {
                    ctrl.$onInit();
                }

            },

            createPanelUiRouteActions: function (ctrl) {
                return {
                    selectHome: function () {
                        $state.go("home")
                    },

                    closeAllMenus: function () {
                        closeAllMenus(ctrl);
                    },

                    leaveMenu: function (menu) {
                        menu.entered = false;
                    },

                    expandMenu: function (menu) {
                        if (menu.entered == false && menu.expanded == false) {
                            closeAllMenus(ctrl);
                            menu.entered = true;
                            menu.expanded = true;
                        } else {
                            closeAllMenus(ctrl);
                        }
                    },

                    select: function (menu, detail) {
                        $state.go(detail.state);

                        ctrl.SelectedPage = menu.prefix + "." + detail.labelKey;
                        ctrl.selectedIndex = -1;
                        ctrl.hasCreate = detail.hasCreate;

                        ctrl.closeAllMenus();
                    }
                }

            },
            createPanelNgRouteActions: function (ctrl) {
                return {
                    selectHome: function () {
                        $location.url("/#")
                    },

                    closeAllMenus: function () {
                        closeAllMenus(ctrl);
                    },

                    leaveMenu: function (menu) {
                        menu.entered = false;
                    },

                    expandMenu: function (menu) {
                        if (menu.entered == false && menu.expanded == false) {
                            closeAllMenus(ctrl);
                            menu.entered = true;
                            menu.expanded = true;
                        } else {
                            closeAllMenus(ctrl);
                        }
                    },

                    select: function (menu, menuItem) {
                        $timeout(function () {
                            $location.url(menu.prefix.replace(/\./g, "/") + "/" + menuItem.labelKey);

                            ctrl.SelectedPage = menu.prefix + "." + menuItem.labelKey;
                            ctrl.selectedIndex = -1;
                            ctrl.hasCreate = menuItem.hasCreate;

                            ctrl.closeAllMenus();
                        })
                    }
                }

            },
            /**
             *
             * @param config
             * @param extraActions
             * @returns {{find: actions.find, insert: actions.insert, add: actions.add, cancel: actions.cancel}}
             */
            createBasicModalActions: function (config, extraActions) {
                function getModel() {
                    return config.ctrl["Model"];
                }

                function getGrid() {
                    return config.ctrl["MainGrid"];
                }


                var actions = {
                    find: function () {
                        config.service.search(getModel()).then(
                            function (data) {
                                getGrid().content = data
                            }
                        );
                    },
                    insert: function () {
                        config.$modalInstance.close(getModel());
                    },
                    add: function () {
                        var selected = getGrid().gridApi.selection.getSelectedRows()[0];
                        config.$modalInstance.close(selected);
                    },
                    cancel: function () {
                        config.$modalInstance.dismiss();
                    }
                };

                _.assign(actions, extraActions);

                return actions;
            },
            makeBasicServiceOperations: function (config, extraOperations) {
                var operations = {
                    update: function (item) {
                        var defer = $q.defer();
                        config.restService.update(item).$promise.then(
                            function (success) {
                                NotificationUtils.notifySuccess();
                                defer.resolve(success);
                            },
                            function (failure) {
                                defer.reject(failure);
                            }
                        );

                        return defer.promise;
                    },
                    create: function (item) {
                        var defer = $q.defer();
                        config.restService.create(item).$promise.then(
                            function (success) {
                                NotificationUtils.notifySuccess();
                                defer.resolve(success);
                            },
                            function (failure) {
                                defer.reject(failure);
                            }
                        );

                        return defer.promise;
                    },

                    search: function (item) {
                        var defer = $q.defer();
                        config.restService.search(item).$promise.then(
                            function (data) {
                                if (data.length <= 0) {
                                    NotificationUtils.notify(utils.translate('app.not-found'), "warning");
                                }
                                defer.resolve(data);
                            },
                            function (failure) {
                                defer.reject(failure);
                            }
                        );

                        return defer.promise;
                    },
                    delete: function (item) {
                        var defer = $q.defer();
                        NotificationUtils.question().then(function () {
                            config.restService.delete({id: item.id}).$promise.then(
                                function (success) {
                                    NotificationUtils.notifySuccess();
                                    defer.resolve(success);
                                },
                                function (failure) {
                                    defer.reject(failure);
                                }
                            )
                        });

                        return defer.promise;
                    },
                    changePassword: function (item) {
                        var defer = $q.defer();

                        config.crudService.changePassword(item).then(
                            function (result) {
                                config.restService.changePassword(result).$promise.then(
                                    function (success) {
                                        NotificationUtils.notifySuccess();
                                        defer.resolve(success);
                                    },
                                    function (failure) {
                                        defer.reject(failure);
                                    }
                                );
                            }
                        );
                        return defer.promise;
                    }
                };
                _.assign(operations, extraOperations);
                return operations;
            },
            makeBasicActions: function (config, extraActions) {
                // crudService, restService, grid, model, printUrl
                function getSelectedItem() {
                    return getGrid().gridApi.selection.getSelectedRows()[0];
                }

                function getModel() {
                    return config.ctrl['Search'];
                }

                function getGrid() {
                    return config.ctrl['MainGrid'];
                }

                function getOperation() {
                    return config.ctrl["operation"];
                }

                function setOperation(op) {
                    FormActions.signalAction(op);
                }

                var actions = {
                    show: function () {
                        setOperation("SHOW");
                        var selectedItem = getSelectedItem();
                        config.crudService.show(selectedItem);
                    },

                    update: function (args) {
                        var selectedItem = getSelectedItem();
                        setOperation("EDIT");
                        config.crudService.edit(selectedItem).then(
                            function (result) {
                                config.service.update(result);
                            });
                    },
                    create: function () {
                        config.service.create(config.ctrl["Model"]).then(
                            function (success) {
                                $timeout(function () {
                                    config.ctrl['Model'] = {};
                                });
                            }
                        );
                    },

                    reset: function () {
                        $timeout(function () {
                            config.ctrl['Search'] = {};
                        });
                    },

                    showPrint: function () {
                        getPrintService().print(config.printUrl, getModel());
                    },

                    search: function () {

                        config.service.search(getModel()).then(
                            function (data) {

                                getGrid().content = data;
                            });
                    },
                    delete: function () {
                        var selectedItem = getSelectedItem();
                        config.service.delete(selectedItem).then(
                            function (success) {
                                utils.removeGridItem(getGrid().content, selectedItem);
                            }
                        );
                    },
                    changePassword: function () {
                        var selectedItem = getSelectedItem();
                        config.crudService.changePassword(selectedItem).then(
                            function (result) {
                                config.service.changePassword(result);
                            }
                        );
                    }
                };
                _.assign(actions, extraActions);
                return actions;
            }
        }
    }

})(angular.module('lego.base'));