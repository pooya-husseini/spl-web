/**
 * Created by pooya on 20/12/14.
 */

(function (module) {
    module.factory('ProjectCache', projectCache);

    projectCache.$inject = ['$cacheFactory'];

    function projectCache($cacheFactory) {
        return $cacheFactory('projectCache');
    }
})(angular.module("lego.base"));
