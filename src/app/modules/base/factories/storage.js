(function (module) {

    module.factory('Storage', Storage);

    function Storage() {

        return {

            writeOnStorage: function (key, value) {
                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem(key, value);
                    return true;
                } else {
                    return false;
                }
            },
            readStorageValue: function (key) {
                if (typeof(Storage) !== "undefined") {
                    return localStorage.getItem(key);
                } else {
                    return undefined;
                }
            }
        };
    }
})(angular.module('lego.base'));