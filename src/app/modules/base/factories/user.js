(function (module) {

    module.factory('User', User);

    User.$inject = ['$http', '$q'];

    function User($http, $q) {

        return {
            getCurrentUser: getCurrentUser
        };

        function getCurrentUser() {
            var defer = $q.defer();

            $http.get("/login/user").success(function (success) {
                var user = {
                    username: success.username,
                    displayName: success.firstName,
                    fullName: success.firstName + "  " + success.lastName,
                    avatar: "/app/resources/images/admin.png",
                    status: 'busy',
                    lastLoginDate: success.lastLoginDate,
                    role: success.role
                };
                defer.resolve(user);
            });

            return defer.promise;
        }


    }
})(angular.module('lego.base'));