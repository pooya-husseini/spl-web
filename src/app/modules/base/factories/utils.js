(function (module) {

    var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    var ARGUMENT_NAMES = /([^\s,]+)/g;

    function getParamNames(func) {
        var fnStr = func.toString().replace(STRIP_COMMENTS, '');
        var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
        if (result === null)
            result = [];
        return result;
    }

    module.factory('Utils', Utils);

    Utils.$inject = ['$q', 'i18nService', '$translate', '$filter'];

    function Utils($q, i18nService, $translate, $filter) {

        function translate(key) {
            return $filter("translate")(key);
        }

        function translateAsync(key) {
            return $translate(key);
        }

        return {

            cloneObject: function (obj) {
                return JSON.parse(JSON.stringify(obj));
            },
            normalizeGDate: function (date, delimiter) {
                delimiter = delimiter || '/';
                return date[0] + delimiter + ('0' + date[1]).slice(-2) + delimiter + ('0' + date[2]).slice(-2);
            },

            translate: function (item) {
                return translate(item)
            },

            initGrid: function (ctrl, gridConf, autoArrange) {
                var grid = {
                    multiSelect: false,

                    enableHighlighting: true,
                    enableColumnReordering: true,
                    enableColumnResize: true,
                    enableRowSelection: true,
                    enableRowHeaderSelection: false,
                    enablePaginationControls: false,
                    paginationCurrentPage: 1,
                    minRowsToShow: 10,
                    paginationPageSize: 10,
                    totalItems: 100,
                    content: [],
                    data: '$ctrl.parent.MainGrid.content',
                    onRegisterApi: function (gridApi) {
                        grid.gridApi = gridApi;
                    }
                };

                grid.isSelected = function () {
                    return grid.gridApi != undefined &&
                        grid.gridApi.selection.getSelectedRows() != undefined &&
                        grid.gridApi.selection.getSelectedRows().length > 0;
                };

                grid.selectedRowId = function () {
                    if (grid.isSelected()) {
                        return grid.gridApi.selection.getSelectedRows()[0].id;
                    }
                };

                _.assign(grid, gridConf);

                if (autoArrange) {
                    arrangeColumnsFunc(grid, 100);
                }
                i18nService.setCurrentLang('fa');

                return grid;
            },
            arrangeGridColumns: function (grid) {
                arrangeColumnsFunc(grid, 100);
            },
            extractSelectedItem: function (grid) {
                if (grid == undefined || grid.data == undefined) {
                    return {};
                }

                var selected = copyObject(grid.selectedItems);

                if (selected.length > 1) {
                    return selected
                } else if (selected.length === 1) {
                    return selected[0];
                } else {
                    return {};
                }

            },


            removeGridItem: function (model, item) {
                return _.remove(model, item);

            },
            addToModel: function (model, data) {
                return _.concat(model, data);
            },

            populateGrid: function (grid, data) {
                if (data instanceof Array) {
                    grid.data = data;
                } else {
                    grid.data = [data];
                }
            },
            convertEnum: function (e, className) {
                return _.map(e, function (item, index) {
                    return {
                        code: item.value,
                        description: translate(className + "." + item.value + ".label"),
                        index: index
                    }
                });

            },
            loadAllPromises: function (promises) {
                var defer = $q.defer();
                $q.all(promises).then(function () {
                    defer.resolve();
                }, function () {
                    defer.reject();
                });
                return defer.promise;
            },

            intersect: function (item1, item2) {
                return _.intersection(item1, item2);
            },
            isFunction: function (item) {

                return typeof item === "function";
            },
            validityCheck: function (conf) {
                var elementNgModel = conf.form[conf.ngModel.$name];

                if (conf.form.saved || conf.validityChecker()) {
                    elementNgModel.$setValidity(conf.validationName, true);
                    conf.element.removeClass('has-error');
                } else {
                    elementNgModel.$setValidity(conf.validationName, false);
                    if (!conf.initState) {
                        conf.element.addClass('has-error');
                    }
                    if (conf.data) {
                        elementNgModel.data = conf.data;
                    }
                }
            },
            testAndSet: TestAndSet
        };
    }

    /**
     * @return {Function}
     */
    function TestAndSet() {
        var that = this;
        return function () {
            if (that.result == undefined) {
                that.result = false;
                return true;
            }
            return that.result;
        };
    }

    function arrangeColumnsFunc(grid, headsLength) {
        if (headsLength == undefined) {
            headsLength = 88;
        }

        var total = headsLength;
        var gfilter = _.filter(grid.columnDefs, function (item) {
            return item.visible != false;
        });

        if (headsLength == 100) {
            var count = gfilter.length;
        } else {
            count = gfilter.length - 1; // to remove the operation column
        }
        var a = _.range(count);

        var slice = headsLength / count;
        var acc = 0;
        var array = _.map(a, function () {
            var returnVal = slice;
            if (total < slice) {
                returnVal = total
            }
            total -= slice;
            acc += returnVal;
            return returnVal;
        });
        array.push(100 - acc);

        _.each(gfilter, function (item, index) {
            item.width = array[index] + "%";
        });
    }

    function copyObject(obj) {
        return JSON.parse(JSON.stringify(obj));
    }
})(angular.module('lego.base'));