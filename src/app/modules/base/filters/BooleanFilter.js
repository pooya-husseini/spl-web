(function (module) {
    module.filter('boolean', booleanFilter);

    function booleanFilter() {
        return function (code) {
            if (code == true) {
                return "بلی";
            } else {
                return "خیر";
            }
        };
    }
})(angular.module("lego.base"));