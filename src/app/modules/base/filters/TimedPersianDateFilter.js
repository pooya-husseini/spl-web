/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : XVIEW CODE GENERATOR
 * Email : husseini@caspco.ir
 * Date: Dec 1, 2015 5:37:15 PM
 * @version 1.0.0
 */


(function (module) {

    module.filter('timedPersianDate', ['persianDateFilter', timedPersianDate]);

    function timedPersianDate(persianDateFilter) {
        return function (date) {
            if (date != undefined && date != '') {
                return persianDateFilter(date) + " " + date.substring(10);
            } else {
                return "";
            }
        };
    }
})(angular.module("lego.base"));