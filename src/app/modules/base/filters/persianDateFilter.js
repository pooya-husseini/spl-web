(function (module) {
    module.filter('persianDate', persianDate);

    persianDate.$inject = ['Utils'];

    function persianDate(utils) {
        return function (item) {
            if (angular.isUndefined(item)) {
                return '';
            }
            var gYear = parseInt(item.split('-')[0]);
            var gmonth = parseInt(item.split('-')[1]);
            var gDay = parseInt(item.split('-')[2]);
            var jalali = JalaliDate.gregorianToJalali(gYear, gmonth, gDay);
            return utils.normalizeGDate(jalali, "/")
        };
    }


})(angular.module('lego.base'));