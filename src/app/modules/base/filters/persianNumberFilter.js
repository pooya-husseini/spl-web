/**
 * Created by keshvari on 8/19/14.
 */
(function (module) {

    module.filter('persianNumber', persianNumber);

    function persianNumber() {
        return function (item) {
            if (angular.isUndefined(item)) {
                return '';
            }
            var id = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
            return item.replace(/[0-9]/g, function (w) {
                return id[+w]
            });
        };
    }
})(angular.module('lego.base'));