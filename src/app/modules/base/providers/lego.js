(function (module) {
    module.provider('lego', ['$translateProvider', lego]);

    function lego($translateProvider) {
        return {
            loadTranslations: function (translations, locale) {
                var convertedTranslation = {};
                _.forOwn(translations, function (value, key) {
                    if (key !== "prefix") {
                        var translationKey = translations.prefix + "." + key;
                        if (translations.prefix == "" || translations.prefix.trim() == "") {
                            translationKey = key;
                        }
                        convertedTranslation[translationKey] = value;
                    }
                });
                $translateProvider.translations(locale, convertedTranslation);
            },
            $get: function () {

            }
        }
    }
})(angular.module('lego.base'));