(function (module) {
    module.service("print", ["$uibModal", print]);

    function print(e) {
        return {
            print: function (url, model) {
                return e.open({
                    templateUrl: "/app/view/PrintDialog.html",
                    size: "sm",
                    controller: "PrintController",
                    windowClass: "modal-print-content",
                    resolve: {
                        url: function () {
                            return url;
                        },
                        model: function () {
                            return model;
                        }
                    }
                }).result
            }
        }
    }
})(angular.module("lego.base"));