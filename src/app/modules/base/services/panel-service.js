(function (module) {
    module.service("panel", ["$http", "$q", PanelService]);


    function PanelService($http, $q) {

        return {
            loadMenu: function () {

                var defer = $q.defer();
                $http.get("app/conf/menu.json").then(function (success) {

                    // these two operations is just for removing empty objects from menu

                    var menus = _.filter(success.data.menus, function (item) {
                        return Object.keys(item).length > 0;
                    });
                    _.forEach(menus, function (menu) {
                        menu.details = _.filter(menu.details, function (item) {
                            return Object.keys(item).length > 0;
                        });
                    });
                    defer.resolve(menus);
                });
                return defer.promise;
            }
        }
    }
})(angular.module("lego.base"));