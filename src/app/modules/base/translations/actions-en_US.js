(function (module) {
    module.config(['legoProvider', function (legoProvider) {
        var trs = {
            'prefix': 'Action',
            "CREATE": "Create Page",
            "SEARCH": "Search Page"
        };
        legoProvider.loadTranslations(trs, "en_US");
    }
    ]);
})(angular.module("lego.base"));