(function (module) {
    module.config(['legoProvider', function (legoProvider) {
            var trs = {
                'prefix': 'Action',
                "CREATE": "صفحه ایجاد",
                "SEARCH": "صفحه جستجو"
            };
            legoProvider.loadTranslations(trs, "fa_IR");
        }]
    );
})(angular.module("lego.base"));