(function (module) {
    module.config(['legoProvider', function (legoProvider) {
            var trs = {

                'prefix': 'messages',
                "required": "مقدار این فیلد اجباری است",
                "minlength": "طول عبارت باید بیشتر از {0} باشد",
                "maxlength": "طول عبارت باید کمتر از {0} باشد",
                "email": "مقدار وارد شده آدرس ایمیل نمی باشد",
                "pattern": "مقدار وارد شده از الگوی مورد نظر تبعیت نمی کند",
                "greaterThan": "مقدار وارد شده باید بیشتر از {0} باشد",
                "lessThan": "مقدار وارد شده باید کمتر از {0} باشد",
                "english": "مقدار وارد شده باید کاراکترهای انگلیسی باشد",
                "persian": "مقدار وارد شده باید کاراکترهای فارسی باشد",
                "equals": "مقدار وارد شده باید برابر با {0} باشد",
                "sameAs": "مقدار وارد شده باید برابر با {0} باشد",
                "number": "مقدار وارد شده باید عدد باشد",
                "custom": "مقدار وارد شده نادرست است",
                "strong-password": "رمز وارد شده از لحاظ امنیتی ضعیف می باشد"
            };
            legoProvider.loadTranslations(trs, "fa_IR");
        }]
    );
})(angular.module("lego.base"));