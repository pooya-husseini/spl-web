(function (module) {
    module.directive('customValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^ngController', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[2];
                var ngModel = ctrls[0];
                var ngController = ctrls[1];
                var data = attrs.customValidator;
                var initState = new Utils.testAndSet();

                ngModel.$validators.customValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'custom',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return ngController[data]();
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));