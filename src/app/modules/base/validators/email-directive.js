(function (module) {


    module.directive('emailValidator', Validation);

    Validation.$inject = ["Utils"];
    const email = /[a-z]+([\._][a-z0-9]+)*([+\-][^@]+)?@(\d+\.\d+\.\d+\.\d+|([a-z0-9]+(-[a-z0-9]+)?\.)*[a-z]{2,})/i;


    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.emailValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'email',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || email.test(value);
                    }
                };
            }
        }
    }
})(angular.module("lego.base"));