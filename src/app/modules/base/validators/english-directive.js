(function (module) {
    module.directive('englishValidator', Validation);

    const pattern = /^[\s\w\d\?><;,\{}\[\]\-_\+=!@#\$%^&\(\)\*\|']*$/;

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.englishValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'english',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));