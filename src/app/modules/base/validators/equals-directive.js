(function (module) {
    module.directive('equalsValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {

                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = attrs.equalsValidator;
                var initState = new Utils.testAndSet();

                ngModel.$validators.equalsValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'equals',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker,
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return data == value;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));