(function (module) {
    module.directive('greaterThanValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = parseFloat(attrs.greaterThanValidator);
                var initState = new Utils.testAndSet();

                ngModel.$validators.greaterThanValidator = function (value) {

                    value = parseFloat(value);

                    Utils.validityCheck({
                        form: form,
                        validationName: 'greaterThan',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker,
                        data: data
                    });


                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || value > data;
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));