(function (module) {
    module.directive('lessThanValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = parseFloat(attrs.lessThanValidator);
                var initState = new Utils.testAndSet();

                ngModel.$validators.lessThanValidator = function (value) {
                    value = parseFloat(value);

                    Utils.validityCheck({
                        form: form,
                        validationName: 'lessThan',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || value < data;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));