/**
 * Created by mokaram on 06/21/2016.
 */
(function (module) {
    module.directive('minLengthValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = parseInt(attrs.minLengthValidator);
                var initState = new Utils.testAndSet();

                ngModel.$validators.minLengthValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'minlength',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || value.length >= data;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));