(function (module) {
    module.directive('numberValidator', Validation);

    Validation.$inject = ["Utils"];
    const pattern = /^[+\-]?(\d|[۰۱۲۳۴۵۶۷۸۹])+(\.(\d|[۰۱۲۳۴۵۶۷۸۹])+)?$/;


    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {

                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();
                
                ngModel.$validators.numberValidator = function (value) {
                    // var value = ngModel.$modelValue;
                    Utils.validityCheck({
                        form: form,
                        validationName: 'number',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));