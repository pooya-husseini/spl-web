(function (module) {
    module.directive('patternValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var pattern = attrs.patternValidator;
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.patternValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'pattern',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || new RegExp("^" + pattern + "$").test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));