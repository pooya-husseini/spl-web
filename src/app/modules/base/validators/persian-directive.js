(function (module) {
    module.directive('persianValidator', Validation);

    Validation.$inject = ["Utils"];

    const pattern = /^([آ-ی\s]|[۰۱۲۳۴۵۶۷۸۹])+$/;

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.persianValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'persian',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));
