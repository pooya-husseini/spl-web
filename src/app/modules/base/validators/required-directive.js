(function (module) {
    module.directive('requiredValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.requiredValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'required',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return value && value.length > 0;
                    }
                };
            }
        }
    }
})(angular.module("lego.base"));