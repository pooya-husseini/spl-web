(function (module) {
    module.directive('sameAsValidator', Validation);

    Validation.$inject = ["Utils"];

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {

                var form = ctrls[1];
                var ngModel = ctrls[0];
                var data = attrs.sameAsValidator;
                var initState = new Utils.testAndSet();

                ngModel.$validators.sameAsValidator = function (value) {
                    Utils.validityCheck({
                        form: form,
                        validationName: 'sameAs',
                        ngModel: ngModel,
                        element: element,
                        validityChecker: ValidityChecker,
                        initState: initState(),
                        data: data
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return data === value;
                    }
                };
            }
        }
    }


})(angular.module("lego.base"));