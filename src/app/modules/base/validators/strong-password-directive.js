(function (module) {
    module.directive('strongPasswordValidator', Validation);
    Validation.$inject = ["Utils"];

    const pattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;

    function Validation(Utils) {
        return {
            require: ['ngModel', '^form'],
            link: function (scope, element, attrs, ctrls) {
                var form = ctrls[1];
                var ngModel = ctrls[0];
                var initState = new Utils.testAndSet();

                ngModel.$validators.strongPasswordValidator = function (value) {

                    Utils.validityCheck({
                        form: form,
                        validationName: 'strong-password',
                        ngModel: ngModel,
                        element: element,
                        initState: initState(),
                        validityChecker: ValidityChecker
                    });

                    /**
                     * @return {boolean}
                     */
                    function ValidityChecker() {
                        return !value || value.length == 0 || pattern.test(value);
                    }
                };
            }
        }
    }

})(angular.module("lego.base"));