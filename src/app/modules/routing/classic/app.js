(function () {
    'use strict';
    angular.module('lego.routing.classic', ['ngRoute', 'ngSanitize', 'ngResource', 'lego.base']);
})();
