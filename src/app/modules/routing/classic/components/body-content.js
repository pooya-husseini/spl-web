(function (module) {
    module.component("splBodyContent", component());
    function component() {
        return {
            templateUrl: "/app/modules/routing/classic/templates/body-content.html",
            transclude: false
        }
    }
})(angular.module("lego.routing.classic"));