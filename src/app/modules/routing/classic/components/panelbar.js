(function (module) {
    module.component("splPanelBar", splPanelBar());

    function splPanelBar() {
        return {
            templateUrl: "/app/modules/base/templates/panelbar.html",
            transclude: true,
            require: {
                parent: "^ngController"
            },
            controller: Controller
        }
    }

    Controller.$inject = ["Operations", "panel"];

    function Controller(operations, PanelService) {
        var self = this;

        this.$onInit = function () {
            PanelService.loadMenu().then(function (menus) {
                self.menus = menus;
                self.Actions = operations.createPanelNgRouteActions(self);
            });

        }
    }
})(angular.module("lego.routing.classic"));