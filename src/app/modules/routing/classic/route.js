(function (module) {
    module.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $routeProvider.when('/', {
            redirectTo: '/home'
        }).when('/:page*', {
            templateUrl: function (parameters) {
                var page = parameters.page;
                if (!/\.html$/.test(page)) {
                    page = page + ".html";
                }
                return "/app/view/" + page;
            }
        });
    }]);

})(angular.module('lego.routing.classic'));