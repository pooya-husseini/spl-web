(function (angular) {

    'use strict';
    angular.module('lego.ui.route', ['ui.router', 'ngSanitize', 'ngResource', 'lego.base']);

})(window.angular);