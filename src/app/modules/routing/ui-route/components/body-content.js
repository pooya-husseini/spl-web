(function (module) {
    module.component("splBodyContent", component());
    function component() {
        return {
            templateUrl: "/app/modules/routing/ui-route/templates/body-content.html",
            transclude: false
        }
    }
})(angular.module("lego.ui.route"));