(function (module) {
    var $urlRouterProviderRef;
    var $stateProviderRef;

    module.config(Configure);
    module.run(Runnable);

    Configure.$inject = ["$locationProvider", "$urlRouterProvider", "$stateProvider"];
    Runnable.$inject = ['$q', '$state', '$http', 'panel'];

    function Configure($locationProvider, $urlRouterProvider, $stateProvider) {
        $urlRouterProviderRef = $urlRouterProvider;
        $locationProvider.html5Mode(false);
        $stateProviderRef = $stateProvider;

        // $stateProvider.state('com', {
        //     template: "<ui-view/>",
        //     url: "/home"
        // });

        // $urlRouterProvider.otherwise("/");

    }

    function Runnable($q, $state, $http, panel) {
        $http.get("app/conf/routes.json").success(function (data) {
            var home = {};
            _.forEach(data, function (value) {

                var keys = Object.keys(value);

                var state = {};

                _.forEach(keys, function (key) {
                    state[key] = value[key];
                });

                // for (var i = 0; i < keys.length; i++) {
                //     var key = keys[i];
                //
                // }
                if (state.home) {
                    home = state;
                }
                // if (!state.parent) {
                //     state.parent = '';
                // }

                $stateProviderRef.state(value.name, state);
            });
            $state.go(home.name);
        });

        // panel.loadMenu().then(function (menus) {
        //     _.forEach(menus, function (menu) {
        //
        //         _.forEach(menu.details, function (detail) {
        //             var name = detail.domain + "." + detail.title;
        //             var url = name.replace(/\./g, "/");
        //             var state = {
        //                 url: url,
        //                 templateUrl: "app/views/" + url + ".html"
        //                 // parent: 'root'
        //             };
        //             $stateProviderRef.state(name, state);
        //         });
        //     });
        // });

        // $state.go("com");
    }
})(angular.module("lego.ui.route"));