/**
 * THIS IS AN AUTO GENERATED CODE.
 * @author : Lego Code Generator
 * Email : husseini@caspco.ir
 * Date: Jul 17, 2016 2:17:47 PM
 * @version 2.5-SNAPSHOT
 */

(function (module) {
    module.factory('RestServiceMock', Service);
    function Model() {
        this.items = {};
        this.id = 0;
    }

    var model = new Model();

    Service.$inject = ['$q', '$timeout', 'Utils'];

    function Service($q, $timeout, Utils) {

        return {
            createInstance: function () {
                return {
                    delete: function (item) {
                        var defer = $q.defer();
                        if (model.items[item.id]) {
                            var item = model.items[item.id];
                            delete model.items[item.id];
                            defer.resolve(item);
                            $timeout(function () {
                                defer.resolve(item);
                            }, 100);
                        }
                        return {$promise: defer.promise};
                    },
                    create: function (obj) {
                        var item = Utils.cloneObject(obj);
                        var defer = $q.defer();
                        $timeout(function () {
                            var id = model.id++;
                            model.items[id] = item;
                            item.id = id;
                            defer.resolve(item);
                        }, 100);
                        return {$promise: defer.promise};
                    },
                    search: function (item) {


                        var defer = $q.defer();
                        $timeout(function () {
                            if (item == undefined) {
                                defer.resolve(model.items);
                            } else {
                                defer.resolve(model.items[item]);
                            }
                        }, 100);

                        return {$promise: defer.promise};
                    },
                    update: function (obj) {
                        var item = Utils.cloneObject(obj);

                        model.items[0] = item;
                        var deffer = $q.defer();
                        $timeout(function () {
                            model.items[0] = item;
                            deffer.resolve(model.items[0])
                        }, 100);
                        return {$promise: deffer.promise};
                    },

                }
            },
            createObject: function () {
                model = new Model();
            }
        }
    }
})(angular.module('lego.test'));
