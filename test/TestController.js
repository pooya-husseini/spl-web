(function (module) {
    module.controller('TestController', [TestController]);

    function TestController() {

        $('link[data-direction]').each(function () {
            var $this = $(this);
            $this.attr('rel', null);
            if ($this.attr('data-direction') == "rtl") {
                $this.attr('rel', 'stylesheet');
            }
        });
    }
})(angular.module("lego.base"));